-- FpgaBee
--
-- Copyright (C) 2012-2013 Topten Software.
-- All Rights Reserved
-- 
-- Licensed under the Apache License, Version 2.0 (the "License"); you may not use this 
-- product except in compliance with the License. You may obtain a copy of the License at
-- 
-- http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software distributed under 
-- the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF 
-- ANY KIND, either express or implied. See the License for the specific language governing 
-- permissions and limitations under the License.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity StatusLedMatrix is
	port 
	(
		-- Clocking
		reset : in std_logic;		 
		clock : in std_logic;	-- clock + clken = max 20Mhz
		clken : in std_logic;

		-- Led Matrix Card Signals
		ss_n : out std_logic;
		mosi : out std_logic;
		sclk : out std_logic;

		-- What to display
		statusA : in std_logic_vector(7 downto 0);
		statusB : in std_logic_vector(7 downto 0);
		statusC : in std_logic_vector(7 downto 0);
		statusD : in std_logic_vector(7 downto 0)
	);

end StatusLedMatrix;

architecture Behavioral of StatusLedMatrix is

	type states is 
	(
		RST,
		CLEAR_ROWS,
		SET_INTENSITY,
		SET_SCANLIMIT,
		SET_SHUTDOWN,
		SEND_STATUS_A,
		SEND_STATUS_B,
		SEND_STATUS_C,
		SEND_STATUS_D,
		START_TX,
		TX_BITS,
		FINISH_TX
	);

	signal state, return_state: states;
	signal sclk_sig : std_logic := '0';
	signal tx_buf : std_logic_vector(15 downto 0);

begin
  	
	sclk <= sclk_sig;

	process(clock)
		variable bit_counter : integer range 0 to 16;
		variable row_counter : integer range 1 to 9;
	begin
		if rising_edge(clock) then
		if reset = '1' then

			state <= RST;

		else

			case state is

				when RST =>
					state <= CLEAR_ROWS;
					sclk_sig <= '0';
					row_counter := 1;
					ss_n <= '0';
					mosi <= '0';

				when CLEAR_ROWS =>
					if row_counter = 9 then 
						state <= SET_INTENSITY;
					else
						tx_buf(15 downto 13) <= "000";
						tx_buf(12 downto 8) <= std_logic_vector(to_unsigned(row_counter, 5));
						tx_buf(7 downto 0) <= (others => '0');
						state <= START_TX;
						row_counter := row_counter + 1;
						return_state <= CLEAR_ROWS;
					end if;

				when SET_INTENSITY =>
					tx_buf <= x"0A11";
					state <= START_TX;
					return_state <= SET_SCANLIMIT;

				when SET_SCANLIMIT => 
					tx_buf <= x"0B07";
					state <= START_TX;
					return_state <= SET_SHUTDOWN;

				when SET_SHUTDOWN => 
					tx_buf <= x"0C07";
					state <= START_TX;
					return_state <= SEND_STATUS_A;

				when SEND_STATUS_A =>
					tx_buf <= x"01" & statusA;
					state <= START_TX;
					return_state <= SEND_STATUS_B;

				when SEND_STATUS_B =>
					tx_buf <= x"02" & statusB;
					state <= START_TX;
					return_state <= SEND_STATUS_C;

				when SEND_STATUS_C =>
					tx_buf <= x"03" & statusC;
					state <= START_TX;
					return_state <= SEND_STATUS_D;

				when SEND_STATUS_D =>
					tx_buf <= x"04" & statusD;
					state <= START_TX;
					return_state <= SEND_STATUS_A;

				when START_TX =>
					if clken='1' then
						sclk_sig <= '0';
						ss_n <= '0';
						mosi <= tx_buf(15);
						tx_buf <= tx_buf(14 downto 0) & '1';
						state <= TX_BITS;
						bit_counter := 16;
					end if;

				when TX_BITS => 
					-- Send bit_counter bits to LED
					-- Assumes mosi already set to first bit, sclk is lo
					if clken='1' then
						if sclk_sig = '1' then
							
							if bit_counter = 1 then
								ss_n <= '1';
								state <= FINISH_TX;
							else
								mosi <= tx_buf(15);
								tx_buf <= tx_buf(14 downto 0) & '1';
								bit_counter := bit_counter - 1;
							end if;

						end if;
						sclk_sig <= not sclk_sig;
					end if;

				when FINISH_TX =>
					if clken='1' then
						ss_n <= '0';
						state <= return_state;
					end if;


			end case;

		end if;
		end if;
	end process;

  
end Behavioral;


