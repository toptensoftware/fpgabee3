-- FpgaBee
--
-- Copyright (C) 2012-2013 Topten Software.
-- All Rights Reserved
-- 
-- Licensed under the Apache License, Version 2.0 (the "License"); you may not use this 
-- product except in compliance with the License. You may obtain a copy of the License at
-- 
-- http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software distributed under 
-- the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF 
-- ANY KIND, either express or implied. See the License for the specific language governing 
-- permissions and limitations under the License.


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
use work.DiskConstants.ALL;
use work.SDStatusBits.ALL;
 
entity DiskControllerPcu is
	port
	(
		-- Clocking
		reset : in std_logic;
		clock_108_000 : in std_logic;								-- CPU Clock
		clken_3_375 : in std_logic;								-- CPU Clock enable

		-- CPU Interface
		cpu_port : in std_logic_vector(2 downto 0);			-- low 3 bits of the z80 port
		cpu_wr : in std_logic;								-- write signal
		cpu_rd : in std_logic;								-- read signal
		cpu_din : in std_logic_vector(7 downto 0);			-- output from z80, input to controller
		cpu_dout : out std_logic_vector(7 downto 0);		-- output from controller, input to z80

		intreq : out std_logic;								-- asserts when disk controller is finished and status hasn't been read

		-- SDCard Arbitration
		sd_arb_req : out std_logic;
		sd_arb_ack : in std_logic;

		-- SDCard Interface
		sd_status : in std_logic_vector(7 downto 0);
		sd_op_wr : out std_logic;
		sd_op_cmd : out std_logic_vector(1 downto 0);
		sd_op_block_number : out std_logic_vector(31 downto 0);
		sd_dstart : in std_logic;
		sd_dcycle : in std_logic;
		sd_dout : in std_logic_vector(7 downto 0);
		sd_din : out std_logic_vector(7 downto 0)

	);
end DiskControllerPcu;


 
architecture behavior of DiskControllerPcu is 


	-- Edge detection the for CPU read/write port signals
	-- (we need this since the Z80 always introduces one wait state
	--  for port instructions and we don't want to inadvertantly double
	--  invoke commands, or increments our DMA buffer address)
	signal cpu_wr_prev : std_logic;
	signal cpu_rd_prev : std_logic;

	-- DMA buffer - CPU side access
	signal cpuram_we : std_logic;
	signal cpuram_addr : std_logic_vector(8 DOWNTO 0);
	signal cpuram_dout : std_logic_vector(7 DOWNTO 0);

	-- DMA buffer - SD controller side
	signal sdram_we : std_logic;
	signal sdram_addr : std_logic_vector(8 DOWNTO 0);
	signal sdram_dout : std_logic_vector(7 DOWNTO 0);

	-- Command execution state machine
	type exec_states is 
	(
		STATE_READY,
		STATE_SD_ARBWAIT,
		STATE_SD_BUSY
	);

	type exec_results is
	(
		RESULT_OK,
		RESULT_SD_ERROR
	);

	signal exec_state : exec_states := STATE_READY;	-- Execution state
	signal exec_request : std_logic;				-- Request a read/write
	signal exec_response : std_logic;				-- Indicates response to read/write command
	signal exec_result : exec_results := RESULT_OK;	-- Result of the last executed command	
	signal busy : std_logic;					-- 1 = command busy
	signal error : std_logic;						-- 1 = SD error

	signal block_number : std_logic_vector(31 downto 0);
	signal op_cmd : std_logic_vector(1 downto 0);

begin


	sd_op_cmd <= op_cmd;


	-- Front end CPU interface
	process (clock_108_000)
	begin

		if rising_edge(clock_108_000) then
		if reset = '1' then

			cpu_wr_prev <= '0';
			cpu_rd_prev <= '0';

			cpuram_addr <= (others=>'0');
			cpuram_we <= '0';

			exec_request <= '0';
			busy <= '0';
			op_cmd <= "00";
			intreq <= '0';

		elsif clken_3_375='1' then

			-- Track read/write edges
			cpu_wr_prev <= cpu_wr;
			cpu_rd_prev <= cpu_rd;

			if cpuram_we = '1' then
				cpuram_addr <= std_logic_vector(unsigned(cpuram_addr) + 1);
			end if;

			-- Reset ram write line
			cpuram_we <= '0';
			exec_request <= '0';

			if exec_response='1' then
				busy <= '0';
				intreq <= '1';
				if exec_result=RESULT_OK then
					error <= '0';
				else
					error <= '1';
				end if;
			end if;

			-- Port write? (ignore all write operations while busy)
			if cpu_wr='1' and cpu_wr_prev='0' then

				case cpu_port is

					when "000" =>
						-- WRITE DATA
						cpuram_we <= '1';
						if cpuram_addr="111111111" and op_cmd="10" then
							-- Buffer full, trigger the write
						end if;

					when "001" =>
						-- WRITE BLOCK NUMBER
						-- LSB first
						block_number <= cpu_din & block_number(31 downto 8);

					when "111" =>
						-- COMMAND
						case cpu_din(1 downto 0) is
							when "00" =>
								-- Rewind buffer
								cpuram_addr <= (others=>'0');

							when "01" =>
								-- Read
								if busy='0' then
									op_cmd <= "01";
									cpuram_addr <= (others=>'0');
									exec_request <= '1';
									busy <= '1';
								end if;

							when "10" => 
								-- Write
								if busy='0' then
									op_cmd <= "10";
									cpuram_addr <= (others=>'0');
									busy <= '1';
									exec_request <= '1';
								end if;

							when others => 
								null;

						end case;

					when others =>
						null;

				end case;

			elsif cpu_rd='1' and cpu_rd_prev='0' then

				case cpu_port is

					when "000" =>
						-- READ DATA
						cpu_dout <= cpuram_dout;
						cpuram_addr <= std_logic_vector(unsigned(cpuram_addr) + 1);


					when "111" => 
						-- STATUS
						cpu_dout <= busy & sd_status(STATUS_BIT_INIT) & "00000" & error;

						-- status has been read so clear the intreq signal
						intreq <= '0';

					when others =>
						cpu_dout <= (others=>'0');

				end case;

			end if;

		end if;

		end if;

	end process;

	-- Command execution unit
	process (clock_108_000)
	begin
		if rising_edge(clock_108_000) then
		if reset='1' then

			sd_op_wr <= '0';
			exec_state <= STATE_READY;
			sd_op_block_number <= x"00000000";
			exec_response <= '0';
			exec_result <= RESULT_OK;

		elsif clken_3_375='1' then

			sd_op_wr <= '0';
			exec_response <= '0';

			case exec_state is
				when STATE_READY =>

					if exec_request='1' then
						-- Request SD from arbiter
						sd_arb_req <= '1';

						-- Wait for ack
						exec_state <= STATE_SD_ARBWAIT;
					else
						sd_arb_req <= '0';
					end if;
			
				when STATE_SD_ARBWAIT => 
					if sd_arb_ack='1' then
						sd_op_block_number <= block_number;
						sd_op_wr <= '1';
						exec_state <= STATE_SD_BUSY;
					end if;

				when STATE_SD_BUSY =>
					if sd_status(STATUS_BIT_BUSY)='0' then

						-- Operation finished?
						if sd_status(STATUS_BIT_ERROR)='1' then
							exec_result <= RESULT_SD_ERROR;
						else
							exec_result <= RESULT_OK;
						end if;

						exec_state <= STATE_READY;
						exec_response <= '1';

					end if;

			end case;
		end if;
		end if;
	end process;

	process (clock_108_000)
	begin
		if rising_edge(clock_108_000) then
		if reset='1' then

			sdram_addr <= (others => '0');

		else

			if sd_dstart = '1' then

				-- Data start
				sdram_addr <= (others => '0');

			elsif sd_dcycle='1' then

				-- Data cycle
				sdram_addr <= std_logic_vector(unsigned(sdram_addr) + 1);
			end if;

		end if;
		end if;
	end process;

	-- SD Controller -> RAM
	sdram_we <= '1' when (sd_dcycle='1' and sd_arb_ack='1' and op_cmd="01") else '0';

	-- Sector buffer
	ram : entity work.RamInferred
	GENERIC MAP
	(
		ADDR_WIDTH => 9
	)
	PORT MAP
	(
		clock_a => clock_108_000,
		wr_a => cpuram_we,
		addr_a => cpuram_addr,
		din_a => cpu_din,
		dout_a => cpuram_dout,

		clock_b => clock_108_000,
		wr_b => sdram_we,
		addr_b => sdram_addr,
		din_b => sd_dout,
		dout_b => sd_din
	);


end;
