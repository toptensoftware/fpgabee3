-- FpgaBee
--
-- Copyright (C) 2012-2013 Topten Software.
-- All Rights Reserved
-- 
-- Licensed under the Apache License, Version 2.0 (the "License"); you may not use this 
-- product except in compliance with the License. You may obtain a copy of the License at
-- 
-- http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software distributed under 
-- the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF 
-- ANY KIND, either express or implied. See the License for the specific language governing 
-- permissions and limitations under the License.


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;


-- Parses a series of bytes in the following FDD track format:
-- 
--    {<anything/ignored/lead gap>} 
--    <f5> <fe> <sector_id_track> <sector_id_side> <sector_id_sector> <sector_id_seclen> <f7> 
--    {sector length bytes of sector data} 
--    <f5> <fb> <f7> 
--    [repeat]
-- 
 
entity TrackParser is
	port
	(
		-- Clocking
		reset : in std_logic;
		clock_108_000 : in std_logic;							-- CPU Clock
		clken_3_375 : in std_logic;								-- CPU Clock enable

		-- In
		cs : in std_logic;										-- Assert to reset and start a new track parse operation
		we : in std_logic;										-- Assert for one clock cycle to pass a byte to this parser
		din : in std_logic_vector(7 downto 0);					-- Incoming data byte

		-- Out
		dout : out std_logic_vector(7 downto 0);				-- Sector data byte
		dout_we : out std_logic;								-- Asserts for one when dout contains valid track data (write byte to DMA ram)
		sector_detected : out std_logic;						-- Asserts for one cycle when start of sector detected (reset DMA ram)
		sector_complete : out std_logic;						-- Asserts for one cycle when end of sector detected (write DMA ram to disk)
		sector_id_track : out std_logic_vector(7 downto 0);		-- Track number parsed from sector id 
		sector_id_side : out std_logic_vector(7 downto 0);		-- Side parsed from sector id 
		sector_id_seclen : out std_logic_vector(7 downto 0);	-- Sector length parsed from sector id
		sector_id_sector : out std_logic_vector(7 downto 0);	-- Sector number parsed from sector id
		sector_count : out unsigned(7 downto 0)					-- The number of parsed sectors (incremented just before sector_complete asserts)
	);
end TrackParser;


 
architecture behavior of TrackParser is 

	type states is 
	(
		STATE_IDLE,
		STATE_WAITING_FOR_LEAD_BYTE,
		STATE_WAITING_FOR_SECOND_LEAD_BYTE,
		STATE_WAITING_FOR_SECID_TRACK, 
		STATE_WAITING_FOR_SECID_SIDE, 
		STATE_WAITING_FOR_SECID_SECTOR,
		STATE_WAITING_FOR_SECID_SECLEN,
		STATE_EXPECTING_ADDRESS_TERMINATOR,
		STATE_WAITING_FOR_SECTOR_LEAD_BYTE,
		STATE_WAITING_FOR_SECTOR_SECOND_LEAD_BYTE,
		STATE_SECTOR_DATA,
		STATE_EXPECTING_TAIL,
		STATE_SECTOR_RECEIVED
	);

	signal cs_prev : std_logic := '0';
	signal state : states := STATE_IDLE;
	signal data_bytes_left : unsigned(9 downto 0);
	signal r_sector_count : unsigned(sector_count'range);

begin

	sector_detected <= '1' when state = STATE_EXPECTING_ADDRESS_TERMINATOR else '0';
	sector_complete <= '1' when state = STATE_SECTOR_RECEIVED else '0';
	sector_count <= r_sector_count;

	process (clock_108_000)
	begin

		if rising_edge(clock_108_000) then
		if reset = '1' then

			cs_prev <= '0';
			state <= STATE_IDLE;
			r_sector_count <= to_unsigned(0, r_sector_count'length);

		elsif clken_3_375='1' then

			cs_prev <= cs;
			dout_we <= '0';

			if cs = '0' then

				-- Deselected, drop back to idle state
				state <= STATE_IDLE;

			end if;

			if cs_prev = '0' and cs='1' then

				-- CS rising edge, switch to start state
				state <= STATE_WAITING_FOR_LEAD_BYTE;

				-- reset sector count
				r_sector_count <= to_unsigned(0, r_sector_count'length);

			end if;

			if we = '1' then

				case state is

					when STATE_IDLE =>
						null;

					when STATE_WAITING_FOR_LEAD_BYTE => 
						if din = x"F5" then
							state <= STATE_WAITING_FOR_SECOND_LEAD_BYTE;
						end if;

					when STATE_WAITING_FOR_SECOND_LEAD_BYTE => 
						if din = x"FE" then
							state <= STATE_WAITING_FOR_SECID_TRACK;
						elsif din = x"F5" then
							state <= STATE_WAITING_FOR_SECOND_LEAD_BYTE;
						else
							state <= STATE_WAITING_FOR_LEAD_BYTE;
						end if;

					when STATE_WAITING_FOR_SECID_TRACK => 
						sector_id_track <= din;
						state <= STATE_WAITING_FOR_SECID_SIDE;

					when STATE_WAITING_FOR_SECID_SIDE => 
						sector_id_side <= din;
						state <= STATE_WAITING_FOR_SECID_SECTOR;

					when STATE_WAITING_FOR_SECID_SECTOR =>
						sector_id_sector <= din;
						state <= STATE_WAITING_FOR_SECID_SECLEN;

					when STATE_WAITING_FOR_SECID_SECLEN =>
						sector_id_seclen <= din;
						case din(1 downto 0) is
							when "00" =>
								data_bytes_left <= to_unsigned(128, data_bytes_left'length);
							when "01" =>
								data_bytes_left <= to_unsigned(256, data_bytes_left'length);
							when "10" =>
								data_bytes_left <= to_unsigned(512, data_bytes_left'length);
							when "11" =>
								data_bytes_left <= to_unsigned(1024, data_bytes_left'length);
							when others =>
								null;
						end case;
						state <= STATE_EXPECTING_ADDRESS_TERMINATOR;

					when STATE_EXPECTING_ADDRESS_TERMINATOR =>
						if din = x"F7" then
							state <= STATE_WAITING_FOR_SECTOR_LEAD_BYTE;
						else
							state <= STATE_WAITING_FOR_LEAD_BYTE;
						end if;

					when STATE_WAITING_FOR_SECTOR_LEAD_BYTE =>
						if din = x"F5" then
							state <= STATE_WAITING_FOR_SECTOR_SECOND_LEAD_BYTE;
						end if;

					when STATE_WAITING_FOR_SECTOR_SECOND_LEAD_BYTE =>
						if din = x"FB" then 
							state <= STATE_SECTOR_DATA;
						elsif din = x"F5" then
							state <= STATE_WAITING_FOR_SECTOR_SECOND_LEAD_BYTE;
						else
							state <= STATE_WAITING_FOR_LEAD_BYTE;
						end if;

					when STATE_SECTOR_DATA =>
						-- output the byte
						dout <= din;
						dout_we <= '1';

						-- update counter
						data_bytes_left <= data_bytes_left - 1;
						if data_bytes_left = to_unsigned(1, data_bytes_left'length) then
							state <= STATE_EXPECTING_TAIL;
						end if;

					when STATE_EXPECTING_TAIL =>
						if din = x"F7" then
							r_sector_count <= r_sector_count + 1;
							state <= STATE_SECTOR_RECEIVED;
						else
							state <= STATE_WAITING_FOR_LEAD_BYTE;
						end if;

					when STATE_SECTOR_RECEIVED =>
						state <= STATE_WAITING_FOR_LEAD_BYTE;

				end case;

			end if;

		end if;
		end if;
	end process;



end;
