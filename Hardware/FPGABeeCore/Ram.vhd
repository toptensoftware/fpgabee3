-- FpgaBee
--
-- Copyright (C) 2012-2013 Topten Software.
-- All Rights Reserved
-- 
-- Licensed under the Apache License, Version 2.0 (the "License"); you may not use this 
-- product except in compliance with the License. You may obtain a copy of the License at
-- 
-- http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software distributed under 
-- the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF 
-- ANY KIND, either express or implied. See the License for the specific language governing 
-- permissions and limitations under the License.


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALLs;
use std.textio.all;
use ieee.std_logic_textio.all;

entity Ram is
	generic
	(
		ADDR_WIDTH : integer;
		DATA_WIDTH : integer := 8
	);
	port
	(
		-- Port A
		clock_a : in std_logic;
		addr_a : in std_logic_vector(ADDR_WIDTH-1 downto 0);
		din_a : in std_logic_vector(DATA_WIDTH-1 downto 0);
		dout_a : out std_logic_vector(DATA_WIDTH-1 downto 0);
		wr_a : in std_logic;

		-- Port B
		clock_b : in std_logic;
		addr_b : in std_logic_vector(ADDR_WIDTH-1 downto 0);
		din_b : in std_logic_vector(DATA_WIDTH-1 downto 0);
		dout_b : out std_logic_vector(DATA_WIDTH-1 downto 0);
		wr_b : in std_logic
	);
end Ram;
 
architecture behavior of Ram is 
begin

	ramImpl : entity work.RamInferred
	GENERIC MAP
	(
		ADDR_WIDTH => ADDR_WIDTH,
		DATA_WIDTH => DATA_WIDTH
	)
	PORT MAP 
	(
		-- port A		
		clock_a => clock_a,
		wr_a => wr_a,
		addr_a => addr_a,
		din_a => din_a,
		dout_a => dout_a,

		-- port B
		clock_b => clock_b,
		wr_b => wr_b,
		addr_b => addr_b,
		din_b => din_b,
		dout_b => dout_b
	);

end;
