-- FpgaBee
--
-- Copyright (C) 2012-2013 Topten Software.
-- All Rights Reserved
-- 
-- Licensed under the Apache License, Version 2.0 (the "License"); you may not use this 
-- product except in compliance with the License. You may obtain a copy of the License at
-- 
-- http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software distributed under 
-- the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF 
-- ANY KIND, either express or implied. See the License for the specific language governing 
-- permissions and limitations under the License.


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
use work.DiskConstants.ALL;
 
entity DiskControllerSelector is
	port
	(
		-- Clocking
		reset : in std_logic;
		clock_108_000 : in std_logic;								-- CPU Clock
		clken_3_375 : in std_logic;								-- CPU Clock enable

		-- Selector
		sel_wd1793 : in std_logic;
		sel_wd1002 : in std_logic;

		-- CPU Interface
		cpu_port : in std_logic_vector(3 downto 0);	
		cpu_wr : in std_logic;						
		cpu_rd : in std_logic;							
		cpu_din : in std_logic_vector(7 downto 0);		
		cpu_dout : out std_logic_vector(7 downto 0);	

		debugA : out std_logic_vector(31 downto 0);
		debugS : out std_logic_vector(7 downto 0);

		-- VDC Interace
		cmd : out std_logic_vector(3 downto 0);			
		cmd_din : out std_logic_vector(7 downto 0);
		cmd_dout : in std_logic_vector(7 downto 0);
		cmd_track : out std_logic_vector(15 downto 0);
		cmd_drive : out std_logic_vector(2 downto 0);
		cmd_head : out std_logic_vector(2 downto 0);
		cmd_sector : out std_logic_vector(7 downto 0);
		cmd_status_busy : in std_logic;
		cmd_status_error : in std_logic;
		cmd_status_geo_error : in std_logic;
		cmd_status_track_error : in std_logic;
		cmd_status_nodisk : in std_logic;
		cmd_flag_buffer : in std_logic
	);
end DiskControllerSelector;


architecture behavior of DiskControllerSelector is 

	signal a_cpu_wr : std_logic;
	signal a_cpu_dout : std_logic_vector(7 downto 0);
	signal a_cmd : std_logic_vector(3 downto 0);
	signal a_cmd_din : std_logic_vector(7 downto 0);
	signal a_cmd_track : std_logic_vector(15 downto 0);
	signal a_cmd_drive : std_logic_vector(2 downto 0);
	signal a_cmd_head : std_logic_vector(2 downto 0);
	signal a_cmd_sector : std_logic_vector(7 downto 0);
	signal a_debugA : std_logic_vector(31 downto 0);
	signal a_debugS : std_logic_vector(7 downto 0);

	signal b_cpu_wr : std_logic;
	signal b_cpu_dout : std_logic_vector(7 downto 0);
	signal b_cmd : std_logic_vector(3 downto 0);
	signal b_cmd_din : std_logic_vector(7 downto 0);
	signal b_cmd_track : std_logic_vector(15 downto 0);
	signal b_cmd_drive : std_logic_vector(2 downto 0);
	signal b_cmd_head : std_logic_vector(2 downto 0);
	signal b_cmd_sector : std_logic_vector(7 downto 0);
	signal b_debugA : std_logic_vector(31 downto 0);
	signal b_debugS : std_logic_vector(7 downto 0);

begin

	process (
		sel_wd1793, sel_wd1002,
		cpu_wr,
		a_cpu_dout, b_cpu_dout,
		a_cmd, b_cmd,
		a_cmd_din, b_cmd_din,
		a_cmd_track, b_cmd_track,
		a_cmd_drive, b_cmd_drive,
		a_cmd_head, b_cmd_head,
		a_cmd_sector, b_cmd_sector,
		a_debugA, b_debugA,
		a_debugS, b_debugS
		)
	begin
		if sel_wd1002 = '1' then
			a_cpu_wr <= cpu_wr;
			b_cpu_wr <= '0';
			cpu_dout <= a_cpu_dout;
			cmd <= a_cmd;
			cmd_din <= a_cmd_din;
			cmd_track <= a_cmd_track;
			cmd_drive <= a_cmd_drive;
			cmd_head <= a_cmd_head;
			cmd_sector <= a_cmd_sector;
			debugA <= a_debugA;
			debugS <= a_debugS;
		elsif sel_wd1793 = '1' then
			a_cpu_wr <= '0';
			b_cpu_wr <= cpu_wr;
			cpu_dout <= b_cpu_dout;
			cmd <= b_cmd;
			cmd_din <= b_cmd_din;
			cmd_track <= b_cmd_track;
			cmd_drive <= b_cmd_drive;
			cmd_head <= b_cmd_head;
			cmd_sector <= b_cmd_sector;
			debugA <= b_debugA;
			debugS <= b_debugS;
		else
			a_cpu_wr <= '0';
			b_cpu_wr <= '0';
			cpu_dout <= (others => '0');
			cmd <= (others => '0');
			cmd_din <= (others => '0');
			cmd_track <= (others => '0');
			cmd_drive <= (others => '0');
			cmd_head <= (others => '0');
			cmd_sector <= (others => '0');
			debugA <= (others => '0');
			debugS <= (others => '0');
		end if;
	end process;

	-- WD1002 Disk controller
	DiskControllerWD1002 : entity work.DiskControllerWD1002
	PORT MAP
	(
		reset => reset,
		clock_108_000 => clock_108_000,
		clken_3_375 => clken_3_375,
		cpu_port => cpu_port,
		cpu_din => cpu_din,
		cpu_dout => a_cpu_dout,
		cpu_wr => a_cpu_wr,
		cpu_rd => cpu_rd,

		debugA => a_debugA,
		debugS => a_debugS,

		cmd => a_cmd,
		cmd_din => a_cmd_din,
		cmd_dout => cmd_dout,
		cmd_track => a_cmd_track,
		cmd_drive => a_cmd_drive,
		cmd_head => a_cmd_head,
		cmd_sector => a_cmd_sector,
		cmd_status_busy => cmd_status_busy,
		cmd_status_error => cmd_status_error,
		cmd_status_geo_error => cmd_status_geo_error,
		cmd_status_nodisk => cmd_status_nodisk,
		cmd_status_track_error => cmd_status_track_error,
		cmd_flag_buffer => cmd_flag_buffer
	);

	-- WD1793 Disk controller
	DiskControllerWD1793 : entity work.DiskControllerWD1793
	PORT MAP
	(
		reset => reset,
		clock_108_000 => clock_108_000,
		clken_3_375 => clken_3_375,
		cpu_port => cpu_port,
		cpu_din => cpu_din,
		cpu_dout => b_cpu_dout,
		cpu_wr => b_cpu_wr,
		cpu_rd => cpu_rd,

		debugA => b_debugA,
		debugS => b_debugS,

		cmd => b_cmd,
		cmd_din => b_cmd_din,
		cmd_dout => cmd_dout,
		cmd_track => b_cmd_track,
		cmd_drive => b_cmd_drive,
		cmd_head => b_cmd_head,
		cmd_sector => b_cmd_sector,
		cmd_status_busy => cmd_status_busy,
		cmd_status_error => cmd_status_error,
		cmd_status_geo_error => cmd_status_geo_error,
		cmd_status_track_error => cmd_status_track_error,
		cmd_status_nodisk => cmd_status_nodisk,
		cmd_flag_buffer => cmd_flag_buffer
	);


end;
