-- FpgaBee
--
-- Copyright (C) 2012-2013 Topten Software.
-- All Rights Reserved
-- 
-- Licensed under the Apache License, Version 2.0 (the "License"); you may not use this 
-- product except in compliance with the License. You may obtain a copy of the License at
-- 
-- http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software distributed under 
-- the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF 
-- ANY KIND, either express or implied. See the License for the specific language governing 
-- permissions and limitations under the License.


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
use work.DiskConstants.ALL;
 
entity DiskControllerWD1793 is
	port
	(
		-- Clocking
		reset : in std_logic;
		clock_108_000 : in std_logic;								-- CPU Clock
		clken_3_375 : in std_logic;								-- CPU Clock enable

		-- CPU Interface
		cpu_port : in std_logic_vector(3 downto 0);			-- low 4 bits of the z80 port
		cpu_wr : in std_logic;								-- write signal
		cpu_rd : in std_logic;								-- read signal
		cpu_din : in std_logic_vector(7 downto 0);			-- output from z80, input to controller
		cpu_dout : out std_logic_vector(7 downto 0);		-- output from controller, input to z80

		debugA : out std_logic_vector(31 downto 0);
		debugS : out std_logic_vector(7 downto 0);

		-- VDC Interace
		cmd : out std_logic_vector(3 downto 0);			-- VDC_CMD_xxx commands
		cmd_din : out std_logic_vector(7 downto 0);
		cmd_dout : in std_logic_vector(7 downto 0);
		cmd_track : out std_logic_vector(15 downto 0);
		cmd_drive : out std_logic_vector(2 downto 0);
		cmd_head : out std_logic_vector(2 downto 0);
		cmd_sector : out std_logic_vector(7 downto 0);
		cmd_status_busy : in std_logic;
		cmd_status_error : in std_logic;
		cmd_status_geo_error : in std_logic;
		cmd_status_nodisk : in std_logic;
		cmd_status_track_error : in std_logic;
		cmd_flag_buffer : in std_logic
	);
end DiskControllerWD1793;


architecture behavior of DiskControllerWD1793 is 

	type states is 
	(
		STATE_IDLE,
		STATE_EXEC_CMD,
		STATE_STEP,
		STATE_VERIFY_IF_REQUESTED,
		STATE_VERIFY,
		STATE_PREPARE_DISK_READ,
		STATE_WAIT_FOR_VDC1,
		STATE_WAIT_FOR_VDC2,
		STATE_DATA_THROTTLE,
		STATE_INITIAL_READ_DELAY,
		STATE_START_READ_SECTOR,
		STATE_START_READ_SECTORID,
		STATE_STREAM_READ_DATA,
		STATE_STREAM_READ_FINISHED,
		STATE_SUBSEQUENT_SECTOR_READ,
		STATE_PREPARE_DISK_WRITE,
		STATE_INITIAL_WRITE_DELAY,
		STATE_STREAM_WRITE_DATA,
		STATE_STREAM_WRITE_FINISHED,
		STATE_INITIAL_WRITE_TRACK_DELAY,
		STATE_STREAM_WRITE_TRACK_DATA,
		STATE_WAIT_FOR_TRACK_PARSER_RESPONSE,
		STATE_PROCESS_TRACK_PARSER_RESPONSE,
		STATE_TRACK_SECTOR_WRITTEN,
		STATE_COMMAND_COMPLETE
	);

	constant SysFlagNoVideoColor : natural := 0;
	constant SysFlagNoVideoAttr : natural := 1;
	constant SysFlagNoTapeMonitor : natural := 2;
	constant SysFlagNoSound : natural := 3;
	constant SysFlagDirectTapeMonitor : natural := 4;
	constant SysFlagWD1793 : natural := 5;
	constant SysFlagStatusIndicators : natural := 6;

	-- Common status bits
    constant Status_Busy : natural := 0;
    constant Status_CrcError : natural := 3;
    constant Status_WriteProtected : natural := 6;
    constant Status_NotReady : natural := 7;

    -- T1 status bits
    constant Status_IndexPulse : natural := 1;
    constant Status_Track0 : natural := 2;
    constant Status_SeekError : natural := 4;
    constant Status_HeadLoaded : natural := 5;

    -- T2 status bits
    constant Status_DataRequest : natural := 8;
    constant Status_LostData : natural := 9;
    constant Status_RecordNotFound : natural := 10;
    constant Status_RecordType : natural := 11;

	-- Edge detection the for port read/write port signals
	signal cpu_wr_prev : std_logic;
	signal cpu_rd_prev : std_logic;

	signal reg_sector : unsigned(7 downto 0);
	signal reg_track : unsigned(7 downto 0);
	signal reg_status : std_logic_vector(11 downto 0);
	signal reg_data : std_logic_vector(7 downto 0);

	signal status_resolved : std_logic_vector(7 downto 0);

	signal reg_cmd : std_logic_vector(7 downto 0);
	signal reg_drive : unsigned(1 downto 0);
	signal reg_head : unsigned(0 downto 0);
	signal reg_mfm : std_logic;
	signal reg_stepDirIn : std_logic;

	signal reg_irq : std_logic;

	type T_PHYSICAL_TRACK_POSITIONS is array (0 to 3) of unsigned(7 downto 0);
	signal physical_track_positions : T_PHYSICAL_TRACK_POSITIONS;

	signal physical_track : unsigned(7 downto 0);

	signal state : states := STATE_IDLE;
	signal state_next : states := STATE_IDLE;

	signal byte_throttle_divider : unsigned(6 downto 0);
	signal data_stream_delay : unsigned(6 downto 0);
	signal vdc_buffer_full : std_logic;

	signal track_parser_cs : std_logic;
	signal track_parser_we : std_logic;
	signal track_parser_dout : std_logic_vector(7 downto 0);
	signal track_parser_dout_we : std_logic;
	signal track_parser_sector_detected : std_logic;
	signal track_parser_sector_complete : std_logic;
	signal track_parser_sector_id_track : std_logic_vector(7 downto 0);
	signal track_parser_sector_id_side : std_logic_vector(7 downto 0);
	signal track_parser_sector_id_seclen : std_logic_vector(7 downto 0);
	signal track_parser_sector_id_sector : std_logic_vector(7 downto 0);
	signal track_parser_sector_count : unsigned(7 downto 0);
	signal track_bytes_remaining : unsigned(12 downto 0);


begin

	-- Decode registers
	cmd_drive <= "1" & std_logic_vector(reg_drive);
	cmd_track <= "00000000" & std_logic_vector(physical_track);
	cmd_head <= "00" & std_logic_vector(reg_head);

	cmd_sector <= std_logic_vector(reg_sector)
					when track_parser_cs = '0'
					else track_parser_sector_id_sector;

	physical_track <= physical_track_positions(to_integer(reg_drive));

	reg_status(Status_Busy) <= '0' when state = STATE_IDLE else '1';
	reg_status(Status_Track0) <= '1' when physical_track=to_unsigned(0, physical_track'length) else '0';

	-- Resolve status depending on last command
	status_resolved <=
						reg_status(Status_NotReady) &
						reg_status(Status_WriteProtected) &
						reg_status(Status_HeadLoaded) &
						reg_status(Status_SeekError) &
						reg_status(Status_CrcError) &
						reg_status(Status_Track0) &
						reg_status(Status_IndexPulse) & 
						reg_status(Status_Busy)

					when reg_cmd(7) ='0' else

						reg_status(Status_NotReady) &
						reg_status(Status_WriteProtected) &
						reg_status(Status_RecordType) &
						reg_status(Status_RecordNotFound) &
						reg_status(Status_CrcError) &
						reg_status(Status_LostData) &
						reg_status(Status_DataRequest) & 
						reg_status(Status_Busy);

	debugA <= (others => '0');
	debugS <= (others => '0');


	-- Front end CPU interface
	process (clock_108_000)
	begin

		if rising_edge(clock_108_000) then
		if reset = '1' then

			cpu_wr_prev <= '0';
			cpu_rd_prev <= '0';
			reg_drive <= to_unsigned(0, reg_drive'length);
			reg_head <= to_unsigned(0, reg_head'length);
			reg_sector <= to_unsigned(0, reg_sector'length);
			reg_track <= to_unsigned(0, reg_track'length);
			reg_data <= (others => '0');
			reg_stepDirIn <= '0';
			physical_track_positions(0) <= to_unsigned(0, 8);
			physical_track_positions(1) <= to_unsigned(0, 8);
			physical_track_positions(2) <= to_unsigned(0, 8);
			physical_track_positions(3) <= to_unsigned(0, 8);
			state <= STATE_IDLE;
		    reg_status(Status_CrcError) <= '0';
		    reg_status(Status_WriteProtected) <= '0';
		    reg_status(Status_NotReady) <= '0';
		    reg_status(Status_IndexPulse) <= '0';
		    reg_status(Status_SeekError) <= '0';
		    reg_status(Status_HeadLoaded) <= '0';
		    reg_status(Status_DataRequest) <= '0';
		    reg_status(Status_LostData) <= '0';
		    reg_status(Status_RecordNotFound) <= '0';
		    reg_status(Status_RecordType) <= '0';

			track_parser_cs <= '0';

		elsif clken_3_375='1' then

			-- Track read/write edges
			cpu_wr_prev <= cpu_wr;
			cpu_rd_prev <= cpu_rd;

			-- Defaults
			cmd <= VDC_CMD_NOP;
			track_parser_we <= '0';

			-- Convert this flag to a signal
			if cmd_flag_buffer='1' then

				vdc_buffer_full <= '1';

			end if;

			-- Exec state
			case state is

				when STATE_IDLE => 
					track_parser_cs <= '0';
					null;

				when STATE_EXEC_CMD =>

					if reg_cmd(7 downto 4) = "0000" then

						-- COMMAND: Restore

						-- Load/unload head
						reg_status(Status_HeadLoaded) <= reg_cmd(3);

						if physical_track = to_unsigned(0, physical_track'length) then

							-- Found track zero
							reg_track <= to_unsigned(0, reg_track'length);
							state <= STATE_VERIFY_IF_REQUESTED;

						else
							
							-- Step out
							reg_stepDirIn <= '0';
							state <= STATE_STEP;
							state_next <= STATE_EXEC_CMD;
									
						end if;


					elsif reg_cmd(7 downto 4) = "0001" then

						-- COMMAND : Seek

						-- Load/unload head
						reg_status(Status_HeadLoaded) <= reg_cmd(3);

						if reg_track = unsigned(reg_data) then

							-- On the right track, verify it
							state <= STATE_VERIFY_IF_REQUESTED;

						else
							
							-- Work out direction
							if unsigned(reg_data) > reg_track then
								reg_stepDirIn <= '1';
							else
								reg_stepDirIn <= '0';
							end if;

							-- Step
							state <= STATE_STEP;
							state_next <= STATE_EXEC_CMD;
									
						end if;

					elsif reg_cmd(7 downto 5) = "001" then

						-- COMMAND: Step

						reg_status(Status_HeadLoaded) <= reg_cmd(3);
						state <= STATE_STEP;
						state_next <= STATE_VERIFY_IF_REQUESTED;


					elsif reg_cmd(7 downto 5) = "010" then 

						-- COMMAND: Step In
 
						reg_status(Status_HeadLoaded) <= reg_cmd(3);
						reg_stepDirIn <= '1';
						state <= STATE_STEP;
						state_next <= STATE_VERIFY_IF_REQUESTED;

					elsif reg_cmd(7 downto 5) = "011" then

						-- COMMAND: Step Out

						reg_status(Status_HeadLoaded) <= reg_cmd(3);
						reg_stepDirIn <= '0';
						state <= STATE_STEP;
						state_next <= STATE_VERIFY_IF_REQUESTED;

					elsif reg_cmd(7 downto 5) = "100" then

						-- COMMAND: Read sector

						state <= STATE_PREPARE_DISK_READ;
						state_next <= STATE_START_READ_SECTOR;

					elsif reg_cmd(7 downto 5) = "101" then

						-- COMMAND: Write sector

						cmd <= VDC_CMD_RESET_BYTE_PTR;
						state <= STATE_PREPARE_DISK_WRITE;
						data_stream_delay <= to_unsigned(20, data_stream_delay'length);

					elsif reg_cmd(7 downto 4) = "1100" then

						-- COMMAND: Read address

						state <= STATE_PREPARE_DISK_READ;
						state_next <= STATE_START_READ_SECTORID;



					elsif reg_cmd(7 downto 4) = "1110" then

						-- Read track

					elsif reg_cmd(7 downto 4) = "1111" then

						-- Write Track

						-- Check track number is valid
						if physical_track /= reg_track or cmd_status_track_error = '1' then

							-- Nope, abort
							reg_status(Status_RecordNotFound) <= '1';
							state <= STATE_COMMAND_COMPLETE;

						else

							state <= STATE_INITIAL_WRITE_TRACK_DELAY;
							data_stream_delay <= to_unsigned(98, data_stream_delay'length);
							track_parser_cs <= '1';

						end if;





					end if;

				when STATE_STEP =>

					if reg_stepDirIn = '1' then

						-- Step physical track
						if physical_track /= to_unsigned(255, physical_track'length) then

							physical_track_positions(to_integer(reg_drive)) <= physical_track + 1;

						end if;

						-- Adjust track register
						if reg_cmd(4) ='1' and reg_track /= to_unsigned(255, reg_track'length)  then

							reg_track <= reg_track + 1;

						end if;

					else

						-- Step physical track
						if physical_track /= to_unsigned(0, physical_track'length) then

							physical_track_positions(to_integer(reg_drive)) <= physical_track - 1;

						end if;

						-- Adjust track register
						if reg_cmd(4) ='1' and reg_track /= to_unsigned(0, reg_track'length)  then

							reg_track <= reg_track - 1;

						end if;

					end if;

					-- Next state
					state <= state_next;

				when STATE_VERIFY_IF_REQUESTED =>
					
					-- Verify
					if reg_cmd(2) ='1' then

						-- Do the verification
						state <= STATE_VERIFY;

					else

						-- Finshed operation
						state <= STATE_COMMAND_COMPLETE;

					end if;


				when STATE_VERIFY =>

					-- Check if physical track matches track register
					-- and that the track number is valid (we'll use the VDC to verify)
					if physical_track /= reg_track or cmd_status_track_error = '1' then
						reg_status(Status_SeekError) <= '1';
					end if;

					-- Finished operation
					state <= STATE_COMMAND_COMPLETE;


				when STATE_PREPARE_DISK_READ => 

					if cmd_status_nodisk = '1' 
						or physical_track /= reg_track 
						or reg_mfm = '0' then

						reg_status(Status_RecordNotFound) <= '1';
						state <= STATE_COMMAND_COMPLETE;

					else

						state <= state_next;

					end if;

				when STATE_WAIT_FOR_VDC1 =>

					state <= STATE_WAIT_FOR_VDC2;

				when STATE_WAIT_FOR_VDC2 =>

					vdc_buffer_full <= '0';

					-- Wait for the VDC to complete operation
					if cmd_status_busy = '0' then

						state <= state_next;

					end if;	

				when STATE_DATA_THROTTLE =>

					if byte_throttle_divider = to_unsigned(0, byte_throttle_divider'length) then
						state <= state_next;
					else
						byte_throttle_divider <= byte_throttle_divider - 1;
					end if;

				when STATE_INITIAL_READ_DELAY =>

					if data_stream_delay = to_unsigned(0, data_stream_delay'length) then

						-- Was there an error?
						if cmd_status_error='1' 	
								or cmd_status_geo_error='1' 
								or cmd_status_nodisk='1' 
								or cmd_status_track_error = '1' then

							reg_status(Status_RecordNotFound) <= '1';
							state <= STATE_COMMAND_COMPLETE;

						else

							-- Initial delay has finished, start streaming the data
							state <= STATE_STREAM_READ_DATA;

						end if;

					else

						-- Wait another byte
						state <= STATE_DATA_THROTTLE;
						state_next <= STATE_INITIAL_READ_DELAY;
						byte_throttle_divider <= to_unsigned(105, byte_throttle_divider'length);
						data_stream_delay <= data_stream_delay - 1;

					end if;


				when STATE_START_READ_SECTOR => 

					cmd <= VDC_CMD_READ_BLOCK;
					state <= STATE_WAIT_FOR_VDC1;
					state_next <= STATE_INITIAL_READ_DELAY;
					data_stream_delay <= to_unsigned(20, data_stream_delay'length);

				when STATE_START_READ_SECTORID => 

					cmd <= VDC_CMD_READ_SECTORID_FIELD;
					state <= STATE_WAIT_FOR_VDC1;
					state_next <= STATE_INITIAL_READ_DELAY;
					data_stream_delay <= to_unsigned(18, data_stream_delay'length);

				when STATE_STREAM_READ_DATA => 

					-- Lost data?
					if reg_status(Status_DataRequest) = '1' then
						reg_status(Status_LostData) <= '1';
					end if;

					if vdc_buffer_full = '1' then

						-- Read finished
						state <= STATE_STREAM_READ_FINISHED;

					else

						-- Tell VDC we want the next byte
						cmd <= VDC_CMD_READ_BYTE;		

						-- Make data available
						reg_status(Status_DataRequest) <= '1';
						reg_data <= cmd_dout;

						-- Wait for one byte time		
						state <= STATE_DATA_THROTTLE;
						byte_throttle_divider <= to_unsigned(107, byte_throttle_divider'length);

						-- Continue streaming
						state_next <= STATE_STREAM_READ_DATA;
						
					end if;

				when STATE_STREAM_READ_FINISHED =>

					-- End of operation

					if reg_cmd(4)='1' then

						-- Multiple sector read, start next operation
						reg_sector <= reg_sector + 1;
						state <= STATE_PREPARE_DISK_READ;
						state_next <= STATE_SUBSEQUENT_SECTOR_READ;

					else

						-- End of operation
						state <= STATE_COMMAND_COMPLETE;

					end if;

				when STATE_SUBSEQUENT_SECTOR_READ => 

					-- Start the next sector read in a multi-byte read operation
					cmd <= VDC_CMD_READ_BLOCK;
					state <= STATE_WAIT_FOR_VDC1;
					state_next <= STATE_INITIAL_READ_DELAY;
					data_stream_delay <= to_unsigned(105, data_stream_delay'length);


				when STATE_PREPARE_DISK_WRITE => 

					-- Invoke a geo test
					cmd <= VDC_CMD_GEO_TEST;
					state <= STATE_WAIT_FOR_VDC1;
					state_next <= STATE_INITIAL_WRITE_DELAY;


				when STATE_INITIAL_WRITE_DELAY =>

					if data_stream_delay = to_unsigned(0, data_stream_delay'length) then

						-- Initial delay has finished, start streaming the data

						-- Check the result of the geo test
						if cmd_status_error='1' 	
								or physical_track /= reg_track 
								or reg_mfm = '0'
								or cmd_status_geo_error='1' 
								or cmd_status_nodisk='1' 
								or cmd_status_track_error = '1' then

							-- Bail!
							reg_status(Status_RecordNotFound) <= '1';
							state <= STATE_COMMAND_COMPLETE;

						else

							-- Flag that we're ready for data
							reg_status(Status_DataRequest) <= '1';
							reg_data <= (others => '0');

							-- Wait for the data
							state <= STATE_DATA_THROTTLE;
							state_next <= STATE_STREAM_WRITE_DATA;
							byte_throttle_divider <= to_unsigned(105, byte_throttle_divider'length);

						end if;

					else

						-- Wait another byte
						state <= STATE_DATA_THROTTLE;
						state_next <= STATE_INITIAL_WRITE_DELAY;
						byte_throttle_divider <= to_unsigned(105, byte_throttle_divider'length);
						data_stream_delay <= data_stream_delay - 1;

					end if;

				when STATE_STREAM_WRITE_DATA => 


					if vdc_buffer_full = '1' then

						-- Write Finished
						cmd <= VDC_CMD_WRITE_BLOCK;
						state <= STATE_WAIT_FOR_VDC1;
						state_next <= STATE_STREAM_WRITE_FINISHED;

					else

						-- Data lost?
						if reg_status(Status_DataRequest) = '1' then 

							reg_status(Status_LostData) <= '1';

						end if;

						-- Send the byte to the virtual disk controller
						cmd <= VDC_CMD_WRITE_BYTE;
						cmd_din <= reg_data;
						
						-- Flag that we're ready for more data
						reg_status(Status_DataRequest) <= '1';
						reg_data <= (others => '0');

						-- Wait for the data
						state <= STATE_DATA_THROTTLE;
						state_next <= STATE_STREAM_WRITE_DATA;
						byte_throttle_divider <= to_unsigned(105, byte_throttle_divider'length);

					end if;

				when STATE_STREAM_WRITE_FINISHED =>

					-- Block has been written
					-- NB: We don't actually check if the write failed!

					if reg_cmd(4)='1' then

						-- Multiple sector write, start next operation
						reg_sector <= reg_sector + 1;
						state <= STATE_PREPARE_DISK_WRITE;
						data_stream_delay <= to_unsigned(100, data_stream_delay'length);				

					else

						-- End of operation
						state <= STATE_COMMAND_COMPLETE;

					end if;

				when STATE_INITIAL_WRITE_TRACK_DELAY =>

					if data_stream_delay = to_unsigned(0, data_stream_delay'length) then

						-- We only support double density so 6250 bytes for the entire track
						track_bytes_remaining <= to_unsigned(6249, track_bytes_remaining'length);

						-- Flag that we're ready for data
						reg_status(Status_DataRequest) <= '1';
						reg_data <= (others => '0');

						-- Wait for the data
						state <= STATE_DATA_THROTTLE;
						state_next <= STATE_STREAM_WRITE_TRACK_DATA;
						byte_throttle_divider <= to_unsigned(105, byte_throttle_divider'length);


					else

						-- Wait another byte
						state <= STATE_DATA_THROTTLE;
						state_next <= STATE_INITIAL_WRITE_TRACK_DELAY;
						byte_throttle_divider <= to_unsigned(105, byte_throttle_divider'length);
						data_stream_delay <= data_stream_delay - 1;

					end if;

				when STATE_STREAM_WRITE_TRACK_DATA =>

					-- Data lost?
					if reg_status(Status_DataRequest) = '1' then 
						reg_status(Status_LostData) <= '1';
					end if;

					-- Entire track processed yet?
					if track_bytes_remaining = to_unsigned(0, track_bytes_remaining'length) then

						-- Yep, finish up
						state <= STATE_COMMAND_COMPLETE;

					else 

						-- Pass it to the track parser
						track_parser_we <= '1';
						state <= STATE_WAIT_FOR_TRACK_PARSER_RESPONSE;

						-- Keep track of how many bytes we've processed
						track_bytes_remaining <= track_bytes_remaining - 1;

					end if;

				when STATE_WAIT_FOR_TRACK_PARSER_RESPONSE =>
					state <= STATE_PROCESS_TRACK_PARSER_RESPONSE;

				when STATE_PROCESS_TRACK_PARSER_RESPONSE =>

					-- If new sector found, then reset the virtual 
					-- disk buffer in prep for the sector data
					if track_parser_sector_detected = '1' then
						cmd <= VDC_CMD_RESET_BYTE_PTR;
					end if;

					-- If track data then pass to virtual disk controller
					if track_parser_dout_we = '1' then
						cmd <= VDC_CMD_WRITE_BYTE;
						cmd_din <= track_parser_dout;
					end if;

					-- Sector complete?
					if track_parser_sector_complete = '1' then

						-- Sector complete, write it
						if track_parser_sector_id_track = std_logic_vector(physical_track) and
							track_parser_sector_id_side = "0000000" & std_logic_vector(reg_head) then

							-- But only if the sector id matches the current track and head
							cmd <= VDC_CMD_WRITE_BLOCK;
							state <= STATE_WAIT_FOR_VDC1;
							state_next <= STATE_TRACK_SECTOR_WRITTEN;

						else

							-- This means host is writing the incorrect 
							-- sector id for the current track/head
							-- We can't record this with raw disks so just ignore it
							state <= STATE_TRACK_SECTOR_WRITTEN;
							
						end if;

					else

						-- Flag that we're ready for more data
						reg_status(Status_DataRequest) <= '1';
						reg_data <= (others => '0');

						-- Wait for it
						state <= STATE_DATA_THROTTLE;
						state_next <= STATE_STREAM_WRITE_TRACK_DATA;
						byte_throttle_divider <= to_unsigned(105, byte_throttle_divider'length);

					end if;

				when STATE_TRACK_SECTOR_WRITTEN => 

					-- Flag that we're ready for more data
					reg_status(Status_DataRequest) <= '1';
					reg_data <= (others => '0');

					-- Wait for it
					state <= STATE_DATA_THROTTLE;
					state_next <= STATE_STREAM_WRITE_TRACK_DATA;
					byte_throttle_divider <= to_unsigned(105, byte_throttle_divider'length);


				when STATE_COMMAND_COMPLETE => 

					-- Finshed operation
					reg_status(Status_DataRequest) <= '0';
					state <= STATE_IDLE;
					reg_irq <= '1';

			end case;

			-- Port write?
			if cpu_wr='1' and cpu_wr_prev='0' then

				if cpu_port(3) = '0' then
					
					case cpu_port(1 downto 0) is

						when "00" => 
							-- Command
							if cpu_din(7 downto 4) = "1101" then

								-- Special handling for ForceInterrupt
								reg_irq <= '1';
								reg_status(Status_DataRequest) <= '0';
								state <= STATE_IDLE;

							elsif state = STATE_IDLE then

								reg_irq <= '0';
								reg_cmd <= cpu_din;

								state <= STATE_EXEC_CMD;
								reg_status(Status_CrcError) <= '0';
							    reg_status(Status_WriteProtected) <= '0';
							    reg_status(Status_NotReady) <= '0';
							    reg_status(Status_IndexPulse) <= '0';
							    reg_status(Status_SeekError) <= '0';
							    reg_status(Status_HeadLoaded) <= '0';
							    reg_status(Status_DataRequest) <= '0';
							    reg_status(Status_LostData) <= '0';
							    reg_status(Status_RecordNotFound) <= '0';
							    reg_status(Status_RecordType) <= '0';


							end if;

						when "01" => 
							-- Write track
							reg_track <= unsigned(cpu_din);

						when "10" => 
							-- Write sector
							reg_sector <= unsigned(cpu_din);

						when "11" => 
							-- Write data
							reg_data <= cpu_din;
							reg_status(Status_DataRequest) <= '0';

						when others => 
							null;

					end case;

				else
					
					reg_drive <= unsigned(cpu_din(1 downto 0));
					reg_head <= unsigned(cpu_din(2 downto 2));
					reg_mfm <= cpu_din(3);

				end if;

			elsif cpu_rd='1' and cpu_rd_prev='0' then

				if cpu_port(3) = '0' then
					
					case cpu_port(1 downto 0) is

						when "00" => 
							-- Read state
							reg_irq <= '0';
							cpu_dout <= status_resolved;

						when "01" => 
							-- Read track
							cpu_dout <= std_logic_vector(reg_track);

						when "10" => 
							-- Read sector
							cpu_dout <= std_logic_vector(reg_sector);

						when "11" => 
							-- Read data
							reg_status(Status_DataRequest) <= '0';
							cpu_dout <= reg_data;

						when others => 
							cpu_dout <= (others => '0');

					end case;

				else

					cpu_dout <= (reg_irq or reg_status(Status_DataRequest)) & "0000000";

				end if;

			end if;

		end if;
		end if;
	end process;

	TrackParser : entity work.TrackParser
	PORT MAP
	(
		clock_108_000 => clock_108_000,
		clken_3_375 => clken_3_375,
		reset => reset,
		cs => track_parser_cs,
		we => track_parser_we,
		din => reg_data,
		dout => track_parser_dout,
		dout_we => track_parser_dout_we,
		sector_detected => track_parser_sector_detected,
		sector_complete => track_parser_sector_complete,
		sector_id_track => track_parser_sector_id_track,
		sector_id_side => track_parser_sector_id_side,
		sector_id_seclen => track_parser_sector_id_seclen,
		sector_id_sector => track_parser_sector_id_sector,
		sector_count => track_parser_sector_count
	);


end;
