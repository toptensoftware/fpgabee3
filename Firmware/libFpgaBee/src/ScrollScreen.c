//////////////////////////////////////////////////////////////////////////////
// libFpgaBee

#include "common.h"

void ScrollScreen(uint8_t left, uint8_t top, uint8_t width, uint8_t height, 
				int dy, bool attr,
				uint8_t* pRedrawFrom, uint8_t* pRedrawCount)
{
	uint8_t i,first, last;
	int delta;

	// Redundant
	if (dy==0)
	{
		*pRedrawFrom = 0;
		*pRedrawCount = 0;
		return;
	}

	if (dy < 0)
	{
		// Scroll up
		if (-dy > height)
		{
			*pRedrawFrom = 0;
			*pRedrawCount = height;
			return;
		}

		first = top - dy;
		last = top + height;
		delta = 1;
		*pRedrawFrom = height + dy;
		*pRedrawCount = -dy;
	}
	else
	{
		// Scroll down
		if (dy > height)
		{
			*pRedrawFrom = 0;
			*pRedrawCount = height;
			return;
		}

		last = top - 1;
		first = top + height - dy - 1;
		delta = -1;
		*pRedrawFrom = 0;
		*pRedrawCount = dy;
	}

	for (i=first; i!=last; i+=delta)
	{
		char* pSrc = VideoCharBuffer + i * SCREEN_WIDTH + left;
		char* pDest = VideoCharBuffer + (i + dy) * SCREEN_WIDTH + left;

		// Move characters
		memcpy(pDest, pSrc, width);

		// Move attributes
		if (attr)
		{
			memcpy(pDest + (VideoAttrBuffer - VideoCharBuffer),
					pSrc + (VideoAttrBuffer - VideoCharBuffer), 
					width);
		}
	}
}
