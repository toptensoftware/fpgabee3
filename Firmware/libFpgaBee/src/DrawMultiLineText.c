//////////////////////////////////////////////////////////////////////////////
// libFpgaBee

#include "common.h"
#include <string.h>

void MeasureMultiLineText(const char* psz, POINT* pVal)
{
	int lineWidth = 0;
	pVal->x = 0;
	pVal->y = 0;


	while (psz[0])
	{
		if (psz[0]=='\n')
		{
			if (lineWidth > pVal->x)
				pVal->x = lineWidth;
			lineWidth = 0;
		}
		else
		{
			if (lineWidth == 0)
				pVal->y++;
			lineWidth++;
		}

		psz++;
	}

	if (lineWidth > pVal->x)
		pVal->x = lineWidth;
}

void DrawMultiLineText(const char* psz, POINT* pPos)
{
	int y = pPos->y;
	char* pDest = VideoCharBuffer + pPos->x + y * SCREEN_WIDTH;
	while (psz[0])
	{
		if (psz[0]=='\n')
		{
			y++;
			pDest = VideoCharBuffer + pPos->x + y * SCREEN_WIDTH;
		}
		else
		{
			*pDest = *psz;
			pDest++;
		}
		psz++;
	}
}