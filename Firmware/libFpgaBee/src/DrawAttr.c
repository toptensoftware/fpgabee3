//////////////////////////////////////////////////////////////////////////////
// libFpgaBee

#include "common.h"
#include <string.h>


void DrawAttr(uint8_t left, uint8_t top, uint8_t width, uint8_t height, uint8_t attr)
{
	uint8_t i;

	// Calculate starting position
	uint8_t* p = VideoAttrBuffer + left + top * SCREEN_WIDTH;

	// Middle
	for (i=0; i<height; i++)
	{
		memset(p, attr, width);
		p += SCREEN_WIDTH;
	}
}
