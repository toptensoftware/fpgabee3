//////////////////////////////////////////////////////////////////////////////
// libFpgaBee

#include "common.h"

static void ListBoxDrawContent(LISTBOX* pListBox, uint8_t from, uint8_t count)
{
	RECT rcClient;
	int item = pListBox->scrollPos + from;
	int itemCount = pListBox->getItemCount(pListBox);
	uint8_t row, endRow;

	GetClientRect(&pListBox->window, &rcClient);
 	row = rcClient.top + from;
	endRow = row + count;

	while (row < endRow)
	{
		// Draw text
		const char* psz = item < itemCount ? pListBox->getItemText(pListBox, item) : "";

		if (psz[0]=='\1')
			memset(VideoCharBuffer + row * SCREEN_WIDTH + rcClient.left, BOX_H, rcClient.width);
		else
			DrawText(rcClient.left, row, rcClient.width, psz, DT_LEFT);

		row++;
		item++;
	}
}

static void ListBoxDrawHighlight(LISTBOX* pListBox)
{
	int highlightedRow = pListBox->selectedItem - pListBox->scrollPos;
	RECT rcClient;
	GetClientRect(&pListBox->window, &rcClient);


	if (highlightedRow >= 0 && highlightedRow < rcClient.height)
	{
		DrawAttr(rcClient.left, highlightedRow + rcClient.top, rcClient.width, 1, GetSelectedAttr(&pListBox->window));
	}
}

void ListBoxDraw(LISTBOX* pListBox)
{
	RECT rcClient;
	GetClientRect(&pListBox->window, &rcClient);
	ListBoxDrawHighlight(pListBox);
	ListBoxDrawContent(pListBox, 0, rcClient.height);
}

void ListBoxDrawItem(LISTBOX* pListBox, int item)
{
	int row = item - pListBox->scrollPos;
	RECT rcClient;
	GetClientRect(&pListBox->window, &rcClient);
	if (row >=0 && row < rcClient.height)
	{
		ListBoxDrawContent(pListBox, row, 1);
	}
}

// Moves the selection and scroll position
static bool ListBoxUpdate(LISTBOX* pListBox, int newSelectedItem, int newScrollPos)
{
	// Remember which row was highlighted
	int oldHighlightedRow, newHighlightedRow;
	int oldSelectedItem = pListBox->selectedItem;
	int oldScrollPos = pListBox->scrollPos;
	RECT rcClient;
	GetClientRect(&pListBox->window, &rcClient);

	// Redundant?
	if (newSelectedItem == oldSelectedItem && newScrollPos == oldScrollPos)
		return false;

	// Store new state
	pListBox->selectedItem = newSelectedItem;
	pListBox->scrollPos = newScrollPos;

	// Scroll
	if (newScrollPos != oldScrollPos)
	{
		uint8_t redrawFrom, redrawCount;

		// Scroll screen
		ScrollScreen(rcClient.left, rcClient.top, rcClient.width, rcClient.height, 
						oldScrollPos - newScrollPos, false, 
						&redrawFrom, &redrawCount
						);

		// Redraw context
		ListBoxDrawContent(pListBox, redrawFrom, redrawCount);
	}

	// Update highlighted row
	oldHighlightedRow = oldSelectedItem - oldScrollPos;
	newHighlightedRow = newSelectedItem - newScrollPos;
	if (oldHighlightedRow != newHighlightedRow)
	{
		// Unhighlight old row
		if (oldHighlightedRow >= 0 && oldHighlightedRow < rcClient.height)
			DrawAttr(rcClient.left, rcClient.top + oldHighlightedRow, rcClient.width, 1, GetNormalAttr(&pListBox->window));

		// Highlight new row
		ListBoxDrawHighlight(pListBox);
	}

	return true;
}

int ListBoxEnsureVisible(LISTBOX* pListBox, int item)
{
	int scrollPos = pListBox->scrollPos;
	RECT rcClient;
	GetClientRect(&pListBox->window, &rcClient);
	if (item >= 0)
	{
		if (item < pListBox->scrollPos)
			scrollPos = item;
		else if (item >= pListBox->scrollPos + rcClient.height)
			scrollPos = item - rcClient.height + 1;
	}
	return scrollPos;
}

// Select listbox item
bool ListBoxSelect(LISTBOX* pListBox, int item)
{
	// Get count
	int scrollPos;
	int count = pListBox->getItemCount(pListBox);

	// Check in range
	if (item < 0)
		item = 0;
	if (item >= count)
		item = count - 1;

	// Scroll visible
	scrollPos = ListBoxEnsureVisible(pListBox, item);

	// Update display
	return ListBoxUpdate(pListBox, item, scrollPos);
}


int lb_GetItemCount(LISTBOX* pListBox)
{
	return pListBox->itemCount;
}

const char* lb_GetItemText(LISTBOX* pListBox, int item)
{
	return pListBox->ppItems[item];
}

void ListBoxSetData(LISTBOX* pListBox, int itemCount, const char** ppItems)
{
	if (itemCount < 0)
	{
		const char** pp = ppItems;
		itemCount = 0;
		while (*pp)
		{
			itemCount++;
			pp++;
		}
	}
	pListBox->getItemCount = lb_GetItemCount;
	pListBox->getItemText = lb_GetItemText;
	pListBox->itemCount = itemCount;
	pListBox->ppItems = ppItems;
}


size_t ListBoxWindowProc(WINDOW* pWindow, MSG* pMsg)
{
	LISTBOX* pListBox = (LISTBOX*)pWindow;
	switch (pMsg->message)
	{
		case MESSAGE_DRAWCONTENT:
			ListBoxDraw(pListBox);
			break;

		case MESSAGE_KEYDOWN:
		{
			int newSel = pListBox->selectedItem;
			switch (pMsg->param1)
			{
				case VK_UP:
					newSel--;
					break;

				case VK_DOWN:
					newSel++;
					break;

				case VK_NEXT:
				{
					RECT rcClient;
					GetClientRect(&pListBox->window, &rcClient);
					newSel += rcClient.height;
					break;
				}

				case VK_PRIOR:
				{
					RECT rcClient;
					GetClientRect(&pListBox->window, &rcClient);
					newSel -= rcClient.height;
					break;
				}

				case VK_HOME:
					newSel = 0;
					break;

				case VK_END:
					newSel = pListBox->getItemCount(pListBox) - 1;
					break;

				case VK_ESCAPE:
					if (pListBox->window.modal)
						EndModal(-1);
					break;

				case VK_ENTER:
					if (pListBox->window.modal)
						EndModal(pListBox->selectedItem);
					break;

				default:
					return false;
			}

			// Select and redraw
			ListBoxSelect(pListBox, newSel);
		}
	}

	return DefWindowProc(pWindow, pMsg);
}
