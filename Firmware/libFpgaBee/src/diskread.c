//////////////////////////////////////////////////////////////////////////////
// libFpgaBee

#include "common.h"

#pragma disable_warning 85 // unreferenced function argument :

bool DiskRead(uint32_t blockNumber, void* buffer)
{
	unsigned char status;

	// Send block number to disk controller
	SetDiskAddress(blockNumber);


	// Send the read command
	PcuDiskCommandPort = DISK_COMMAND_READ;

	// Wait for command to finish
	while (true)
	{
		status = PcuDiskStatusPort;
		if ((status & DISK_STATUS_BIT_BUSY)==0)
			break;

		Yield();
	}


	// Error?
	if ((status & DISK_STATUS_BIT_ERROR)!=0)
		return false;

	__asm

	ld	hl, #6
	add	hl, sp
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
	ld	l, a
	ld  bc,#0x0000 + _PcuDiskDataPort

	inir
	inir
	__endasm;

	return true;
}