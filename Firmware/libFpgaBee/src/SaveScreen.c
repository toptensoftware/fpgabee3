//////////////////////////////////////////////////////////////////////////////
// libFpgaBee

#include "common.h"

typedef struct tagSAVEDATA
{
	uint8_t left;
	uint8_t top;
	uint8_t width;
	uint8_t height;
	uint8_t data[];
} SAVEDATA;

void CopyScreenData(
	void* baseSrc, uint8_t strideSrc, 
	void* baseDest, uint8_t strideDest, 
	uint8_t leftSrc, uint8_t topSrc, uint8_t leftDest, uint8_t topDest,
	uint8_t width, uint8_t height)
{
	uint8_t y;
	uint8_t* pSrc = ((uint8_t*)baseSrc) + leftSrc + topSrc * strideSrc;
	uint8_t* pDest = ((uint8_t*)baseDest) + leftDest + topDest * strideDest;

	for (y=0; y<height; y++)
	{
		memcpy(pDest, pSrc, width);
		pSrc += strideSrc;
		pDest += strideDest;
	}
}


void* SaveScreen(uint8_t left, uint8_t top, uint8_t width, uint8_t height)
{
	SAVEDATA* sd;

	sd = (SAVEDATA*)malloc(sizeof(SAVEDATA) + width * height * 2);
	sd->left = left;
	sd->top = top;
	sd->width = width;
	sd->height = height;
	CopyScreenData(VideoCharBuffer, SCREEN_WIDTH, sd->data, width, left, top, 0, 0, width, height);
	CopyScreenData(VideoAttrBuffer, SCREEN_WIDTH, sd->data + width * height, width, left, top, 0, 0, width, height);
	return sd;
}

void RestoreScreen(void* saveData)
{
	SAVEDATA* sd = (SAVEDATA*)saveData;

	CopyScreenData(sd->data, sd->width, VideoCharBuffer, SCREEN_WIDTH, 0, 0, sd->left, sd->top, sd->width, sd->height);
	CopyScreenData(sd->data + sd->width * sd->height, sd->width, VideoAttrBuffer, SCREEN_WIDTH, 0, 0, sd->left, sd->top, sd->width, sd->height);

	free(sd);
}

