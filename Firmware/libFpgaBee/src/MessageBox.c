//////////////////////////////////////////////////////////////////////////////
// libFpgaBee

#include "common.h"
#include <string.h>

const char* okButtons[] = { "OK", NULL };
const char* okCancelButtons[] = { "OK", "Cancel", NULL };
const char* yesNoButtons[] = { "Yes", "No", NULL };
const char* stopButtons[] = { "Stop", NULL };
const char* cancelButtons[] = { "Cancel", NULL };
const char* stopCancelButtons[] = { "Cancel", NULL };
const char** buttonTexts[] = { okButtons, okCancelButtons, yesNoButtons, stopButtons, cancelButtons, stopCancelButtons };


int ButtonsWidth(int buttons)
{
	const char** ppsz = buttonTexts[buttons];
	int length = 0;
	while (*ppsz)
	{
		length += strlen(*ppsz) + 2;
		ppsz++;
	}
	return length;
}

int ButtonCount(int buttons)
{
	const char** ppsz = buttonTexts[buttons];
	int count = 0;
	while (*ppsz)
	{
		count++;
		ppsz++;
	}
	return count;
}

size_t MessageBoxWindowProc(WINDOW* pWindow, MSG* pMsg)
{
	MESSAGEBOX* pmb = (MESSAGEBOX*)pWindow;
	switch (pMsg->message)
	{
		case MESSAGE_DRAWCONTENT:
		{
			RECT rcClient;
			POINT pos;
			int buttonXPos;
			int button = 0;
			const char** ppszButtons = buttonTexts[pmb->buttons];

			// Draw the message
			GetClientRect(pWindow, &rcClient);
			pos.x = rcClient.left + 1;
			pos.y = rcClient.top + 1;
			DrawMultiLineText(pmb->pszMessage, &pos);

			// Draw the buttons
			buttonXPos = rcClient.left + (rcClient.width - ButtonsWidth(pmb->buttons))/2;
			while (*ppszButtons)
			{
				DrawAttr(buttonXPos, rcClient.top + rcClient.height - 1, strlen(*ppszButtons) + 2, 1, 
						button == pmb->selectedButton ? pWindow->attrSelected : pWindow->attrNormal);
				TextOut(buttonXPos + 1, rcClient.top + rcClient.height - 1, *ppszButtons);
				buttonXPos += 2 + strlen(*ppszButtons);
				ppszButtons++;	
				button++;
			}
			break;
		}

		case MESSAGE_KEYDOWN:
		{
			switch (pMsg->param1)
			{
				case VK_LEFT:
					if (pmb->selectedButton > 0)
					{
						pmb->selectedButton--;
						Invalidate(pWindow);
					}
					break;

				case VK_RIGHT:
				{
					if (pmb->selectedButton + 1 < ButtonCount(pmb->buttons))
					{
						pmb->selectedButton++;
						Invalidate(pWindow);
					}
					break;
				}

				case VK_ESCAPE:
					EndModal(0);
					break;

				case VK_ENTER:
					if (pmb->selectedButton == ButtonCount(pmb->buttons) - 1)
						EndModal(0);	// Cancel
					else
						EndModal(pmb->selectedButton+1);	// 1, 2, 3...
					break;
			}
		}

	}
	return DefWindowProc(pWindow, pMsg);
}

void ModelessMessageBox(MESSAGEBOX* pMessageBox, const char* pszTitle, const char* pszMessage, int buttons)
{
	POINT textSize;
	int temp;
	bool error = (buttons & MB_ERROR)!=0;
	bool progress = (buttons & MB_INPROGRESS)!=0;

	buttons &= 0x0F;

	// Measure text
	MeasureMultiLineText(pszMessage, &textSize);

	// Work out width of buttons
	temp = ButtonsWidth(buttons);
	if (temp > textSize.x)
		textSize.x = temp;

	// Work out width of title
	temp = strlen(pszTitle);
	if (temp > textSize.x)
		textSize.x = temp;

	// Setup window
	memset(pMessageBox, 0, sizeof(*pMessageBox));
	pMessageBox->window.rcFrame.left = 0;
	pMessageBox->window.rcFrame.top = SCREEN_HEIGHT - 3;
	pMessageBox->window.rcFrame.width = textSize.x + 4;
	pMessageBox->window.rcFrame.height = textSize.y + 5;
	pMessageBox->window.rcFrame.left = (SCREEN_WIDTH - pMessageBox->window.rcFrame.width) / 2;
	pMessageBox->window.rcFrame.top = (SCREEN_HEIGHT - pMessageBox->window.rcFrame.height) / 2;
	pMessageBox->window.attrNormal = MAKECOLOR(COLOR_WHITE, error ? COLOR_DARKRED : (progress ? COLOR_DARKGREEN : COLOR_BLUE));
	pMessageBox->window.attrSelected = MAKECOLOR(COLOR_BLACK, COLOR_YELLOW);
	pMessageBox->window.title = pszTitle;
	pMessageBox->window.wndProc = MessageBoxWindowProc;
	pMessageBox->pszMessage = pszMessage;
	pMessageBox->buttons = buttons;
	pMessageBox->selectedButton = 0;
}

int MessageBox(const char* pszTitle, const char* pszMessage, int buttons)
{
	// Setup
	MESSAGEBOX mb;
	ModelessMessageBox(&mb, pszTitle, pszMessage, buttons);

	// Run it...
	return RunModalWindow(&mb.window);
}