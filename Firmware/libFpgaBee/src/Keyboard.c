//////////////////////////////////////////////////////////////////////////////
// libFpgaBee

#include "common.h"

typedef struct SCANCODEINFO
{
	uint8_t vk;
	uint8_t vkExtended;
} SCANCODEINFO;

// This keycode table maps ps2 scan code and extended scan codes to a 
// simpler "key number". This can then be mapped to characters using
// the character table below
SCANCODEINFO g_scanCodeInfo[] = 
{
	// 0x0?
	{ 0,0 },
	{ VK_F9,0 }, 
	{ 0,0 },
	{ VK_F5,0 },
	{ VK_F3,0 }, 
	{ VK_F1,0 }, 
	{ VK_F2,0 }, 
	{ VK_F12,0 },
	{ 0,0 },
	{ VK_F10,0 },
	{ VK_F8,0 },
	{ VK_F6,0 },
	{ VK_F4,0 },
	{ VK_TAB,0 },
	{ VK_BACKTICK,0 },
	{ 0,0 },

	// 0x10
	{ 0,0 },
	{ VK_LMENU,VK_RMENU },
	{ VK_LSHIFT,0 },
	{ 0,0 },
	{ VK_LCTRL,VK_RCTRL },
	{ 'Q', 0 },
	{ '1',0 }, 
	{ 0,0 },
	{ 0,0 },
	{ 0,0 },
	{ 'Z', 0 },
	{ 'S', 0 },
	{ 'A', 0 },
	{ 'W', 0 },
	{ '2',0 }, 
	{ 0,0 },

	// 0x2?
	{ 0,0 },
	{ 'C', 0 },
	{ 'X', 0 },
	{ 'D', 0 },
	{ 'E', 0 },
	{ '4', 0 },
	{ '3', 0 }, 
	{ 0,0 },
	{ 0,0 },
	{ VK_SPACE, 0 },
	{ 'V', 0 },
	{ 'F', 0 },
	{ 'T', 0 },
	{ 'R', 0 },
	{ '5', 0 },
	{ 0,0 },

	// 0x3?
	{ 0,0 },
	{ 'N', 0 },
	{ 'B', 0 },
	{ 'H', 0 },
	{ 'G', 0 },
	{ 'Y', 0 },
	{ '6', 0 }, 
	{ 0,0 },
	{ 0,0 },
	{ 0,0 },
	{ 'M', 0 },
	{ 'J', 0 },
	{ 'U', 0 },
	{ '7',0 }, 
	{ '8',0 }, 
	{ 0,0 },

	// 0x4?
	{ 0,0 },
	{ VK_COMMA, 0 },
	{ 'K', 0 },
	{ 'I', 0 },
	{ 'O', 0 },
	{ '0', 0 }, 
	{ '9', 0 }, 
	{ 0,0 },
	{ 0,0 },
	{ VK_PERIOD, 0 },
	{ VK_SLASH,VK_DIVIDE },
	{ 'L', 0 },
	{ VK_SEMICOLON, 0 },
	{ 'P', 0 },
	{ VK_HYPHEN, 0 },
	{ 0, 0 },

	// 0x5?
	{ 0,0 },
	{ 0,0 },
	{ VK_QUOTE,0 },
	{ 0,0 },
	{ VK_LSQUARE, 0 },
	{ VK_EQUALS, 0 },
	{ 0,0 },
	{ 0,0 },
	{ VK_CAPITAL,0 }, 
	{ VK_RSHIFT,0 },
	{ VK_ENTER,VK_NUMENTER },
	{ VK_RSQUARE, 0 },
	{ 0,0 },
	{ VK_BACKSLASH,0 },
	{ 0,0 },
	{ 0,0 },

	// 0x6?
	{ 0,0 },
	{ 0,0 },
	{ 0,0 },
	{ 0,0 },
	{ 0,0 },
	{ 0,0 },
	{ VK_BACKSPACE,0 },
	{ 0,0 },
	{ 0,0 },
	{ VK_NUM1,VK_END },
	{ 0,0 },
	{ VK_NUM4,VK_LEFT },
	{ VK_NUM7,VK_HOME },
	{ 0,0 },
	{ 0,0 },
	{ 0,0 },

	// 0x7?
	{ VK_NUM0,VK_INSERT },
	{ VK_DECIMAL,VK_DELETE },
	{ VK_NUM2,VK_DOWN },
	{ VK_NUM5,0 },
	{ VK_NUM6,VK_RIGHT },
	{ VK_NUM8,VK_UP },
	{ VK_ESCAPE,0 },
	{ 0,0 },
	{ VK_F11,0 },
	{ VK_ADD,0 },
	{ VK_NUM3,VK_NEXT },
	{ VK_SUBTRACT,0 },
	{ VK_MULTIPLY,0 },
	{ VK_NUM9,VK_PRIOR },
	{ 0,0 },
	{ 0,0 },

	// 0x8?
	{ 0,0 },
	{ 0,0 },
	{ 0,0 },
	{ VK_F7,0 },
};	


// Wait for SD card to initialize
uint8_t ScanCodeToVK(uint16_t keyEvent)
{
	uint8_t scanCode = keyEvent & 0xFF;
	if (scanCode < sizeof(g_scanCodeInfo) / sizeof(SCANCODEINFO))
	{
		if (keyEvent & 0x0100)
			return g_scanCodeInfo[scanCode].vkExtended;
		else
			return g_scanCodeInfo[scanCode].vk;
	}
	return 0;
}

// Map a VK to a character code
char VKToChar(uint8_t vk, uint16_t keyEvent)
{
	bool shift = keyEvent & 0x400;
	bool ctrl = keyEvent & 0x800;
	if (ctrl)
		return 0;
	if (vk >= 'A' && vk <= 'Z')
	{
		if (shift)
			return vk;
		else
			return vk - 'A' + 'a';
	}

	if (vk >= '0' && vk <= '9')
	{
		if (!shift)
			return vk;
	}

	if (shift)
	{
		switch (vk)
		{
			case '1': return '!';
			case '2': return '@';
			case '3': return '#';
			case '4': return '$';
			case '5': return '%';
			case '6': return '^';
			case '7': return '&';
			case '8': return '*';
			case '9': return '(';
			case '0': return '0';
			case VK_BACKTICK: return '~';
			case VK_HYPHEN: return '_';
			case VK_EQUALS: return '+';
			case VK_LSQUARE: return '{';
			case VK_RSQUARE: return '}';
			case VK_BACKSLASH: return '|';
			case VK_SEMICOLON: return ':';
			case VK_QUOTE: return '\"';
			case VK_COMMA: return '<';
			case VK_PERIOD: return '>';
			case VK_SLASH: return '?';

		}
	}
	else
	{
		switch (vk)
		{
			case VK_BACKTICK: return '`';
			case VK_HYPHEN: return '-';
			case VK_EQUALS: return '=';
			case VK_LSQUARE: return '[';
			case VK_RSQUARE: return ']';
			case VK_BACKSLASH: return '\\';
			case VK_SEMICOLON: return ';';
			case VK_QUOTE: return '\'';
			case VK_COMMA: return ',';
			case VK_PERIOD: return '.';
			case VK_SLASH: return '/';
		}
	}

	return 0;
}
