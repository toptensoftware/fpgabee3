#include <stdint.h>
#include <stdbool.h>

__at(0xF800) uint8_t colorRam[0x800];
__at(0xF800) uint8_t pcgRam[0x800];
__at(0xF000) uint8_t charRam[0x800];
__at(0xF000) uint8_t attrRam[0x800];


__sfr __at(0x0C) PortCrtcRegister;
__sfr __at(0x0D) PortCrtcData;

__sfr __at(0x08) PortColorRam;
#define COLOR_RAM_DISABLE 0x00
#define COLOR_RAM_ENABLE 0x40

__sfr __at(0x1c) PortVideoMemoryLatch;
#define VML_BANK_SELECT_MASK		0x0F
#define VML_CHARACTER_RAM_ENABLE	0x00
#define VML_ATTRIBUTE_RAM_ENABLE	0x10
#define VML_EXTENDED_GRAPHICS		0x80

// Colors
#define COLOR_BLACK			0
#define COLOR_DARK_RED     	1
#define COLOR_DARK_GREEN   	2
#define COLOR_BROWN        	3
#define COLOR_DARK_BLUE    	4
#define COLOR_DARK_MAGENTA 	5
#define COLOR_DARK_CYAN    	6
#define COLOR_LIGHT_GREY   	7
#define COLOR_DARK_GREY    	8
#define COLOR_RED          	9
#define COLOR_GREEN        	10
#define COLOR_YELLOW       	11
#define COLOR_BLUE         	12
#define COLOR_MAGENTA      	13
#define COLOR_CYAN         	14
#define COLOR_WHITE         15


#define KEY_UP			56
#define KEY_DOWN		58
#define KEY_LEFT      	59
#define KEY_RIGHT     	62
#define KEY_COMMA     	44
#define KEY_PERIOD    	46
#define KEY_SPACE     	55
#define KEY_ENTER     	52
#define KEY_1         	33
#define KEY_2         	34
#define KEY_ESCAPE    	48
#define KEY_SHIFT		63
#define KEY_CTRL		57
#define KEY_A		  	1
#define KEY_D0		    32

// Special keys returned by read_key()
#define CHAR_DELETE	'\x01'
#define CHAR_LOCK '\x02'
#define CHAR_BREAK '\x03'
#define CHAR_UP	'\x04'
#define CHAR_DOWN '\x05'
#define CHAR_LEFT '\x06'
#define CHAR_RIGHT '\x07'

bool is_key_down(uint8_t key);
uint8_t read_key();
char getchar();
void initScreen();
void clearScreen();
void putchar(char ch);


__sfr __at(0x40) PortFdcStatus;		// Read
__sfr __at(0x40) PortFdcCommand;	// Write
__sfr __at(0x41) PortFdcTrack;		// Read-Write
__sfr __at(0x42) PortFdcSector;		// Read-Write
__sfr __at(0x43) PortFdcData;		// Read-Write
__sfr __at(0x48) PortFdcStatus2;	// Read
__sfr __at(0x48) PortFdcDHM;		// Write (Drive/Head/Mode)

