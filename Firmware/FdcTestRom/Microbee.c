#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Microbee.h"

#define LINE_WIDTH 80

char normalKeyMap[64] = "@abcdefghijklmnopqrstuvwxyz[\\]^\x01""0123456789:;,-./\x1b\x08\t\x0A\n\x02\x03 \x04\xFF\x05\xFF\xFF\x06\xFF";
char shiftKeyMap[64] = "`ABCDEFGHIJKLMNOPQRSTUVWXYZ{|}~\x01\xFF!\"#$%&'()*+<=>?\x1b\x08\t\x0A\n\x02\x03 \x04\xFF\x05\xFF\xFF\x06\xFF";

bool is_key_down(uint8_t key) __naked
{
	key;
__asm
            ld    iy,#2
            add   iy,sp
            ld    a,0 (iy)

            PUSH    BC

            ; Write the loword to register 0x12 (18)
            LD      B,A
            LD      A,#0x12
            OUT     (#0x0C),A
            LD      A,B
            RRCA
            RRCA
            RRCA
            RRCA
            AND     #0x03
            OUT     (#0x0D),A

            ; Write the hiword to register 0x13 (19)
            LD      A,#0x13
            OUT     (#0x0C),A
            LD      A,B
            RLCA
            RLCA
            RLCA
            RLCA
            OUT     (#0x0D),A

            ; Enable Latch ROM (to disable key scan)
            LD      A,#0x01
            OUT     (#0x0B),A


            ; Read register 0x10 (16) (light pen address to clear light pen flag)
            LD      A,#0x10
            OUT     (#0x0C),A
            IN      A,(#0x0D)

            ; Write to port 31 to scan the key
            LD      A,#0x1F
            OUT     (#0x0C),A
            OUT     (#0x0D),A

            ; Wait for update strobe bit to be set
00001$:     IN      A,(#0x0C)
            BIT     #7,A
            JR      Z,00001$

            ; Read status register and check lpen bit
            IN      A,(#0x0C)
            BIT     #6,A

            ; Turn off latch rom
            LD      A,#0x00
            OUT     (#0x0B),A

            ; Setup return value
            ld      L,#1
            jr      nz,00002$
            ld      L,#0
00002$:
            POP     BC
            RET
__endasm;
}


uint8_t read_key() __naked
{
__asm
            PUSH    BC

            ; Wait till light pen bit set
00001$:
            IN      A,(#0x0C)
            BIT     #6,A                     ; Is light pen bit set?
            JR      Z,00001$

            ; Read register 16 (high word)
            LD          A,#16
            OUT         (#0x0C),A
            IN          A,(#0x0D)

            ; Shift lo 4 bits left and store in B
            sla         A
            sla         A
            sla         A
            sla         A
            LD          B,A

            ; Read register 17 (loword)
            LD          A,#17
            OUT         (#0x0C),A
            IN          A,(#0x0D)

            ; Shift hi 4 bits right
            srl         A
            srl         A
            srl         A
            srl         A

            ; Combine to form key code
            OR          B

            LD          L,A
            POP         BC
            RET
__endasm;
}


uint8_t lastKey = 0xFF;
char getchar()
{
	while (is_key_down(lastKey))
	{
	}

	while (true)
	{
		// Read a key
		bool shift;
		char ch;
		uint8_t key = read_key();

		// Ignore modifiers
		if (key == KEY_SHIFT || key == KEY_CTRL)
			continue;

		shift = is_key_down(KEY_SHIFT);

		ch = shift ? shiftKeyMap[key] : normalKeyMap[key];

		if (ch == 0xFF)
			continue;

		lastKey = key;
		return ch;
	}
}

void initScreen()
{
	uint8_t CRTC_Registers_80_24[] = {
		0x6B,0x50,0x58,0x37,0x1B,0x05,0x18,0x1A,
	  	0x48,0x0A,0x2A,0x0A,0x20,0x00,0x20,0x00
	};
	uint8_t i;

	for (i=0; i<16; i++)
	{
		PortCrtcRegister = i;
		PortCrtcData = CRTC_Registers_80_24[i];
	}


	PortCrtcRegister = 10;
	PortCrtcData = 0xCA;
	PortCrtcRegister = 11;
	PortCrtcData = 0x0B;
}

char* psz = charRam;

void clearScreen()
{
	// Clear characters
	memset(charRam, ' ', sizeof(charRam));

	// Clear colors
	PortColorRam = COLOR_RAM_ENABLE;
	memset(colorRam, COLOR_YELLOW | (COLOR_BLACK << 4), sizeof(colorRam));
	PortColorRam = COLOR_RAM_DISABLE;

	psz = charRam;
}

void putchar(char ch)
{
	// Carriage return
	if (ch == '\n')
	{
		// Move to next line
		psz += LINE_WIDTH - ((size_t)psz % LINE_WIDTH);
	}
	else if (ch == '\r')
	{
		psz -= ((size_t)psz % LINE_WIDTH);
	}
	else if (ch == '\t')
	{
		*psz++=' ';
		while ((((size_t)psz % LINE_WIDTH) % 4) != 0)
		{
			*psz++=' ';
		}
	}
	else if (ch == '\x08')
	{
		if ((size_t)psz % LINE_WIDTH)
		{
			psz--;
			*psz = ' ';
		}
	}
	/*
	else if (ch < 32)
	{
		return;
	}
	*/	
	else
	{
		// Normal character
		*psz++ = ch;
	}

	// Scroll?
	if (psz >= (charRam + (80*24)))
	{
		// Screen
		memmove(charRam, charRam + LINE_WIDTH, sizeof(charRam) - LINE_WIDTH);
		memset(charRam + LINE_WIDTH * 23, ' ', LINE_WIDTH);
		psz -= LINE_WIDTH;
	}

	// Position cursor
	PortCrtcRegister = 14;
	PortCrtcData = (((size_t)psz >> 8) & 0x0F) | 0x20;
	PortCrtcRegister = 15;
	PortCrtcData = (size_t)psz & 0xFF;
}
