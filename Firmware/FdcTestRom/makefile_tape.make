COMMONFLAGS=-mz80 --stack-auto
CFLAGS=$(COMMONFLAGS) --std-c99
LINKFLAGS=$(COMMONFLAGS) --code-loc 0x480 --data-loc 0x3000 --no-std-crt0
INTDIR=./Debug

#SOURCES  := $(wildcard ./*.c)
PROJNAME := FdcTestRom
SOURCES  := main.c
INCLUDES := $(wildcard ./*.h)
PROJOBJS := $(SOURCES:%.c=$(INTDIR)/%.rel)
ASMOBJS  := $(INTDIR)/crt0.rel $(INTDIR)/keyboard.rel
OBJECTS  := $(ASMOBJS) $(PROJOBJS)
LIBS	 := 


# Default
all: $(PROJNAME).rom

# Clean
clean:
	rm -r $(INTDIR)
	rm $(PROJNAME).rom

# Rebuild
rebuild: clean all

# Compile
$(PROJOBJS): $(INTDIR)/%.rel : ./%.c $(INCLUDES)
	sdcc $(CFLAGS) -c $< -o $@

# Link
$(PROJNAME).rom: $(INTDIR) $(OBJECTS) $(LIBS) 
	sdcc $(LINKFLAGS) $(OBJECTS) $(LIBS) -o $(INTDIR)/$(PROJNAME).ihx
	../../tools/hex2bin.exe -s 400 -e rom $(INTDIR)/$(PROJNAME).ihx
	cp $(INTDIR)/$(PROJNAME).rom ./$(PROJNAME).rom
	../../tools/yazd.exe $(PROJNAME).rom --addr:0x400 --entry:0x400 --lst > $(PROJNAME).lst
	tapetool2 --microbee binReader --filename:FdcTestRom.bin packData setHeader --autoStart:255 blocksToBytes out.tap

# Assemble
$(ASMOBJS): $(INTDIR)/%.rel : ./%.s
	@echo Compiling $(notdir $<)...
	@sdasz80 -l -o $@ $<

# Directories
$(INTDIR):
	mkdir -p $(INTDIR)


	