#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Microbee.h"

uint16_t try_boot_1();
uint16_t try_boot_2();
bool try_write(uint8_t sector, uint8_t track, void* ptr);

void fdc_state(int type)
{
	uint8_t status2 = PortFdcStatus2;
	uint8_t status = PortFdcStatus;
	printf("Status: ");
	if (status & 0x80) printf("nr ");
	if (status & 0x40) printf("prot ");
	if (type == 1)
	{
		if (status & 0x20) printf("head ");
		if (status & 0x10) printf("sekr ");
		if (status & 0x04) printf("tr00 ");
		if (status & 0x02) printf("indx ");
	}
	else
	{
		if (status & 0x20) printf("rect ");
		if (status & 0x10) printf("notf ");
		if (status & 0x04) printf("lost ");
		if (status & 0x02) printf("drq ");
	}
	if (status & 0x08) printf("crc ");
	if (status & 0x01) printf("busy ");

	printf("(%02x) dirq:%02x track:%i sector: %i\n", status, status2, PortFdcTrack, PortFdcSector);
}

uint8_t fdcWaitWhileBusy() __naked
{
__asm
    00001$:
		in    	A,(#0x48)
		bit	  	7,A
		jr	  	z,00001$
		in		A,(#0x40)
		ld		L,A
		ret
__endasm;
}

uint8_t fdcRestore(bool loadHead)
{
	PortFdcCommand = loadHead ? 0x08 : 0x00;
	return fdcWaitWhileBusy();
}

uint8_t fdcSeek(bool loadHead, bool verify)
{
	PortFdcCommand = 0x10 | (loadHead ? 0x08 : 0) | (verify ? 0x04 : 0);
	return fdcWaitWhileBusy();
}

uint8_t fdcStep(bool loadHead, bool verify, bool update)
{
	PortFdcCommand = 0x20 | (update ? 0x10 : 0) | (loadHead ? 0x08 : 0) | (verify ? 0x04 : 0);
	return fdcWaitWhileBusy();
}

uint8_t fdcStepIn(bool loadHead, bool verify, bool update)
{
	PortFdcCommand = 0x40 | (update ? 0x10 : 0) | (loadHead ? 0x08 : 0) | (verify ? 0x04 : 0);
	return fdcWaitWhileBusy();
}

uint8_t fdcStepOut(bool loadHead, bool verify, bool update)
{
	PortFdcCommand = 0x60 | (update ? 0x10 : 0) | (loadHead ? 0x08 : 0) | (verify ? 0x04 : 0);
	return fdcWaitWhileBusy();
}

uint16_t fdcReadData(uint8_t* pBuf, uint16_t len) __naked
{
	pBuf;
	len;
	__asm
            ld    iy,#2
            add   iy,sp
            ld    l,0 (iy)
            ld	  h,1 (iy)
            ld    c,2 (iy)
            ld    b,3 (iy)
            ld	  de,#0

            call  _show_prog2

            ; Wait for drq/irq
    00001$:
    		in    A,(#0x48)
    		bit	  7,A
    		inc   de
    		jr	  z,00001$

            call  _show_prog2

    		; Read byte
	00002$:
			in	  A,(#0x43)
			ld	  (HL),A
			inc     hl
			dec		bc
			ld		a,b
			or		c
			jr		z, 00099$

	00003$:
			in 		A,(#0x40)
			INC		DE
			bit		#1,A
			jr		z,00003$
			jr		00002$


	00099$:
            call  _show_prog2

    		push	de
    		pop		hl
    		ret
	__endasm;
}

void fdcReadAddress(uint8_t* pBuf)
{
	// Invoke command
	PortFdcCommand = 0xc0;

	printf("Time to drq: %u\n", fdcReadData(pBuf, 6));

}

void fdcReadSector(uint8_t* pBuf)
{
	// Invoke read command command
	PortFdcCommand = 0x80;

	printf("Time to drq: %u\n", fdcReadData(pBuf, 512));

}

void fdcReadSectorMulti(uint8_t* pBuf)
{
	uint16_t t1, t2;
	memset(pBuf, 0xCC, 1024);	

	// Invoke read command command
	PortFdcCommand = 0x90;

	t1 = fdcReadData(pBuf, 512);
	t2 = fdcReadData(pBuf + 512, 512);

	PortFdcCommand = 0xD0;
	fdcWaitWhileBusy();

	printf("Time to drq 1st: %u\n", t1);
	printf("Time to drq 2nd: %u\n", t2);
}

void fdcSetTrack(uint8_t track)
{
	PortFdcTrack = track;
}

void fdcSetSector(uint8_t sector)
{
	PortFdcSector = sector;
}

void fdcData(uint8_t d)
{
	PortFdcData = d;
}

uint8_t buf[1024];

void mainX(void) 
{
	uint16_t i;
	bool multi;

	initScreen();
	clearScreen();

	PortFdcDHM = 0x08;

	printf("Single or multi?");
	multi = getchar() == 'm';
	printf("\n");

	memset(buf, 0, sizeof(buf));

	printf("restore: "); fdcRestore(true); fdc_state(1);
	printf("seek 11: "); fdcData(11); fdcSeek(true, false); fdc_state(1);

	printf("setsect: "); fdcSetSector(5); fdc_state(1);

	if (multi)
	{
		printf("read multi: " ); fdcReadSectorMulti(buf); fdc_state(2);
	}
	else
	{
		printf("read single: " ); fdcReadSector(buf); fdc_state(2);
	}

	printf("\n Block 1 --> ");
	for (i=0; i<32; i++)
	{
		printf("%2x ", buf[i], buf[i]);
	}

	printf("\n");
	for (i=0; i<32; i++)
	{
		putchar(buf[i]);
	}
	printf("\n");
	for (i=0; i<32; i++)
	{
		putchar(buf[512 - 32 + i]);
	}

	printf("\n\n Block 2--> ");
	for (i=0; i<32; i++)
	{
		printf("%2x ", buf[i+512], buf[i+512]);
	}

	printf("\n");
	for (i=0; i<32; i++)
	{
		putchar(buf[i+512]);
	}
	printf("\n");
	for (i=0; i<32; i++)
	{
		putchar(buf[512 + 512 - 32 + i]);
	}

/*
	fdc_state(1);
	fdcSetTrack(10); fdc_state(1);
	printf("restore: "); fdcRestore(true); fdc_state(1);
	printf("seek 10: "); fdcData(10); fdcSeek(true, false); fdc_state(1);
	printf("step In: "); fdcStepIn(true, true, true); fdc_state(1);
	printf("step   : "); fdcStep(true, true, true); fdc_state(1);
	printf("step Ot: "); fdcStepOut(true, true, true); fdc_state(1);
	printf("step   : "); fdcStep(true, true, true); fdc_state(1);
	printf("restore: "); fdcRestore(true); fdc_state(1);

	printf("step In: "); fdcStepIn(true, true, true); fdc_state(1);
	printf("step In: "); fdcStepIn(true, true, true); fdc_state(1);
	printf("sector5: "); fdcSetSector(5); fdc_state(1);


	printf("readadr:" ); fdcReadAddress(buf); fdc_state(2);
	printf(" --> ");
	for (i=0; i<6; i++)
	{
		printf("%2x ", buf[i]);
	}
	printf("\n");
	*/

	while (true)
	{
		char ch = getchar();
		putchar(ch);
	}
}


void mainY(void) 
{
	uint16_t i;
	initScreen();
	clearScreen();
	printf("\n\n");

	PortFdcDHM = 0x08;

	printf("try boot 1: %04x\n", try_boot_1());
	fdc_state(2);

//	printf("Ready?");
//	getchar();
//	printf("\n");
	printf("try boot 2: %04x\n", try_boot_2());
	fdc_state(2);

/*
	printf("seek 1: "); fdcData(1); fdcSeek(true, false); fdc_state(1);;
	printf("setsect: "); fdcSetSector(1); fdc_state(1);
	printf("read multi: " ); fdcReadSectorMulti(buf); fdc_state(2);

	printf("\n Block 1 --> ");
	for (i=0; i<32; i++)
	{
		printf("%2x ", buf[i], buf[i]);
	}

	printf("\n");
	for (i=0; i<32; i++)
	{
		putchar(buf[i]);
	}
	printf("\n");
	for (i=0; i<32; i++)
	{
		putchar(buf[512 - 32 + i]);
	}
*/
}

void main(void) 
{
	uint16_t i;
	initScreen();
	clearScreen();
	printf("\n\n");

	printf("Ready?"); getchar(); printf("\n");

	PortFdcDHM = 0x08;

	for (i=0; i<512; i++)
	{
		buf[i] = (uint8_t)i;
	}

	printf("Result: %s\n", try_write(0, 1, buf) ? "Success" : "Failed");
	fdc_state(2);

	printf("Result: %s\n", try_write(0, 3, buf) ? "Success" : "Failed");
	fdc_state(2);
}