; Boot starts at 0x8000 in BN54 rom
; Rom copies itself from 0x8100 to 0xE000 RAM and latches out the ROM
; Rom jumps to itself in new location at 0xE000
; Rom reads first sector of disk which to 0x0080 which it then jumps to


; ------------------------------- THIS IS FROM THE FIRST SECTOR ON THE DISK  ------------------------------- 

addr_0080:
;    LD SP,#0x0080		
    LD DE,#0x0002		; Track 0 sector 2
    LD HL,#0xB400		; Destination
    LD BC,#0x11FF		; Number of bytes - 1 ( = 9 x 512 byte sectors )
    CALL addr_E039_read_sectors
    LD DE,#0x0101		; Track 1 sector 1
    LD HL,#0xC600		; Destination ( = b400 + 1200 )
    LD BC,#0x111B		; Number of bytes - 1 ( = 8 x 512 byte sectors + 0x11C bytes)
    CALL Z,addr_E039_read_sectors
    JR NZ,addr_00B0
    DI
    LD A,#0x0C
    OUT (#0x50),A				; Memory Latch
    LD HL,#0xD719
    LD DE,#0xFC19
    LD BC,#0x231A
    LDDR
    JP #0xE700			; BOOT!

addr_00B0:
	; LOAD FAILED 
	; Looks like it does some clean up and then jumps back to 0xE000 to restart probably.


; ------------------------------- THIS IS FROM THE BOOT ROM AT NEW LOCATION 0xE000  ------------------------------- 

	; ROM function vector table
addr_E039_read_sectors:
    JP addr_EF15_read_sectors


addr_EDB6_read_address:
    PUSH BC
    PUSH DE
    DI
    OUT (#0x09),A				; Clock speed
    LD A,#0xC0					; COMMAND: Read Address
    CALL addr_EEC9_invoke_and_delay10
    LD C,#0x48			
l_EDC2:
    IN A,(C)					; dirq
    JP P,l_EDC2
    IN A,(#0x47)				; Data Register
    LD D,A						; Track field of sector id
    LD B,#0x03					; read three more bytes
l_EDCC:
    IN A,(C)					; dirq
    JP P,l_EDCC
    IN A,(#0x47)				; Data Register
    DJNZ l_EDCC
    LD E,A						; Sector Length from the SectorID record
    CALL addr_EEDC_wait_not_busy
    EI
    AND #0x98
    JR NZ,addr_EDF0
    LD A,D
    OUT (#0x45),A				; Track Register
    LD A,E
    LD (#0xDF4D_sector_length_shr_8),A	; Save sector length
    LD A,#0x20
l_EDE7:
    RLA
    DEC E
    JP P,l_EDE7					; Jump if not negative
    LD (#0xDF4E_sector_length_divided_by_two),A				; 32 << (sector length + 1) = 256 = 0 for 512 byte sector
    CP A
addr_EDF0:
    POP DE
    POP BC
    RET

    ; Seek to track D and set the sector register to E
    ; if <40 then head 0 else head 1
addr_EDF3_seek_track_and_sector:
    OUT (#0x09),A				; Clock Speed (Test = 0x03)
    PUSH DE
    LD A,(#0xDF4C_selected_disk)				; 0x00
    LD E,A
    LD A,(#0xDF66_double_density)				; 0x08
    OR E
    LD E,A
    LD A,D
    CP #0x28					; 40
    JR C,addr_EE0A				; C
    LD A,#0x4F
    SUB D
    LD D,A
    SET 2,E						; Set head
addr_EE0A:
    LD A,E						; All tests = 0x08
    OUT (#0x48),A				; Drive/head/recording mode
    IN A,(#0x45)				; Track Register
    CP D						; 0x00
    JR Z,addr_EE26				; Z
    LD A,D
    OUT (#0x47),A				; Data Register
    LD A,(#0xDF67_track_to_track_delay_mask)
    OR #0x1C
    CALL addr_EED7_invoke_and_wait_not_busy
    LD B,#0x0A
    CALL addr_EEE3_delay_B_by_7A
    IN A,(#0x44)				; Status Register
    AND #0x18
addr_EE26:
    LD A,(#0xDF4E_sector_length_divided_by_two)
    LD B,A
    LD C,#0x48					; Setup dirq register in C
    POP DE
    LD A,E
    OUT (#0x46),A				; Set Sector Register
    RET

; Not called during boot but left for reference
addr_EE71_write_sector:
    PUSH BC
    LD B,#0x32					; 50d  Retry Count
addr_EE74:
    PUSH HL
    PUSH BC
    CALL addr_EDF3_seek_track_and_sector
    							; Returns B=0 for 512 byte sectors
    JR NZ,addr_EEA3
    DI
    PUSH DE
    LD A,#0xA8					; COMMAND: Write Sector
    CALL addr_EEC9_invoke_and_delay10
l_EE82:
    LD A,(HL)
l_EE83:
    IN D,(C)					; dirq
    JP P,l_EE83
    OUT (#0x47),A				; Data Register
    INC HL
    LD A,(HL)
l_EE8C:
    IN D,(C)					; dirq
    JP P,l_EE8C
    OUT (#0x47),A				; Data Register
    INC HL
    DJNZ l_EE82
    EI
    POP DE
    CALL addr_EEDC_wait_not_busy
    AND #0xFD
    JR NZ,addr_EEA3
    POP BC
    POP BC
    POP BC
    RET

    ; Looks like an error/retry handler for the above
addr_EEA3:
    POP BC
    POP HL
    CALL addr_EEB3
    LD A,#0xD7
    LD (#0xF780),A
    DJNZ addr_EE74
    POP BC
    XOR A
    INC A
    RET

addr_EEB3:
    PUSH AF
    CALL addr_EDB6_read_address
    LD A,#0x07
    AND B
    CALL Z,addr_EED5_restore
    CALL addr_EED1_force_interrupt
    LD A,(#0xDF4F_total_error_count)
    INC A
    LD (#0xDF4F_total_error_count),A
    POP AF
    RET


; Read multiple sectors
;  D = track number
;  E = sector number
;  HL = destination buffer
;  BC = number of bytes - 1
addr_EF15_read_sectors:

    PUSH IX
    PUSH IY

    ; IX = HL + BC
    PUSH HL
    POP IX
    ADD IX,BC				

    CALL addr_EFAB_ld_iy_ptr_to_sectors_per_track
    
    LD B,#0x05				; Retry count

addr_EF23:
    PUSH BC
    PUSH DE
    PUSH HL
addr_EF26:
    CALL addr_EDF3_seek_track_and_sector
    JR NZ,addr_EF70			; Retry handler

    ; Invoke read command
    DI
    LD A,#0x98				; COMMAND: MultiSector Read
    CALL addr_EEC9_invoke_and_delay10

    ; Input sector bytes
addr_EF31:
    LD A,(#0xDF4E_sector_length_divided_by_two)			; Sector size/2
    LD B,A
    LD C,#0x48				; dirq port

    ; Sector data read loop
l_EF37:
	; Wait for dirq
    IN A,(C)				; dirq
    JP P,l_EF37				

    ; Store byte
    IN A,(#0x47)			; Data Register
    LD (HL),A
    INC HL

    ; Wait for dirq
l_EF40:
    IN A,(C)				; dirq
    JP P,l_EF40

    ; Store byte
    IN A,(#0x47)			; Data Register
    LD (HL),A
    INC HL

    ; Continue reading bytes
    DJNZ l_EF37

    ; Has everything the caller asked for been read?
    PUSH IX
    POP BC
    PUSH HL
    AND A
    SBC HL,BC
    POP HL
    JR NC,addr_EF81			; Yes

    ; Are there more sectors on the track
    LD A,E
    INC E
    CP (IY+#0x00)
    JR NZ,addr_EF31			; Yes, loop for the next sector

    ; Stall for a bit
    LD B,#0x03
    CALL addr_EEE3_delay_B_by_7A

    ; Check the busy status bit
    IN A,(#0x44)			; Status Register
    CPL
    BIT 0,A
    JR NZ,addr_EF70			; Busy not set, start again

    ; Stop the multi-sector read command
    CALL addr_EED1_force_interrupt

    ; Next track, sector 1
    LD E,#0x01
    INC D

    ; Continue with next sector
    JR addr_EF26

; Retry handler for read sector
addr_EF70:
    POP HL
    POP DE
    POP BC
    DJNZ addr_EF23
    ; fall through

; Exit the function
addr_EF75_exit:
    EI
    POP IY
    POP IX
    RET NZ
    CALL addr_EED1_force_interrupt
    AND #0x9C
    RET

; Read complete
addr_EF81:
    POP HL
    POP DE
    POP BC
    XOR A			; No error
    JR addr_EF75_exit


; Not called during boot but left for reference
    PUSH IY
    PUSH HL
    ADD HL,BC
    LD B,H
    LD C,L
    CALL addr_EFAB_ld_iy_ptr_to_sectors_per_track
    POP HL
addr_EF91:
    CALL addr_EE71_write_sector
    JR NZ,addr_EFA8
    LD A,E
    INC E
    CP (IY+#0x00)
    JR C,addr_EFA0
    INC D
    LD E,#0x01
addr_EFA0:
    PUSH HL
    AND A
    SBC HL,BC
    POP HL
    JR C,addr_EF91
    CP A
addr_EFA8:
    POP IY
    RET

addr_EFAB_ld_iy_ptr_to_sectors_per_track:
    PUSH HL
    LD HL,#0xDF66_double_density
    LD A,(#0xDF4D_sector_length_shr_8)			; 0x02
    RLCA					; 0x04
    BIT 3,(HL)
    POP HL
    JR Z,addr_EFB9			; NZ
    DEC A					; 0x03
addr_EFB9:
    LD IY,#0xEFF0
    RET M
    BIT 2,A
    RET NZ
    PUSH DE
    LD D,#0x00
    LD E,A
    ADD IY,DE				; IY += 3 = 0xEFF3
    POP DE
    RET


; Invoke an FDC command and the delay for a bit
addr_EEC9_invoke_and_delay10:
    OUT (#0x44),A				; Invoke Command
addr_EECB_delay10:
    LD A,#0x10
addr_EECD:
    DEC A
    JR NZ,addr_EECD
    RET


; Seek to a track (D) and load the sector register (E)
; Returns C set to 0x48 (the dirq port)
; And return B set to the number bytes per sector divided by 2 (ie: 0 for 512 byte sector)
addr_EDF3_seek_track_and_sector:
    OUT (#0x09),A				; Clock Speed
    PUSH DE
    LD A,(#0xDF4C_selected_disk)
    LD E,A
    LD A,(#0xDF66_double_density)
    OR E
    LD E,A
    LD A,D
    CP #0x28
    JR C,addr_EE0A
    LD A,#0x4F
    SUB D
    LD D,A
    SET 2,E
addr_EE0A:
    LD A,E
    OUT (#0x48),A
    IN A,(#0x45)				; Track Register
    CP D
    JR Z,addr_EE26
    LD A,D
    OUT (#0x47),A				; Set Data Register to the track we want to seek to
    LD A,(#0xDF67_track_to_track_delay_mask)
    OR #0x1C					; Command Seek (w LoadHead + Verify)
    CALL addr_EED7_invoke_and_wait_not_busy
    LD B,#0x0A
    CALL addr_EEE3_delay_B_by_7A
    IN A,(#0x44)				; Status Register
    AND #0x18
addr_EE26:
    LD A,(#0xDF4E_sector_length_divided_by_two)
    LD B,A
    LD C,#0x48
    POP DE
    LD A,E
    OUT (#0x46),A				; Set the Sector Register
    RET


; Delay for B * 0x7a loop cycles
addr_EEE3_delay_B_by_7A:
    PUSH HL
addr_EEE4:
    LD HL,#0x007A
addr_EEE7:
    DEC HL
    LD A,H
    OR L
    JR NZ,addr_EEE7
    DJNZ addr_EEE4
    POP HL
    RET


; Invoke the FDC Force Interrupt comamnd
addr_EED1_force_interrupt:
    LD A,#0xD0
    JR addr_EED7_invoke_and_wait_not_busy

; Invoke the FDC Restore command
addr_EED5_restore:
    LD A,#0x0B
    ; fall through

; Invoke an FDC command an wait until not busy
addr_EED7_invoke_and_wait_not_busy:
    OUT (#0x44),A				; Invoke Command
    CALL addr_EECB_delay10
    ; fall through

; Wait until FDC reports not busy
addr_EEDC_wait_not_busy:
    IN A,(#0x44)				; Status Register
    BIT 0,A
    JR NZ,addr_EEDC_wait_not_busy
    RET

