#include "common.h"
#include "DiskContext.h"
#include "ChooseFile.h"

static void add_file(CHOOSEFILECONTEXT* pctx, const char* filename)
{
	// Copy to work buffer
	strcpy(pctx->pBuffer, filename);

	// Update file count and buffer position
	pctx->ppFiles[pctx->nFiles++] = pctx->pBuffer;
	pctx->pBuffer += strlen(filename) + 1;
}

static bool enumfiles_callback(uint32_t deIndex, const NFFSDIRENTRY* de, void* user)
{
	CHOOSEFILECONTEXT* pcctx = (CHOOSEFILECONTEXT*)user;
	deIndex;
	user;

	// Matches?
	if (nffsGlob(de->filename, pcctx->pattern))
	{
		add_file(pcctx, de->filename);
	}

	return pcctx->nFiles != 0xFF;
}

int strcmpi(const char* p1, const char* p2)
{
	while (*p1 && *p2)
	{
		char ch1 = tolower(*p1++);
		char ch2 = tolower(*p2++);
		if (ch1 < ch2)
			return -1;
		if (ch1 > ch2)
			return 1;
	}

	if (*p1)
		return 1;
	if (*p2)
		return -1;
	return 0;
}

static void sort_files(const char** ppFiles, size_t count)
{
  size_t i;
  size_t j;
  const char** pi = ppFiles;
  const char** pj;
  const char* tmp;

  for (i=0; i<count-1; i++, pi++)
  {
    pj = pi + 1;
    for (j=i+1; j<count; j++, pj++)
    {
      if (strcmpi(*pi, *pj) > 0)
      {
      	tmp = *pi;
      	*pi = *pj;
      	*pj = tmp;
      }
    }
  }
}

int indexOf(const char** ppFiles, size_t count, const char* psz)
{
	size_t i;

	if (!psz)
		return -1;

	for (i=0; i<count; i++)
	{
		if (strcmp(ppFiles[i], psz)==0)
			return i;
	}

	return -1;
}

const char* ChooseFileEx(
	const char* pattern, 
	const char* selectedFile, 
	const char* pszNullOption, 
	const char* pszTitle,
	size_t (*wndProc)(WINDOW* pWindow, MSG* pMsg)
	)
{
	int err, sel;	
	const char* retv = NULL;
	LISTBOX lb;

	// Find matching files
	CHOOSEFILECONTEXT ctx;
	ctx.pattern = pattern;
	ctx.pBuffer = BankedMemory + 0x100 * sizeof(void*);
	ctx.nFiles = 0;
	ctx.ppFiles = BankedMemory;

	// Null option?
	if (pszNullOption)
	{
		add_file(&ctx, pszNullOption);
	}

	// Find files
	err = nffsEnumFiles(&nffsctx, enumfiles_callback, &ctx);
	if (err)
		return NULL;

	if (pszNullOption)
		sort_files(ctx.ppFiles + 1, ctx.nFiles - 1);
	else
		sort_files(ctx.ppFiles, ctx.nFiles);



	// Display picker...
	memset(&lb, 0, sizeof(lb));
	lb.window.rcFrame.left = 4;
	lb.window.rcFrame.top = 2;
	lb.window.rcFrame.width = 17;
	lb.window.rcFrame.height = 12;
	lb.window.attrNormal = MAKECOLOR(COLOR_WHITE, COLOR_BLUE);
	lb.window.attrSelected = MAKECOLOR(COLOR_BLACK, COLOR_YELLOW);
	lb.window.title = pszTitle;
	lb.window.wndProc = wndProc;
	lb.window.user = &ctx;
	lb.selectedItem = indexOf(ctx.ppFiles, ctx.nFiles, selectedFile);
	ListBoxSetData(&lb, ctx.nFiles, ctx.ppFiles);
	lb.scrollPos = ListBoxEnsureVisible(&lb, lb.selectedItem);

	sel = RunModalWindow(&lb.window);

	// Cancelled
	if (sel<0)
		return NULL;

	// Null option selected?
	if (sel == 0 && pszNullOption)
		return "";

	// If same file selected, return cancelled.
	if (selectedFile != NULL)
	{
		if (strcmp(ctx.ppFiles[sel], selectedFile)==0)
			return NULL;
	}

	// Return pointer to filename!
	return ctx.ppFiles[sel];
}	


// Choose a file matching pattern.
// Returns NULL if cancelled, empty string if ejected, otherwise file name
// which must be copied immediately
const char* ChooseFile(const char* pattern, const char* selectedFile, const char* pszNullOption)
{
	return ChooseFileEx(pattern, selectedFile, pszNullOption, "Choose File", ListBoxWindowProc);
}

