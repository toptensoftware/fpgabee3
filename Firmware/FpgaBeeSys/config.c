#include "common.h"
#include "config.h"
#include "DiskManager.h"
#include "RomManager.h"

CONFIG g_Config;

void DefaultConfig()
{
	memset(&g_Config, 0, sizeof(g_Config));
	g_Config.clockMask = CLOCK_MASK_3_375;
	g_Config.sysFlags = 0;
	g_Config.autoSysFlags = 
		SYSTEM_FLAG_VIDEO_COLOR | 
		SYSTEM_FLAG_VIDEO_ATTR | 
		SYSTEM_FLAG_WD1793 | 
		SYSTEM_FLAG_WD1002;
}


void ConfigureSystem(const char* filename)
{
	// Work out system flags
	uint8_t sysFlags = g_Config.sysFlags & ~g_Config.autoSysFlags;


	// ROM Pack 0 is only loaded on system boot, never from menu
	// so use this opportunity to configure the system
	if (strcmp(filename, "PC85B_BASIC.rom")==0)
	{
		// Zap first 32k of ram
		MemoryBankPort = 0x10;
		memset(BankedMemory, 0, sizeof(BankedMemory));
		MemoryBankPort = 0x11;
		memset(BankedMemory, 0, sizeof(BankedMemory));
	}
	else
	{
		// Enable color and attribute ram if not rom basic
		sysFlags |= (SYSTEM_FLAG_VIDEO_COLOR | SYSTEM_FLAG_VIDEO_ATTR) & g_Config.autoSysFlags;

		// Which Disk controller?
		if (filename[0]=='h' && filename[1]=='d')
		{
			sysFlags |= (SYSTEM_FLAG_WD1002 & g_Config.autoSysFlags);
		}
		else
		{
			sysFlags |= (SYSTEM_FLAG_WD1793 & g_Config.autoSysFlags);
		}
	}

	// Set system flags
	SystemFlagsPort = sysFlags | SYSTEM_FLAG_STATUS_INDICATORS;

	// Setup clock speed
	ClockMaskPort = g_Config.clockMask;

}
int ReadConfig()
{
	int err;
	NFFSDIRENTRY de;

	// Find config file
	err = nffsFindFile(&nffsctx, "fpgabee.cfg", &de);
	if (err == NFFS_ERR_NOTFOUND)
	{
		DefaultConfig();
		return WriteConfig();
	}
	if (err)
		return err;

	// Read to disk buffer
	if (!DiskRead(de.block, diskBuffer))
		return NFFS_ERR_READ;

	// Keep it
	memcpy(&g_Config, diskBuffer, sizeof(g_Config));
	if (g_Config.signature != FPGABEE_CONFIG_SIGNATURE)
	{
		DefaultConfig();
		return WriteConfig();
	}

	return NFFS_OK;
}

int WriteConfig()
{
	int err;
	NFFSDIRENTRY de;

	// Find config file
	err = nffsCreateFile(&nffsctx, "fpgabee.cfg", 1, sizeof(g_Config), &de);
	if (err)
		return err;

	g_Config.signature = FPGABEE_CONFIG_SIGNATURE;
	g_Config.version = FPGABEE_CONFIG_VERSION;

	// Copy to disk buffer
	memset(diskBuffer, 0, sizeof(diskBuffer));
	memcpy(diskBuffer, &g_Config, sizeof(g_Config));

	// Read to disk buffer
	if (!DiskWrite(de.block, diskBuffer))
	{
		return NFFS_ERR_WRITE;
	}

	// Keep it
	return NFFS_OK;
}

void InsertDiskSafe(uint8_t drive,  char* psz)
{
	if (InsertDisk(drive, psz, false, false) != 0)
	{
		// Remove from config menu
		psz[0]='\0';
	}
}

int ApplyConfig()
{
	InsertDiskSafe(1, g_Config.hdd);
	InsertDiskSafe(4, g_Config.fdd1);
	InsertDiskSafe(5, g_Config.fdd2);
	LoadRomPack(0, g_Config.rompack0);
	LoadRomPack(1, g_Config.rompack0);
	LoadRomPack(2, g_Config.rompack0);
	return 0;
}

void EjectFile(const char* pszFileName)
{
	bool dirty = false;
	if (strcmp(g_Config.hdd, pszFileName)==0)
	{
		EjectDisk(1);
		g_Config.hdd[0] = '\0';
		dirty = true;
	}
	if (strcmp(g_Config.fdd1, pszFileName)==0)
	{
		EjectDisk(4);
		g_Config.fdd1[0]='\0';
		dirty = true;
	}
	if (strcmp(g_Config.fdd2, pszFileName)==0)
	{
		EjectDisk(5);
		g_Config.fdd2[0]='\0';
		dirty = true;
	}
	if (strcmp(g_Config.rompack0, pszFileName)==0)
	{
		g_Config.rompack0[0]='\0';
		dirty = true;
	}
	if (strcmp(g_Config.rompack1, pszFileName)==0)
	{
		g_Config.rompack1[0]='\0';
		dirty = true;
	}
	if (strcmp(g_Config.rompack2, pszFileName)==0)
	{
		g_Config.rompack2[0]='\0';
		dirty = true;
	}

	if (dirty)
	{
		WriteConfig();
	}
}