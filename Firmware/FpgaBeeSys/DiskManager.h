uint8_t DiskTypeFromExtension(const char* filename);
uint32_t DiskBlockCount(uint8_t diskType);
void EjectDisk(uint8_t drive);
int InsertDisk(uint8_t drive, const char* filename, bool testOnly, bool showError);
