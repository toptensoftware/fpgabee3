#include "common.h"
#include "RomManager.h"
#include "DiskManager.h"
#include "DiskContext.h"
#include "config.h"
#include "ChooseFile.h"
#include "ConfigMenu.h"
#include "FileManager.h"
#include "TapePlayer.h"

#define COMMAND_FD1			0
#define COMMAND_FD2			1
#define COMMAND_PLAYTAPE	3
#define COMMAND_RECORDTAPE	4
#define COMMAND_CONFIGURE	6
#define COMMAND_FILEMANAGER	7
#define COMMAND_RESET		8

static char fdd1[] = "FD1: ***************";
static char fdd2[] = "FD2: ***************";

static char* items[] = {
	fdd1,
	fdd2,
	"\1",
	"Play Tape...",
	"Record Tape...",
	"\1",
	"Configure...",
	"File Manager...",
	"Reset",
	NULL
};

static void ShowFddNames()
{
	strcpy(fdd1 + 5, g_Config.fdd1);
	strcpy(fdd2 + 5, g_Config.fdd2);
}

static void ChooseFddImage(int drive)
{
	const char* pszFileName;
	char* dest;

	// Work out filename location
	if (drive==0)
		dest = g_Config.fdd1;
	else
		dest = g_Config.fdd2;

	// Pick a file
	pszFileName = ChooseFile("*.ds40;*.ss80;*.ds80;*.ds82;*.ds84;*.ds8B", dest, "[eject]");

	// Cancelled?
	if (pszFileName == NULL)
		return;

	// Store new drive name
	if (drive==0)
		strcpy(dest, pszFileName);
	else
		strcpy(dest, pszFileName);

	// Load image
	if (InsertDisk(4 + drive, pszFileName, false, true))
	{
		dest[0]='\0';		
	}

	// Save config
	WriteConfig();

	// Update menu
	ShowFddNames();
}


static void invokeCommand(LISTBOX* pListBox)
{
	switch (pListBox->selectedItem)
	{
		case COMMAND_FD1:
		case COMMAND_FD2:
			ChooseFddImage(pListBox->selectedItem - COMMAND_FD1);
			ListBoxDrawItem(pListBox, pListBox->selectedItem);
			break;
		
		case COMMAND_PLAYTAPE:
			PlayTapeUI();
			break;

		case COMMAND_RECORDTAPE:
			RecordTapeUI();
			break;

		case COMMAND_CONFIGURE:
			ConfigMenu();
			break;

		case COMMAND_FILEMANAGER:
			FileManager();
			ShowFddNames();		// In case disk ejected by rename, delete
			ListBoxDrawItem(pListBox, 0);
			ListBoxDrawItem(pListBox, 1);
			break;

		case COMMAND_RESET:
			SoftResetRequestPort = 0;
			break;
	}
}

size_t MainMenuProc(WINDOW* pWindow, MSG* pMsg)
{
	switch (pMsg->message)
	{
		case MESSAGE_KEYDOWN:
		{
			switch (pMsg->param1)
			{
				case VK_ESCAPE:
					return 0;

				case VK_ENTER:
					invokeCommand((LISTBOX*)pWindow);
					return 0;
			}
			break;
		}
	}
	
	return ListBoxWindowProc(pWindow, pMsg);
}

void MainMenu()
{
	LISTBOX lb;
	memset(&lb, 0, sizeof(LISTBOX));

	ShowFddNames();

	lb.window.rcFrame.left = 0;
	lb.window.rcFrame.top = 0;
	lb.window.rcFrame.width = 22;
	lb.window.rcFrame.height = 11;
	lb.window.attrNormal = MAKECOLOR(COLOR_WHITE, COLOR_BLUE);
	lb.window.attrSelected = MAKECOLOR(COLOR_BLACK, COLOR_YELLOW);
	lb.window.title = "FPGABee v3";
	lb.window.wndProc = MainMenuProc;
	lb.selectedItem = 0;
	ListBoxSetData(&lb, -1, items);

	RunModalWindow(&lb.window);
}
