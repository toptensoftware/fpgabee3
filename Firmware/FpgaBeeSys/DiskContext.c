#include "common.h"

NFFSCONTEXT nffsctx;
uint8_t diskBuffer[NFFS_BLOCK_SIZE];

void* nffsReadBlock(NFFSCONTEXT* ctx, uint32_t block)
{
	ctx;	// unused

	if (!DiskRead(block, diskBuffer))
	{
		return NULL;
	}
	return diskBuffer;
}

bool nffsWriteBlock(NFFSCONTEXT* ctx, uint32_t block, void* buffer)
{
	ctx;	// unused

	return DiskWrite(block, buffer == NULL ? diskBuffer : buffer);
}

void nffsLockBlock(NFFSCONTEXT* ctx, uint32_t block, bool lock)
{
	ctx; block; lock;		// unused

	// nop
}

void* nffsCreateBlock(NFFSCONTEXT* ctx, uint32_t block)
{
	ctx; block; 
	return diskBuffer;
}

