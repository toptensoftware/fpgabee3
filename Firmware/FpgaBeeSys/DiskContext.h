extern NFFSCONTEXT nffsctx;
extern uint8_t diskBuffer[NFFS_BLOCK_SIZE];

void* nffsReadBlock(NFFSCONTEXT* ctx, uint32_t block);
bool nffsWriteBlock(NFFSCONTEXT* ctx, uint32_t block, void* buffer);
void nffsLockBlock(NFFSCONTEXT* ctx, uint32_t block, bool lock);
void* nffsCreateBlock(NFFSCONTEXT* ctx, uint32_t block);
