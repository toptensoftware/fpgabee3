#include "common.h"
#include "RomManager.h"
#include "DiskManager.h"
#include "DiskContext.h"
#include "config.h"
#include "MainMenu.h"

void main(void) 
{
	int err;

	memset(VideoCharBuffer, ' ', 0x200);
	memset(VideoAttrBuffer, 0, 0x200);

	// Setup NFFS context
	memset(&nffsctx, 0, sizeof(NFFSCONTEXT));
	nffsctx.readBlock = nffsReadBlock;
	nffsctx.createBlock = nffsCreateBlock;
	nffsctx.writeBlock = nffsWriteBlock;
	nffsctx.lockBlock = nffsLockBlock;

	// Initialise NFFS
	err = nffsInit(&nffsctx);
	if (err)
	{
		DebugLeds = 0x88;
		return;
	}

	memset(VideoCharBuffer, 0, sizeof(VideoCharBuffer) * 2);
	//DisplayModePort = DISPLAY_MODE_ENABLE_VIDEO | DISPLAY_MODE_ENABLE_KEYBOARD;
	DisplayModePort = 0;


	if (ReadConfig())
	{
		DebugLeds = 0x89;
		return;
	}

	if (ApplyConfig())
	{
		DebugLeds = 0x8A;
		return;
	}

	// Enable NMI based yield back to Microbee control of the CPU
 	Yield = YieldNmiProc;

 	// Run the main menu
 	MainMenu();
}


/*
void runTestPattern()
{
	int i;
	uint8_t pattern = 0x01;

	while (true)
	{
		DebugLeds = pattern;

		// Delay
		for (i=0; i<10000; i++)
		{
			;
		}

		pattern = (pattern << 1) | pattern >> 7;
	}
}

*/