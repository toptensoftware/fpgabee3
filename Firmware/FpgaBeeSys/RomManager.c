#include "common.h"
#include "config.h"

int LoadRomPack(uint8_t romPackNumber, const char* filename)
{
	NFFSDIRENTRY de;
	uint32_t blockNumber;
	uint8_t i;
	int err;
	uint8_t* pDest;

	// Configure system according to ROM pack
	if (romPackNumber == 0)
	{
		ConfigureSystem(filename);
	}

	// Zap old rom pack
	MemoryBankPort = 0x18 | romPackNumber;
	memset(BankedMemory, 0, sizeof(BankedMemory));
	MemoryBankPort = 0;

	// Eject?
	if (filename == NULL || filename[0] == NULL)
	{
		return 0;
	}

	// Find rom file
	err = nffsFindFile(&nffsctx, filename, &de);
	if (err)
		return err;

	// Map to rom pack
	MemoryBankPort = 0x18 | romPackNumber;

	// Read file to high memory (0x8000)
	pDest = BankedMemory;
	blockNumber = de.block;
	for (i = 0; i < de.blockCount; i++)
	{
		if (!DiskRead(blockNumber, pDest))
			return NFFS_ERR_READ;
		pDest += 512;
		blockNumber++;
	}

	MemoryBankPort = 0;
	return 0;

}