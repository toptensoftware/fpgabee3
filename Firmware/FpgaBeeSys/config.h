#define FPGABEE_CONFIG_SIGNATURE	0x46434246	// "FBCF"
#define FPGABEE_CONFIG_VERSION		1

typedef struct tagCONFIG
{
	uint32_t	signature;
	uint32_t	version;
	uint8_t 	clockMask;
	uint16_t	sysFlags;
	uint16_t	autoSysFlags;
	char 		hdd[NFFS_MAX_FILENAME + 1];
	char 		fdd1[NFFS_MAX_FILENAME + 1];
	char 		fdd2[NFFS_MAX_FILENAME + 1];
	char 		rompack0[NFFS_MAX_FILENAME + 1];
	char    	rompack1[NFFS_MAX_FILENAME + 1];
	char    	rompack2[NFFS_MAX_FILENAME + 1];
} CONFIG;

extern CONFIG g_Config;

int ReadConfig();
int WriteConfig();
int ApplyConfig();
void EjectFile(const char* pszFileName);

void ConfigureSystem(const char* pszRom0);