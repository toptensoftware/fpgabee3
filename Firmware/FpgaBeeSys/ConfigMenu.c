#include "common.h"
#include "RomManager.h"
#include "DiskManager.h"
#include "RomManager.h"
#include "DiskContext.h"
#include "config.h"
#include "ChooseFile.h"
#include <stdio.h>

#define COMMAND_HDD			0
#define COMMAND_ROM0		2
#define COMMAND_ROM1		3
#define COMMAND_ROM2		4
#define COMMAND_SOUND		6
#define COMMAND_TAPEMONITOR	7
#define COMMAND_CLOCKSPEED     	9
#define COMMAND_DISKCONTROLLER 	10
#define COMMAND_VIDEO_COLOR		11
#define COMMAND_VIDEO_ATTRS		12

static char hdd[] =  "HDD:   ***************";
static char rom0[] = "ROM 1: ***************";
static char rom1[] = "ROM 2: ***************";
static char rom2[] = "ROM 3: ***************";
static char sound[] =           "Sound:                Off";
static char tapeMonitor[] =     "Tape Monitor:         Off";
static char clockSpeed[] =      "Clock Speed:    3.375 Mhz";
static char diskController[] =  "Disk Controller:   WD1002";
static char colorRam[] =        "Video Color:      Enabled";
static char attrRam[] =         "Video Attribs:   Disabled";

static char* items[] = {
	hdd,
	"\1",	
	rom0,
	rom1,
	rom2,
	"\1",
	sound,
	tapeMonitor,
	"\1",
	clockSpeed,
	diskController,
	colorRam,
	attrRam,
	NULL
};

const char* diskControllerNames[] = {
	"  None", "  Auto", "WD1002", "WD1793"
};

uint8_t diskControllerIndex()
{
	if (g_Config.autoSysFlags & (SYSTEM_FLAG_WD1793|SYSTEM_FLAG_WD1002))
		return 1;
	else if (g_Config.sysFlags & SYSTEM_FLAG_WD1002)
		return 2;
	else if (g_Config.sysFlags & SYSTEM_FLAG_WD1793)
		return 3;
	else
		return 0;
}

void setDiskControllerIndex(uint8_t index)
{
	g_Config.sysFlags &= ~(SYSTEM_FLAG_WD1002 | SYSTEM_FLAG_WD1793);
	g_Config.autoSysFlags &= ~(SYSTEM_FLAG_WD1002 | SYSTEM_FLAG_WD1793);
	switch (index)
	{
		case 1:
			g_Config.autoSysFlags |= SYSTEM_FLAG_WD1002 | SYSTEM_FLAG_WD1793;
			break;

		case 2:
			g_Config.sysFlags |= SYSTEM_FLAG_WD1002;
			break;

		case 3:
			g_Config.sysFlags |= SYSTEM_FLAG_WD1793;
			break;
	}
}

const char* clockSpeedNames[] = { 
	"3.375 Mhz",
	" 6.75 Mhz",
	" 13.5 Mhz",
	"   27 Mhz"
};

uint8_t clockSpeedIndex()
{
	switch (g_Config.clockMask)
	{
		case CLOCK_MASK_3_375: return  0;
		case CLOCK_MASK_6_750: return  1;
		case CLOCK_MASK_13_500: return 2;
		case CLOCK_MASK_27_000: return 3;
	}
	return 0;
}

void setClockSpeedIndex(uint8_t index)
{
	switch (index)
	{
		case 0:
			g_Config.clockMask = CLOCK_MASK_3_375;
			break;
			
		case 1:
			g_Config.clockMask = CLOCK_MASK_6_750;
			break;
			
		case 2:
			g_Config.clockMask = CLOCK_MASK_13_500;
			break;
			
		case 3:
			g_Config.clockMask = CLOCK_MASK_27_000;
			break;
	}
}

const char* enabledNames[] = {
	"Disabled",
	" Enabled",
	"    Auto",
};

uint8_t enabledAutoIndex(uint8_t mask)
{
	if (g_Config.autoSysFlags & mask)
		return 2;
	else
		return (g_Config.sysFlags & mask) ? 1 : 0;
}

void setEnabledAutoIndex(uint8_t mask, uint8_t index)
{
	g_Config.sysFlags &= ~mask;
	g_Config.autoSysFlags &= ~mask;
	switch (index)
	{
		case 1:
			g_Config.sysFlags |= mask;
			break;

		case 2:
			g_Config.autoSysFlags |= mask;
			break; 
	}
}

uint8_t videoAttrsIndex()
{
	return enabledAutoIndex(SYSTEM_FLAG_VIDEO_ATTR);
}

void setVideoAttrsIndex(uint8_t index)
{
	setEnabledAutoIndex(SYSTEM_FLAG_VIDEO_ATTR, index);
}

uint8_t videoColorIndex()
{
	return enabledAutoIndex(SYSTEM_FLAG_VIDEO_COLOR);
}

void setVideoColorIndex(uint8_t index)
{
	setEnabledAutoIndex(SYSTEM_FLAG_VIDEO_COLOR, index);
}

void cycleOption(uint8_t (*fnget)(), void (*fnset)(uint8_t), uint8_t count, int dir)
{
	int sel, newSel;
	if (dir== 0)
		dir = 1;
	sel = fnget();
	newSel = sel + dir;
	if (newSel < 0)
		newSel = count - 1;
	if (newSel >= count)
		newSel = 0;
	fnset((uint8_t)newSel);
}

static void ShowNames()
{
	strcpy(hdd + 7, g_Config.hdd);
	strcpy(rom0 + 7, g_Config.rompack0);
	strcpy(rom1 + 7, g_Config.rompack1);
	strcpy(rom2 + 7, g_Config.rompack2);
	strcpy(sound + 17, enabledNames[enabledAutoIndex(SYSTEM_FLAG_SOUND)]);
	strcpy(tapeMonitor + 17, enabledNames[enabledAutoIndex(SYSTEM_FLAG_TAPE_MONITOR)]);

	strcpy(diskController + 19, diskControllerNames[diskControllerIndex()]);
	strcpy(clockSpeed + 16, clockSpeedNames[clockSpeedIndex()]);
	strcpy(colorRam + 17, enabledNames[enabledAutoIndex(SYSTEM_FLAG_VIDEO_COLOR)]);
	strcpy(attrRam + 17, enabledNames[enabledAutoIndex(SYSTEM_FLAG_VIDEO_ATTR)]);
}

static void ChooseHddImage()
{
	const char* pszFileName = ChooseFile("*.hd0;*.hd1", g_Config.hdd, "[eject]");

	// Cancelled?
	if (pszFileName == NULL)
		return;

	// Load image
	if (InsertDisk(1, pszFileName, true, true))
		return;

	// Store new drive name
	strcpy(g_Config.hdd, pszFileName);

}

static void ChooseRomImage(int rom)
{
	char* dest;
	const char* pszFileName;

	// Store new drive name
	switch (rom)
	{
		case 0:
			dest = g_Config.rompack0;
			break;
		case 1:
			dest = g_Config.rompack1;
			break;

		default:
			dest = g_Config.rompack2;
			break;
	}

	// Pick a file
	pszFileName = ChooseFile("*.rom", dest, "[eject]");

	// Cancelled?
	if (pszFileName == NULL)
		return;

	strcpy(dest, pszFileName);

	// Save config
	WriteConfig();

	// Load image (unless system rom in which case wait to reboot)
	if (rom != 0)
	{
		LoadRomPack(rom, pszFileName);
	}
}

static void invokeCommand(LISTBOX* pListBox, int dir)
{
	switch (pListBox->selectedItem)
	{
		case COMMAND_HDD:
			if (dir!=0)
				return;
			ChooseHddImage();
			break;

		case COMMAND_ROM0:
		case COMMAND_ROM1:
		case COMMAND_ROM2:
			if (dir!=0)
				return;
			ChooseRomImage(pListBox->selectedItem - COMMAND_ROM0);	
			ListBoxDrawItem(pListBox, pListBox->selectedItem);
			break;

		case COMMAND_SOUND:
			g_Config.sysFlags ^= SYSTEM_FLAG_SOUND;
			SystemFlagsPort ^= SYSTEM_FLAG_SOUND;
			break;

		case COMMAND_TAPEMONITOR:
			g_Config.sysFlags ^= SYSTEM_FLAG_TAPE_MONITOR;
			SystemFlagsPort ^= SYSTEM_FLAG_TAPE_MONITOR;
			break;

		case COMMAND_CLOCKSPEED:
			cycleOption(clockSpeedIndex, setClockSpeedIndex, 4, dir);
			ClockMaskPort = g_Config.clockMask;
			break;

		case COMMAND_DISKCONTROLLER:
			cycleOption(diskControllerIndex, setDiskControllerIndex, 4, dir);
			break;

		case COMMAND_VIDEO_ATTRS:
			cycleOption(videoAttrsIndex, setVideoAttrsIndex, 3, dir);
			break;

		case COMMAND_VIDEO_COLOR:
			cycleOption(videoColorIndex, setVideoColorIndex, 3, dir);
			break;
	}

	ShowNames();
	ListBoxDrawItem(pListBox, pListBox->selectedItem);
	WriteConfig();
}

size_t ConfigMenuProc(WINDOW* pWindow, MSG* pMsg)
{
	if (pMsg->message == MESSAGE_KEYDOWN)
	{
		switch (pMsg->param1)
		{
			case VK_ENTER:
			{
				invokeCommand((LISTBOX*)pWindow, 0);
				return 0;
			}

			case VK_LEFT:
			{
				invokeCommand((LISTBOX*)pWindow, -1);
				return 0;
			}

			case VK_RIGHT:
			{
				invokeCommand((LISTBOX*)pWindow, 1);
				return 0;
			}
		}
	}

	return ListBoxWindowProc(pWindow, pMsg);
}


void ConfigMenu()
{
	LISTBOX lb;
	memset(&lb, 0, sizeof(LISTBOX));

	ShowNames();

	lb.window.rcFrame.left = 1;
	lb.window.rcFrame.top = 1;
	lb.window.rcFrame.width = 27;
	lb.window.attrNormal = MAKECOLOR(COLOR_WHITE, COLOR_BLUE);
	lb.window.attrSelected = MAKECOLOR(COLOR_BLACK, COLOR_YELLOW);
	lb.window.title = "Configure";
	lb.window.wndProc = ConfigMenuProc;
	lb.selectedItem = 0;
	ListBoxSetData(&lb, -1, items);

	lb.window.rcFrame.height = 2 + lb.getItemCount(&lb);

	// Draw list box content
	RunModalWindow(&lb.window);
}

