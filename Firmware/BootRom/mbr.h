#include <stdint.h>

typedef struct tagMBRPARTITIONENTRY
{
	uint8_t 	status;
	uint8_t 	startC;
	uint8_t 	startH;
	uint8_t 	startS;
	uint8_t		partType;
	uint8_t 	endC;
	uint8_t 	endH;
	uint8_t 	endS;
	uint32_t	firstSector;
	uint32_t	sectorCount;
} MBRPARTITIONENTRY;

typedef struct tagMBR
{
	uint8_t				bootstrapCode[446];
	MBRPARTITIONENTRY	partition[4];
	uint16_t			signature;
} MBR;

#define MBR_SIGNATURE	0xAA55