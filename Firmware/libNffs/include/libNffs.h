///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#ifndef __LIBNFSS_H
#define __LIBNFSS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

#ifdef _MSC_VER
#pragma pack(push, 1)
#endif

// Constants
#define NFFS_MAX_FILENAME			15
#define NFFS_BLOCK_SIZE				512
#define NFFS_DIRENTRIES_PER_BLOCK	(NFFS_BLOCK_SIZE / sizeof(NFFSDIRENTRY))

#define NFFS_SIGNATURE  ((uint32_t)(((uint32_t)'s' << 24) | ((uint32_t)'f' << 16) | ((uint32_t)'f' << 8) | ((uint32_t)'n')))
#define NFFS_VERSION	0x00010000

#define NFFS_OK						0		// No error
#define NFFS_ERR_READ				1		// Generic read error
#define NFFS_ERR_WRITE				2		// Generic write error
#define NFFS_ERR_INVALIDFS			3		// Not an NFFS partition
#define NFFS_ERR_VERSION			4		// Version number mismatch
#define NFFS_ERR_FILENAME			5		// Invalid filename
#define NFFS_ERR_NOTFOUND			6		// File not found
#define NFFS_ERR_CANCELLED			7		// Operation cancelled (enum files)
#define NFFS_ERR_ALREADYEXISTS		8		// File already exists (rename)
#define NFFS_ERR_SPACE				9		// Out of space
#define NFFS_ERR_MEMORY				10		// Out of memory
#define NFFS_ERR_TAIL				11		// Operation can't occur during active tail operation
#define NFFS_ERR_PARAMETER			12		// Invalid parameter (typically required parameter is null)
#define NFFS_ERR_READONLY			13      // Attempt to write when read-only
#define NFFS_ERR_ERRNO				14		// Error code is in crt errno
#define NFFS_ERR_UNKNOWN			15		// Unknown error
#define NFFS_ERR_NEEDSREPAIR        16		// File system is dirty and needs to be checked/repaired

// Root Sector
typedef struct tagNFFSROOTSECTOR
{
	uint32_t		signature;
	uint32_t		version;
	uint32_t		directoryBlock;
	uint32_t		directoryBlockCount;
	uint32_t		directoryHighWater;
	uint32_t		fileCount;
	uint32_t		totalBlocksUsed;
	uint32_t		totalBlocks;
	uint32_t		dirty;					// File system is dirty and needs to be checked before opening
} NFFSROOTSECTOR;

// Directory Entry
typedef struct tagNFFSDIRENTRY
{
	char			filename[NFFS_MAX_FILENAME+1];
	uint32_t		block;
	uint32_t		blockCount;
	uint32_t		length;
	uint32_t		unused;
} NFFSDIRENTRY;


// Runtime Context
typedef struct tagNFFSCONTEXT
{
	// Callbacks can cache blocks for improved performance however this implementation
	// only ever has one block active at a time.  ie: you can use just one
	// block of 512 bytes for all operations if tight on space.

	// Read a block from the underlying file system and return a pointer to memory
	// holding it's contents.  Memory only need to stay valid until next callback
	// Returned block may be modified and re-written using writeBlock
	void*			(*readBlock)(struct tagNFFSCONTEXT* ctx, uint32_t block);

	// Lock/unlock block. Used during directory enumeration to keep current directory block
	// locked across callbacks.
	void			(*lockBlock)(struct tagNFFSCONTEXT* ctx, uint32_t block, bool lock);

	// Allocate an empty block of memory that can be later written using writeBlock
	void*			(*createBlock)(struct tagNFFSCONTEXT* ctx, uint32_t block);

	// Write a previously read or created block back to disk.  pBuf will generally
	// be NULL indicating memory returned from read/create has been modified and should
	// be re-written.  Alternatively, pBuf may be a pointer to a different block returned
	// from a readBlock call - used when moving blocks.
	bool			(*writeBlock)(struct tagNFFSCONTEXT* ctx, uint32_t block, void* pBuf);

	// User defined data for whatever
	void*			user;

	// Starting block of active tail operation, or 0 if not active
	uint32_t		tailBlock;

	// A copy of the root sector
	NFFSROOTSECTOR	rootSector;
} NFFSCONTEXT;

typedef bool (*PFNENUMFILES)(uint32_t deIndex, const NFFSDIRENTRY* de, void* user);

// File operations
int nffsInit(NFFSCONTEXT* ctx);
int nffsInitNew(NFFSCONTEXT* ctx, uint32_t directoryBlocks, uint32_t totalBlocks);
int nffsRepair(NFFSCONTEXT* ctx);
int nffsFindFile(NFFSCONTEXT* ctx, const char* filename, NFFSDIRENTRY* ppde);
int nffsCreateFile(NFFSCONTEXT* ctx, const char* filename, uint32_t blocks, uint32_t length, NFFSDIRENTRY* ppde);
int nffsSetFileLength(NFFSCONTEXT* ctx, const char* filename, uint32_t length, bool trimUnusedBlocks);
int nffsDeleteFile(NFFSCONTEXT* ctx, const char* filename);
int nffsRenameFile(NFFSCONTEXT* ctx, const char* oldname, const char* newname);
int nffsEnumFiles(NFFSCONTEXT* ctx, PFNENUMFILES callback, void* user);
int nffsExists(NFFSCONTEXT* ctx, const char* filename);
int nffsDuplicateFile(NFFSCONTEXT* ctx, const char* oldname, const char* newname);
const char* nffsErrorMessage(int err);

// Tail operations allow writing to unused blocks at the end of the currently used
// space and then saving as a file afterwards.
// Only one such operation can be active at a time and calls to nffsCreateFile will
// fail while a tail operation is in progress.
int nffsCreateTail(NFFSCONTEXT* ctx, uint32_t* pBlock, uint32_t* pAvailableBlocks);
int nffsDiscardTail(NFFSCONTEXT* ctx);
int nffsSaveTail(NFFSCONTEXT* ctx, const char* filename, uint32_t length, NFFSDIRENTRY* pde);

// Block helpers
int nffsMoveBlocks(NFFSCONTEXT* ctx, uint32_t block, uint32_t blockCount, uint32_t newLocation);
int nffsZeroBlocks(NFFSCONTEXT* ctx, uint32_t block, uint32_t blockCount);

// Filename helpers
bool nffsGlob(const char* filename, const char* glob);
char* nffsGlobReplace(const char* file, const char* pattern, char* pVal);
bool nffsContainsWildcards(const char* filename);
bool nffsIsValidFileName(const char* filename);
const char* nffsExtension(const char* filename);
int nffsEnsureExtension(char* filename, const char* ext);

typedef struct tagNFFSSTREAM
{
	NFFSCONTEXT* owner;
	uint32_t position;
	uint32_t length;
	uint32_t currentBlock;
	bool currentBlockDirty;
	char buffer[NFFS_BLOCK_SIZE];
	char* filename;
	bool tailStream;
	bool readOnly;
	uint32_t block;
	uint32_t blockCount;
} NFFSSTREAM;

// Stream based API
int nffsStreamOpen(NFFSCONTEXT* ctx, const char* filename, bool readOnly, NFFSSTREAM** pVal);
int nffsStreamCreate(NFFSCONTEXT* ctx, const char* filename, uint32_t length, NFFSSTREAM** pVal);
int nffsStreamClose(NFFSSTREAM* stream);
int nffsStreamFlush(NFFSSTREAM* stream);
int nffsStreamWrite(NFFSSTREAM* stream, void* buf, uint32_t bytes);
int nffsStreamRead(NFFSSTREAM* stream, void* buf, uint32_t bytes, uint32_t* pBytesRead);
int nffsStreamSeek(NFFSSTREAM* stream, uint32_t position);
int nffsStreamTell(NFFSSTREAM* stream, uint32_t* pVal);
int nffsStreamSetLength(NFFSSTREAM* stream, uint32_t val);
int nffsStreamGetLength(NFFSSTREAM* stream, uint32_t* pVal);



#ifdef _MSC_VER
#pragma pack(pop)
#endif

#ifdef __cplusplus
}
#endif

#endif		// LIBNFFS_H
