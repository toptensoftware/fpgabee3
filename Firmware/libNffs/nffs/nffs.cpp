#include "common.h"	
#include "nffsFile.h"

#include "glob.h"

#include "SimpleJson.h"

/*
TODO:

1. Mark root sector as needing check before all write ops
2. Check file system after open

*/

void show_usage()
{
	printf("nffs v1.0 - Non-Fragmented File System Utility\n");
	printf("Copyright (C) 2017 Topten Software.  All Rights Reserved\n\n");
	printf("Usage: nffs <fsimage> <command> [args]\n\n");
	printf("    fsimage            image of the nffs file system to work with\n");
	printf("    command            see commands below\n");
	printf("    args               additional command argument\n");
	printf("\n");
	printf("Commands:\n");
	printf("\n");
	printf("    format                          create a new nffs file system\n");
	printf("    ls [<spec>]                     list files in fsimage\n");
	printf("    add <file> [newname]            add file to fsimage\n");
	printf("    extract <file> [<diskimage>]    extract file from fsimage\n");
	printf("    compare <file> [<diskimage>]    compare file to disk image\n");
	printf("    cp <oldfile> <newfile>          copy file in fsimage\n");
	printf("    mv <oldfile> <newfile>          rename file in fsimage\n");
	printf("    rm <file>                       remove file from fsimage\n");
	printf("    make <file> <size>[b|k|m|s]     make a file filled with zeros [bytes|kilobytes|megabytes|sectors]\n");
	printf("    transfer <newimage> [glob]      ftransfer entire file system to newimage\n");
	printf("    repair                          repair file system\n");
	printf("\n");
	printf("eg:\n");
	printf("\n");
	printf("    nffs myfs.nffs format\n");
	printf("    nffs myfs.nffs add fpgabee_system.bin\n");
	printf("    nffs myfs.nffs add hd.hd0\n");
	printf("    nffs myfs.nffs add hd18.rom\n");
	printf("    nffs myfs.nffs add blank.ds40\n");
	printf("    nffs myfs.nffs make newfile.bin 64k\n");
	printf("    nffs myfs.nffs ls\n");
	printf("    nffs myfs.nffs transfer k:\n");
	printf("\n");
}

int show_error_errno(int error, const char* action)
{
	if (error==0)
		return 0;

	fprintf(stderr, "\nFailed to %s - %s (%i)\n\n", action, strerror(error), error);
	return error;
}

int show_error_nffs(int error, const char* action)
{
	if (error==0)
		return 0;

	if (error == NFFS_ERR_ERRNO)
		return show_error_errno(errno, action);

	fprintf(stderr, "\nFailed to %s - %s (%i)\n\n", action, nffsErrorMessage(error), error);
	return error;
}

CAnsiString globReplace(const char* file, const char* pattern)
{
	char szTemp[512];
	nffsGlobReplace(file, pattern, szTemp);
	return szTemp;
}


NFFSFS* g_pFS = NULL;
NFFSFS* g_pFS2 = NULL;
NFFSSTREAM* g_pStream = NULL;
NFFSSTREAM* g_pStream2 = NULL;
FILE* g_pFile = NULL;


#define RIF_NFFS(x, msg) { int _res=x; if (_res!=0) return show_error_nffs(_res, msg); }
#define RIF_ERRNO(x, msg) { int _res=x; if (_res!=0) return show_error_errno(_res, msg); }



// Format command
int invoke_format(int argc, char* argv[])
{
	FILE* pFile;
	RIF_NFFS(nffsOpenFileOrVolume(argv[1], true, false, &pFile), "open volume");
	RIF_NFFS(nffsStreamInitNew(pFile, 8, &g_pFS), "initialize file system");
	RIF_NFFS(nffsStreamUninit(g_pFS, true), "close file system");
	g_pFS = NULL;
	return 0;
}


// List files callback
int fileCount = 0;
bool ls_files(uint32_t deIndex, const NFFSDIRENTRY* de, void* user)
{
	// Filter
	const char* glob = (const char*)user;
	if (glob && !nffsGlob(de->filename, glob))
		return true;

	printf("%8u %8u %8u %-30s\n", de->block, de->blockCount, de->length, de->filename);
	fileCount++;
	return true;
}


// ls command
int invoke_ls(int argc, char* argv[])
{
	// Open
	RIF_NFFS(nffsStreamInit(argv[1], true, &g_pFS), "open file system");

	// Header
	printf("%8s %8s %8s %-30s\n", "blk#", "blks", "size", "filename");
	printf("----------------------------------------------------\n");

	// List files
	RIF_NFFS(nffsEnumFiles(&g_pFS->ctx, ls_files, argc > 3 ? argv[3] : NULL), "enumerate files");

	// Close fs
	RIF_NFFS(nffsStreamUninit(g_pFS, true), "close file system");
	g_pFS = NULL;

	// Summary
	printf("\nTotal %i files\n", fileCount);

	return 0;
}

int invoke_add(int argc, char* argv[])
{
	// Check args
	if (argc<4)
	{
		fprintf(stderr, "Insufficient arguments for add command\n");
		return 7;
	}

	// Get list of files
	CVector<CAnsiString> files;
	RIF_ERRNO(glob_files(argv[3], files), "glob files");

	// Open
	RIF_NFFS(nffsStreamInit(argv[1], false, &g_pFS), "open file system");

	for (int i=0; i<files.GetSize(); i++)
	{
		CAnsiString targetName = GetFileName(files[i]);
		if (argc>=5)
			targetName = globReplace(targetName, argv[4]);

		printf("Adding: %s... ", targetName.sz());

		// Open source stream
		g_pFile = fopen(files[i], "rb");
		if (g_pFile == NULL)
		{
			show_error_errno(errno, "open file");
			return errno;
		}

		// Work out file length
		fseek(g_pFile, 0, SEEK_END);
		uint32_t length = ftell(g_pFile);
		fseek(g_pFile, 0, SEEK_SET);


		// Open destination
		RIF_NFFS(nffsStreamCreate(&g_pFS->ctx, targetName, length, &g_pStream), "create stream");

		// Copy stream...
		char buf[512];
		while (true)
		{
			// Read source
			int read = (int)fread(buf, 1, (int)sizeof(buf), g_pFile);
			if (read == 0)
				break;

			// Write destination
			RIF_NFFS(nffsStreamWrite(g_pStream, buf, read), "write taget");
		}

		RIF_NFFS(nffsStreamClose(g_pStream), "close stream");
		g_pStream = NULL;

		fclose(g_pFile);
		g_pFile = NULL;

		printf("ok\n");
	}
	return 0;
}

int invoke_extract(int argc, char* argv[])
{
	if (argc<4)
	{
		fprintf(stderr, "Insufficient arguments for extract command\n");
		return 7;
	}

	// Open fs
	RIF_NFFS(nffsStreamInit(argv[1], true, &g_pFS), "open file system ");

	CVector<CAnsiString> files;
	RIF_NFFS(nffs_glob_files(&g_pFS->ctx, argv[3], files), "enumerate files");

	for (int i=0; i<files.GetSize(); i++)
	{
		printf("Extracting: %s... ", files[i].sz());

		// Open destination
		RIF_NFFS(nffsStreamOpen(&g_pFS->ctx, files[i], true, &g_pStream), "open stream");

		// Work out file length
		uint32_t length;
		RIF_NFFS(nffsStreamGetLength(g_pStream, &length), "get stream length");

		CAnsiString targetName = files[i];
		if (argc>=5)
			targetName = globReplace(targetName, argv[4]);


		// Open source stream
		g_pFile = fopen(targetName, "wb");
		if (!g_pFile)
		{
			show_error_errno(errno, "create file");
			return errno;
		}

		// Copy stream...
		char buf[512];
		while (true)
		{
			// Read source
			uint32_t read;
			RIF_NFFS(nffsStreamRead(g_pStream, buf, sizeof(buf), &read), "read stream");
			if (read == 0)
				break;

			// Write destination
			fwrite(buf, read, 1, g_pFile);
		}

		// Clean up
		RIF_NFFS(nffsStreamClose(g_pStream), "close stream");
		g_pStream = NULL;

		fclose(g_pFile);
		g_pFile = NULL;

		printf("ok\n");
	}

	return 0;
}

int invoke_compare(int argc, char* argv[])
{
	if (argc<4)
	{
		fprintf(stderr, "Insufficient arguments for compare command\n");
		return 7;
	}


	// Open fs
	RIF_NFFS(nffsStreamInit(argv[1], true, &g_pFS), "open file system");

	CVector<CAnsiString> files;
	RIF_NFFS(nffs_glob_files(&g_pFS->ctx, argv[3], files), "enumerate files");

	for (int i=0; i<files.GetSize(); i++)
	{
		printf("Comparing: %s... ", files[i].sz());

		// Open source
		RIF_NFFS(nffsStreamOpen(&g_pFS->ctx, files[i], true, &g_pStream), "open stream");

		CAnsiString targetName = files[i];
		if (argc>=5)
			targetName = globReplace(targetName, argv[4]);

		// Open source stream
		g_pFile = fopen(targetName, "rb");
		if (g_pFile == NULL)
		{
			printf("file not found\n");
			RIF_NFFS(nffsStreamClose(g_pStream), "close stream");
			g_pStream = NULL;
		}
		else
		{

			bool equal = true;

			// Compare streams...
			char buf1[512];
			char buf2[512];
			uint32_t pos = 0;
			while (equal)
			{
				// Read source
				uint32_t read1;
				RIF_NFFS(nffsStreamRead(g_pStream, buf1, sizeof(buf1), &read1), "read stream");

				// Read dest
				uint32_t read2 = (uint32_t)fread(buf2, 1, sizeof(buf2), g_pFile);

				// Check length
				if (read1 != read2)
				{
					equal = false;
					printf("length mismatch\n");
				}
				else if (memcmp(buf1, buf2, read1) != 0)
				{
					equal = false;
					printf("content mismatch at %u\n", pos);
				}

				if (read1 < sizeof(buf1))
					break;

				pos += read1;
			}

			// Clean up
			RIF_NFFS(nffsStreamClose(g_pStream), "close stream");
			g_pStream = NULL;

			fclose(g_pFile);
			g_pFile = NULL;

			if (equal)
			{
				printf("same\n");
			}
		}
	}

	return 0;
}

int invoke_rm(int argc, char* argv[])
{
	if (argc<4)
	{
		fprintf(stderr, "Insufficient arguments for rm command\n");
		return 7;
	}

	// Open
	RIF_NFFS(nffsStreamInit(argv[1], false, &g_pFS), "open file system");

	CVector<CAnsiString> files;
	RIF_NFFS(nffs_glob_files(&g_pFS->ctx, argv[3], files), "enumerate files");

	for (int i=0; i<files.GetSize(); i++)
	{
		printf("Removing %s... ", files[i].sz());

		// Delete the file
		RIF_NFFS(nffsDeleteFile(&g_pFS->ctx, files[i]), "delete file");

		printf("ok\n");
	}


	return 0;
}


int invoke_mv(int argc, char* argv[])
{
	if (argc<5)
	{
		fprintf(stderr, "Insufficient arguments for mv command\n");
		return 7;
	}

	// Open
	RIF_NFFS(nffsStreamInit(argv[1], false, &g_pFS), "open file system");


	CVector<CAnsiString> files;
	RIF_NFFS(nffs_glob_files(&g_pFS->ctx, argv[3], files), "enumerate files");

	for (int i=0; i<files.GetSize(); i++)
	{
		CAnsiString targetName = files[i];
		if (argc>=5)
			targetName = globReplace(targetName, argv[4]);

		printf("Renaming %s to %s... ", files[i].sz(), targetName.sz());

		// Delete the file
		RIF_NFFS(nffsRenameFile(&g_pFS->ctx, files[i], targetName), "rename file");

		printf("ok\n");
	}

	return 0;
}

#ifdef _DEBUG
void test(const char* expected, const char* f, const char* p)
{
	char szOutput[512];
	nffsGlobReplace(f, p, szOutput);
	printf("'%s' + '%s' = '%s'", f, p, szOutput);

	if (strcmpi(szOutput, expected)==0)
		printf(" (pass)\n");
	else
		printf(" (FAIL)\n");
}

void globReplaceTest()
{
     const char* targetMask = "?????.?????";
        test("a", "a", targetMask);
        test("a.b", "a.b", targetMask);
        test("a.b", "a.b.c", targetMask);
        test("part1.part2", "part1.part2.part3", targetMask);
        test("12345.12345", "123456.123456.123456", targetMask);

        targetMask = "A?Z*";
        test("AZ", "1", targetMask);
        test("A2Z", "12", targetMask);
        test("AZ.txt", "1.txt", targetMask);
        test("A2Z.txt", "12.txt", targetMask);
        test("A2Z", "123", targetMask);
        test("A2Z.txt", "123.txt", targetMask);
        test("A2Z4", "1234", targetMask);
        test("A2Z4.txt", "1234.txt", targetMask);

        targetMask = "*.txt";
        test("a.txt", "a", targetMask);
        test("b.txt", "b.dat", targetMask);
        test("c.x.txt", "c.x.y", targetMask);

        targetMask = "*?.bak";
        test("a.bak", "a", targetMask);
        test("b.dat.bak", "b.dat", targetMask);
        test("c.x.y.bak", "c.x.y", targetMask);

        targetMask = "*_NEW.*";
        test("abcd_NEW.txt", "abcd_12345.txt", targetMask);
        test("abc_newt_NEW.dat", "abc_newt_1.dat", targetMask);
        test("abcd_123.a_NEW", "abcd_123.a_b", targetMask);

        targetMask = "?x.????999.*rForTheCourse";

        test("px.part999.rForTheCourse", "part1.part2", targetMask);
        test("px.part999.parForTheCourse", "part1.part2.part3", targetMask);
        test("ax.b999.crForTheCourse", "a.b.c", targetMask);
        test("ax.b999.CarParForTheCourse", "a.b.CarPart3BEER", targetMask);
}
#endif

int invoke_cp(int argc, char* argv[])
{
	if (argc<5)
	{
		fprintf(stderr, "Insufficient arguments for cp command\n");
		return 7;
	}

	// Open
	RIF_NFFS(nffsStreamInit(argv[1], false, &g_pFS), "open file system");

	CVector<CAnsiString> files;
	RIF_NFFS(nffs_glob_files(&g_pFS->ctx, argv[3], files), "enumerate files");

	for (int i=0; i<files.GetSize(); i++)
	{
		CAnsiString targetName = files[i];
		if (argc>=5)
			targetName = globReplace(targetName, argv[4]);

		printf("Copying %s to %s... ", files[i].sz(), targetName.sz());

		if (strcmp(files[i], targetName)==0)
		{
			printf("same file!\n");
			continue;
		}

		// Open source stream
		RIF_NFFS(nffsStreamOpen(&g_pFS->ctx, files[i].sz(), true, &g_pStream), "create target stream");

		// Create target stream
		RIF_NFFS(nffsStreamCreate(&g_pFS->ctx, targetName, 0, &g_pStream2), "create target stream");

		// Copy bytes
		char buf[512];
		while (true)
		{
			// Read source
			uint32_t read;
			int err = nffsStreamRead(g_pStream, buf, sizeof(buf), &read);
			if (err)
			{
				show_error_nffs(err, "read source stream");
				break;
			}

			// EOF?
			if (read == 0)
				break;

			// Write target
			err = nffsStreamWrite(g_pStream2, buf, read);
			if (err)
			{
				show_error_nffs(err, "write target stream");
				break;
			}
		}

		// Close streams
		nffsStreamClose(g_pStream);
		nffsStreamClose(g_pStream2);
		g_pStream = NULL;
		g_pStream2 = NULL;

		printf("ok\n");

	}

	return 0;
}

int invoke_transfer(int argc, char* argv[])
{
	// Check args
	if (argc<4)
	{
		fprintf(stderr, "Insufficient arguments for transfer command\n");
		return 7;
	}

	// Open the source file system
	RIF_NFFS(nffsStreamInit(argv[1], true, &g_pFS), "open source file system");

	// Open the target file system
	FILE* fileTarget;
	RIF_NFFS(nffsOpenFileOrVolume(argv[3], true, false, &fileTarget), "open target volume");
	RIF_NFFS(nffsStreamInitNew(fileTarget, g_pFS->ctx.rootSector.directoryBlockCount, &g_pFS2), "create target file system");

	CVector<CAnsiString> files;
	RIF_NFFS(nffs_glob_files(&g_pFS->ctx, argc>=5 ? argv[4] : NULL, files), "enumerate files");

	for (int i = 0; i < files.GetSize(); i++)
	{
		printf("Adding: %s... ", files[i].sz());

		// Open source stream
		RIF_NFFS(nffsStreamOpen(&g_pFS->ctx, files[i].sz(), true, &g_pStream), "open source stream");

		// Create target stream
		RIF_NFFS(nffsStreamCreate(&g_pFS2->ctx, files[i].sz(), 0, &g_pStream2), "open target stream");

		// Copy bytes
		char buf[512];
		while (true)
		{
			// Read source
			uint32_t read;
			int err = nffsStreamRead(g_pStream, buf, sizeof(buf), &read);
			if (err)
			{
				show_error_nffs(err, "read source stream");
				break;
			}

			// EOF?
			if (read == 0)
				break;

			// Write target
			err = nffsStreamWrite(g_pStream2, buf, read);
			if (err)
			{
				show_error_nffs(err, "write target stream");
				break;
			}
		}

		// Close streams
		nffsStreamClose(g_pStream2);
		nffsStreamClose(g_pStream);
		g_pStream = NULL;
		g_pStream2 = NULL;

		printf("ok\n");
	}

	printf("\nFile system transferred\n");

	return 0;

}

int invoke_repair(int argc, char* argv[])
{
	// Open the source file system
	RIF_NFFS(nffsStreamRepairAndInit(argv[1], &g_pFS), "open source file system");

	printf("\nFile system repaired\n");

	return 0;

}

int invoke_make(int argc, char* argv[])
{
	// Check args
	if (argc<5)
	{
		fprintf(stderr, "Insufficient arguments for add command\n");
		return 7;
	}

	// How many bytes?
	int bytes = atoi(argv[4]);
	if (bytes<0)
	{
		fprintf(stderr, "Invalid size argument for make command\n");
		return 7;
	}

	const char* suffix = argv[4];
	while (*suffix == ' ' || (*suffix>='0' && *suffix <='9'))
		suffix++;

	if (*suffix == 'k' || *suffix == 'K')
		bytes *= 1024;
	else if (*suffix == 'm' || *suffix == 'M')
		bytes *= 1024 * 1024;
	else if (*suffix == 'b' || *suffix == 'B')
		bytes *= 1;
	else if (*suffix == 's' || *suffix == 'S')
		bytes *= 512;
	else if (*suffix != '\0')
	{
		fprintf(stderr, "Invalid suffix on number (b=blocks, k=kilobytes, m=megabytes)\n");
		return 7;
	}


	// Open
	RIF_NFFS(nffsStreamInit(argv[1], false, &g_pFS), "open file system");

	printf("Making: %s... ", argv[3]);

	// Open destination
	RIF_NFFS(nffsStreamCreate(&g_pFS->ctx, argv[3], bytes, &g_pStream), "create stream");

	// Copy stream...
	char buf[512];
	memset(buf, 0, sizeof(buf));
	while (true)
	{
		// Read source
		int read = bytes > 512 ? 512 : bytes;
		if (read == 0)
			break;

		// Write destination
		RIF_NFFS(nffsStreamWrite(g_pStream, buf, read), "write taget");

		bytes -= read;
	}

	printf("ok\n");
	return 0;
}


int main(int argc, char* argv[])
{
	SimpleJsonTest();

	if (argc<3)
	{
		show_usage();
		return 7;
	}

	int retv = 7;
	if (strcmp(argv[2], "format")==0)
	{
		retv = invoke_format(argc, argv);
	}
	else if (strcmp(argv[2], "ls")==0)
	{						 
		retv = invoke_ls(argc,argv);
	}
	else if (strcmp(argv[2], "add")==0)
	{
		retv = invoke_add(argc, argv);
	}
	else if (strcmp(argv[2], "extract") == 0)
	{
		retv = invoke_extract(argc, argv);
	}
	else if (strcmp(argv[2], "compare") == 0)
	{
		retv = invoke_compare(argc, argv);
	}
	else if (strcmp(argv[2], "rm") == 0)
	{
		retv = invoke_rm(argc, argv);
	}
	else if (strcmp(argv[2], "mv") == 0)
	{
		retv = invoke_mv(argc, argv);
	}
	else if (strcmp(argv[2], "cp") == 0)
	{
		retv = invoke_cp(argc, argv);
	}
	else if (strcmp(argv[2], "transfer")==0)
	{
		retv = invoke_transfer(argc, argv);
	}
	else if (strcmp(argv[2], "repair")==0)
	{
		retv = invoke_repair(argc, argv);
	}
	else if (strcmp(argv[2], "make")==0)
	{
		retv = invoke_make(argc, argv);
	}
	else 
	{
		printf("unknown command '%s'", argv[1]);
		retv = 7;
	}

	// Close streams
	if (g_pStream)
		nffsStreamClose(g_pStream);
	if (g_pStream2)
		nffsStreamClose(g_pStream2);

	// Close file
	if (g_pFile)
		fclose(g_pFile);

	// Close file systems
	if (g_pFS)
		nffsStreamUninit(g_pFS, true);
	if (g_pFS2)
		nffsStreamUninit(g_pFS2, true);

	return retv;
}
				
