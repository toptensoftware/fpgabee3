#ifndef _GLOB_H
#define _GLOB_H

int nffs_glob_files(NFFSCONTEXT* ctx, const char* pattern, CVector<CAnsiString>& results);
CAnsiString GetFileName(const char* pszFileName);
int glob_files(const char* pattern, CVector<CAnsiString>& results);


#endif // _GLOB_H