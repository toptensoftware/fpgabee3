#include "common.h"
#include "nffsFile.h"

struct GLOBCONTEXT
{
	const char* pattern;
	CVector<CAnsiString>* pResults;
};

bool glob_files_callback(uint32_t deIndex, const NFFSDIRENTRY* de, void* user)
{
	GLOBCONTEXT* pgctx = (GLOBCONTEXT*)user;

	// Filter
	if (pgctx->pattern && !nffsGlob(de->filename, pgctx->pattern))
		return true;

	pgctx->pResults->Add(de->filename);
	return true;
}

// Helper to glob files
int nffs_glob_files(NFFSCONTEXT* ctx, const char* pattern, CVector<CAnsiString>& results)
{
	GLOBCONTEXT gctx;
	gctx.pattern = pattern;
	gctx.pResults = &results;

	// List files
	return nffsEnumFiles(ctx, glob_files_callback, &gctx);
}

// Get just the filename part of a qualified path
CAnsiString GetFileName(const char* pszFileName)
{
	const char* lastSlash = strrchr(pszFileName, PATHSEP);
	if (lastSlash)
		return lastSlash + 1;
	else
		return pszFileName;
}

int glob_files(const char* pattern, CVector<CAnsiString>& results)
{
	// Does it have wildcards?
	if (!nffsContainsWildcards(pattern))
	{
		results.Add(pattern);
		return NFFS_OK;
	}

	// Split into directory and pattern
	CAnsiString strDir;
	CAnsiString strPattern;
	const char* lastSlash = strrchr(pattern, PATHSEP);
	if (lastSlash)
	{
		strDir = CAnsiString(pattern, (int)(lastSlash - pattern + 1));
		strPattern = lastSlash + 1;
	}
	else
	{
		strDir = ".";
		strPattern = pattern;
	}


	// Read directory
	DIR *dir;
    struct dirent *ent;
	/* Open directory stream */
    dir = opendir(strDir);
    if (dir == NULL)
		return errno;

	/* Print all files and directories within the directory */
    while ((ent = readdir (dir)) != NULL) 
	{
        if (ent->d_type == DT_REG)
		{
			if (nffsGlob(ent->d_name, strPattern))
				results.Add(Format("%s%c%s", strDir.sz(), PATHSEP, ent->d_name));
        }
    }

    closedir (dir);
	return 0;
}

