#ifndef __SIMPLEJSON_H
#define __SIMPLEJSON_H

struct JsonValueType
{
	enum value
	{
		Null,
		Undefined,
		Bool,
		Number,
		String,
		Array,
		Map,
		Error,
	};
};

class CJsonNull;
class CJsonUndefined;
class CJsonBool;
class CJsonNumber;
class CJsonString;
class CJsonArray;
class CJsonMap;
class CJsonError;
class CJsonTokenizer;

class CJsonValue
{
public:
	CJsonValue()
	{
	}
	virtual ~CJsonValue()
	{
	}

	virtual JsonValueType::value GetType() = 0;
	virtual CJsonNull* ToNull() { return NULL; }
	virtual CJsonUndefined* ToUndefined() { return NULL; }
	virtual CJsonBool* ToBool() { return NULL; }
	virtual CJsonNumber* ToNumber() { return NULL; }
	virtual CJsonString* ToString() { return NULL; }
	virtual CJsonArray* ToArray() { return NULL; }
	virtual CJsonMap* ToMap() { return NULL; }
	virtual CJsonError* ToError() { return NULL; }

	CJsonNull* AsNull() { return this == NULL ? NULL : this->ToNull(); }
	CJsonUndefined* AsUndefined() { return this == NULL ? NULL : this->ToUndefined(); }
	CJsonBool* AsBool() { return this == NULL ? NULL : this->ToBool(); }
	CJsonNumber* AsNumber() { return this == NULL ? NULL : this->ToNumber(); }
	CJsonString* AsString() { return this == NULL ? NULL : this->ToString(); }
	CJsonArray* AsArray() { return this == NULL ? NULL : this->ToArray(); }
	CJsonMap* AsMap() { return this == NULL ? NULL : this->ToMap(); }
	CJsonError* AsError() { return this == NULL ? NULL : this->ToError(); }

	CJsonValue* GetMember(const wchar_t* psz);
};

class CJsonNull	: public CJsonValue
{
public:
	CJsonNull()
	{
	}
	virtual JsonValueType::value GetType() { return JsonValueType::Null; }
	virtual CJsonNull* ToNull() { return this; }
};

class CJsonUndefined : public CJsonValue
{
public:
	CJsonUndefined()
	{
	}
	virtual JsonValueType::value GetType() { return JsonValueType::Undefined; }
	virtual CJsonUndefined* ToUndefined() { return this; }
};

class CJsonBool : public CJsonValue
{
public:
	CJsonBool(bool value)
	{
		_value = value;
	}
	virtual JsonValueType::value GetType() { return JsonValueType::Bool; }
	virtual CJsonBool* ToBool() { return this; }

	bool GetValue() { return _value; }
	void SetValue(bool value) { _value = value; }

private:
	bool _value;
};

class CJsonNumber : public CJsonValue
{
public:
	CJsonNumber(double value)
	{
		_value = value;
	}

	virtual JsonValueType::value GetType() { return JsonValueType::Number; }
	virtual CJsonNumber* ToNumber() { return this; }

	double GetValue() { return _value; }
	void SetValue(double value) { _value = value; }

private:
	double _value;
};

class CJsonString : public CJsonValue
{
public:
	CJsonString(const wchar_t* psz)
	{
		_value = psz;
	}

	virtual JsonValueType::value GetType() { return JsonValueType::String; }
	virtual CJsonString* ToString() { return this; }

	CUniString GetValue() { return _value.sz(); }
	void SetValue(const wchar_t* psz) { _value = psz; }

private:
	CUniString _value;
};

class CJsonArray : 
	public CVector<CJsonValue*, SOwnedPtr>,
	public CJsonValue
{
public:
	CJsonArray()
	{
	}
	virtual ~CJsonArray()
	{
	}

	virtual JsonValueType::value GetType() { return JsonValueType::Array; }
	virtual CJsonArray* ToArray() { return this; }
};

class CJsonMap : 
	public CMap<CUniString, CJsonValue*, SValue, SOwnedPtr>,
	public CJsonValue
{
public:
	CJsonMap()
	{

	}
	virtual ~CJsonMap()
	{
	}

	virtual JsonValueType::value GetType() { return JsonValueType::Map; }
	virtual CJsonMap* ToMap() { return this; }
};

class CJsonError : public CJsonValue
{
public:
	CJsonError(const wchar_t* psz, CJsonTokenizer& tok)
	{
		_message = psz;
	}

	virtual JsonValueType::value GetType() { return JsonValueType::Error; }
	virtual CJsonError* ToError() { return this; }

	CUniString GetMessage() { return _message.sz(); }

private:
	CUniString _message;
};


struct JsonToken
{
	enum value
	{
		eof,
		Identifier,
		String,
		Number,
		Bool,
		Null,
		Undefined,
		OpenBrace,
		CloseBrace,
		OpenSquare,
		CloseSquare,
		Equal,
		Colon,
		SemiColon,
		Comma,
		Error,
	};
};


class CJsonTokenizer
{
public:
	CJsonTokenizer(const wchar_t* psz, bool strict);

public:
	CUniString GetStringValue() { return _string; }
	double GetNumberValue() { return _number; }
	bool GetBoolValue() { return _bool; }

	void NextToken();
	JsonToken::value CurrentToken;
	bool IsStrict;

	bool IsFinished() { return CurrentToken == JsonToken::eof || CurrentToken == JsonToken::Error; }
	bool Check(JsonToken::value tokenRequired);
	bool Skip(JsonToken::value tokenRequired);
	bool SkipIf(JsonToken::value tokenRequired);

	void SetError(const wchar_t* pszError);

private:
	wchar_t NextChar();
	void TokenizeNumber();

	const wchar_t* _psz;
	const wchar_t* _p;
	CUniString _string;
	double _number;					   
	bool _bool;
	wchar_t _currentChar;
};

CJsonValue* JsonParse(CJsonTokenizer& tok);
CJsonValue* JsonParse(const wchar_t* psz, bool strict);
CJsonValue* JsonParseFile(const wchar_t* psz, bool strict);
CUniString JsonFormat(CJsonValue* pVal);

void SimpleJsonTest();

#endif	// __SIMPLEJSON_H

