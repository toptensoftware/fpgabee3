#ifndef _NFFSFILE_H
#define _NFFSFILE_H


struct NFFSFS
{
	NFFSCONTEXT ctx;
	FILE* pUnderlying;
	uint32_t currentBlock;
	bool currentBlockLocked;
	char buffer[NFFS_BLOCK_SIZE];
	bool readOnly;
};

int nffsOpenFileOrVolume(const char* filename, bool create, bool readOnly, FILE** pVal);


int nffsStreamInit(FILE* pUnderlying, bool readOnly, NFFSFS** pVal);
int nffsStreamInitNew(FILE* pUnderlying, uint32_t directoryBlocks, NFFSFS** pVal);
int nffsStreamRepairAndInit(FILE* pUnderlying, NFFSFS** pVal);
int nffsStreamUninit(NFFSFS* pFS, bool closeUnderlying);
int nffsStreamInit(const char* filename, bool readOnly, NFFSFS** pVal);
//int nffsStreamInitNew(const char* filename, uint32_t directoryBlocks, NFFSFS** pVal);
int nffsStreamRepairAndInit(const char* filename, NFFSFS** pVal);

#endif // _NFFSFILE_H