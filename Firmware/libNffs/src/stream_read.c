///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

int nffsStreamRead(NFFSSTREAM* stream, void* buf, uint32_t bytes, uint32_t* pBytesRead)
{
	uint32_t bytesLeft;	

	// Check params
	if (!stream || !stream->owner)
		return NFFS_ERR_PARAMETER;

	// Truncate bytes to eof
	if (stream->position + bytes > stream->length)
		bytes = stream->length - stream->position;

	// Write bytes
	bytesLeft = bytes;
	while (bytesLeft)
	{
		uint32_t positionInBlock;
		uint32_t bytesInBlock;

		// Which block do we need
		uint32_t requiredBlock = stream->position / NFFS_BLOCK_SIZE;
		if (requiredBlock != stream->currentBlock)
		{
			void* pMem;

			// Flush the current block?
			if (stream->currentBlockDirty)
			{
				int err = nffsStreamFlush(stream);
				if (err)
					return err;
			}

			// Read the block
			pMem = stream->owner->readBlock(stream->owner, stream->block + requiredBlock);
			if (pMem == NULL)
				return NFFS_ERR_READ;

			memcpy(stream->buffer, pMem, NFFS_BLOCK_SIZE);
			stream->currentBlock = requiredBlock;
		}

		// Work out which part of block we're reading
		positionInBlock = stream->position % NFFS_BLOCK_SIZE;
		bytesInBlock = bytesLeft;
		if (positionInBlock + bytesLeft > NFFS_BLOCK_SIZE)
		{
			bytesLeft = NFFS_BLOCK_SIZE - positionInBlock;
		}

		// Return data to caller
		memcpy(buf, (char*)stream->buffer + positionInBlock, bytesInBlock);

		// Update current position
		stream->position += bytesInBlock;
		buf = (char*)buf + bytesInBlock;
		bytesLeft -= bytesInBlock;
	}

	// Return the number of bytes read
	*pBytesRead = bytes;
	return NFFS_OK;
}

