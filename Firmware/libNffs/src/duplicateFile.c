///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

int nffsDuplicateFile(NFFSCONTEXT* ctx, const char* existingFilename, const char* newFilename)
{
	int err;
	NFFSDIRENTRY deExisting;
	NFFSDIRENTRY deNew;

	// Find the existing file entry
	err = nffsFindFile(ctx, existingFilename, &deExisting);
	if (err)
		return err;

	// Create the new file
	err = nffsCreateFile(ctx, newFilename, deExisting.blockCount, deExisting.length, &deNew);
	if (err)
		return err;

	// Copy data
	err = nffsMoveBlocks(ctx, deExisting.block, deExisting.blockCount, deNew.block);

	// Done!
	return err;
}
