///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

#ifdef __SDCC
#pragma disable_warning 196			// pointer target lost const qualifier
#endif

// Just store the matching directory entry and abort enumeration
static bool findFileCallback(uint32_t deIndex, const NFFSDIRENTRY* de, void* user)
{
	FINDFILECTX* fctx = (FINDFILECTX*)user;
	if (strcmp(de->filename, fctx->filename)==0)
	{
		fctx->deIndex = deIndex;
		fctx->de = (NFFSDIRENTRY*)de;
		return false;
	}

	return true;
}

// Find a file (internal version returns additional info)
int nffsFindFileInternal(FINDFILECTX* pfctx)
{
	int err;

	// Check it's a valid filename
	if (!nffsIsValidFileName(pfctx->filename))
		return NFFS_ERR_FILENAME;

	// Enumerate files
	err = nffsEnumFiles(pfctx->ctx, findFileCallback, pfctx);

	// Found by callback
	if (err == NFFS_ERR_CANCELLED)
	{
		return NFFS_OK;
	}

	// Not found by callback?
	if (err == NFFS_OK)
		return NFFS_ERR_NOTFOUND;

	// Other erro
	return err;
}

// Find a file (public version)
int nffsFindFile(NFFSCONTEXT* ctx, const char* filename, NFFSDIRENTRY* pde)
{
	int err;

	// Setup find context
	FINDFILECTX fctx;
	fctx.ctx = ctx;
	fctx.deIndex = 0;
	fctx.de = NULL;
	fctx.filename = filename;

	err = nffsFindFileInternal(&fctx);
	if (err == NFFS_OK)
	{
		memcpy(pde, fctx.de, sizeof(NFFSDIRENTRY));
	}

	return err;
}

