///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"


int nffsStreamCreate(NFFSCONTEXT* ctx, const char* filename, uint32_t length, NFFSSTREAM** pVal)
{
	if (length == 0)
	{
		// Open tail stream...
		uint32_t block;
		uint32_t blockCount;
		int err = nffsCreateTail(ctx, &block, &blockCount);
		if (err)
			return err;

		return nffsOpenStreamInternal(ctx, filename, block, blockCount, 0, false, true, pVal);
	}
	else
	{
		// How many blocks will we need
		uint32_t blocks = (length + NFFS_BLOCK_SIZE - 1) / NFFS_BLOCK_SIZE;

		// Create file of known size
		NFFSDIRENTRY DE;
		int err = nffsCreateFile(ctx, filename, blocks, 0, &DE);
		if (err)
			return err;

		return nffsOpenStreamInternal(ctx, filename, DE.block, DE.blockCount, 0, false, false, pVal);
	}
}

