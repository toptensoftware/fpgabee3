///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

// Rename a file
int nffsDeleteFile(NFFSCONTEXT* ctx, const char* filename)
{
	FINDFILECTX fctx;
	int err;
	void* pBlock;

	// Find the file
	fctx.ctx = ctx;
	fctx.filename = filename;
	err = nffsFindFileInternal(&fctx);
	if (err)
		return err;

	// Zap the directory entry
	memset(fctx.de, 0, sizeof(NFFSDIRENTRY));

	// Re-write the directory block
	if (!ctx->writeBlock(ctx, ctx->rootSector.directoryBlock + fctx.deIndex / NFFS_DIRENTRIES_PER_BLOCK, NULL))
		return NFFS_ERR_WRITE;

	// Update the file count
	ctx->rootSector.fileCount--;

	// Re-write the root block
	pBlock = ctx->createBlock(ctx, 0);
	if (!pBlock)
		return NFFS_ERR_MEMORY;

	memcpy(pBlock, &ctx->rootSector, sizeof(NFFSROOTSECTOR));
	if (!ctx->writeBlock(ctx, 0, NULL))
		return NFFS_ERR_WRITE;

	// Done!
	return NFFS_OK;
}

