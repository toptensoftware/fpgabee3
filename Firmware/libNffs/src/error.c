///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

static const char* g_errorMessages[] = {
	"no error", 
	"disk read error",
	"disk write error",
	"not an NFFS file system",
	"NFFS version mismatch",
	"invalid filename",
	"file not found",
	"operation cancelled",
	"file already exists",
	"out of space",
	"out of memory",
	"active tail operation",
	"invalid parameter",
	"access denied (read only)",
	"crt error (see errno)",
	"unknown error",
	"file system needs repair",
};								


const char* nffsErrorMessage(int err)
{
	if (err>=0 && err<=sizeof(g_errorMessages)/sizeof(g_errorMessages[0]))
		return g_errorMessages[err];
	else
		return NULL;
}
