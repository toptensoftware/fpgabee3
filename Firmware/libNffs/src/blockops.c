///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

// Move blocks
int nffsMoveBlocks(NFFSCONTEXT* ctx, uint32_t block, uint32_t blockCount, uint32_t newLocation)
{
	uint32_t i;
	for (i=0; i<blockCount; i++)
	{
		// Read block
		void* pSrc =  ctx->readBlock(ctx, block + i);
		if (!pSrc)
			return NFFS_ERR_READ;

		// Write
		if (!ctx->writeBlock(ctx, newLocation + i, pSrc))
			return NFFS_ERR_WRITE;
	}

	return NFFS_OK;
}

// Zero blocks
int nffsZeroBlocks(NFFSCONTEXT* ctx, uint32_t block, uint32_t blockCount)
{
	uint32_t i;
	for (i=0; i<blockCount; i++)
	{
		// Create new block
		void* pDest = ctx->createBlock(ctx, block + i);
		if (!pDest)
			return NFFS_ERR_MEMORY;

		// Copy
		memset(pDest, 0, NFFS_BLOCK_SIZE);

		// Write
		if (!ctx->writeBlock(ctx, block + i, NULL))
			return NFFS_ERR_WRITE;
	}

	return NFFS_OK;
}

