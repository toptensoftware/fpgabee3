///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

// Create a new NFFS partition
int nffsInitNew(NFFSCONTEXT* ctx, uint32_t directoryBlocks, uint32_t totalBlocks)
{
	void* pBlock;
	uint32_t i;

	// Create the root sector
	ctx->rootSector.signature = NFFS_SIGNATURE;
	ctx->rootSector.version = NFFS_VERSION;
	ctx->rootSector.directoryBlock = 1;
	ctx->rootSector.directoryBlockCount = directoryBlocks;
	ctx->rootSector.fileCount = 0;
	ctx->rootSector.directoryHighWater = 0;
	ctx->rootSector.totalBlocksUsed = directoryBlocks + 1;
	ctx->rootSector.totalBlocks = totalBlocks;
	ctx->rootSector.dirty = false;

	// Write it the root block
	pBlock = ctx->createBlock(ctx, 0);
	if (!pBlock)
		return NFFS_ERR_MEMORY;
	memcpy(pBlock, &ctx->rootSector, sizeof(ctx->rootSector));
	if (!ctx->writeBlock(ctx, 0, NULL))
		return NFFS_ERR_WRITE;

	// Write the blank directory entries
	for (i=0; i<directoryBlocks; i++)
	{
		pBlock = ctx->createBlock(ctx, i+1);
		if (!pBlock)
			return NFFS_ERR_MEMORY;

		memset(pBlock, 0, NFFS_BLOCK_SIZE);
		if (!ctx->writeBlock(ctx, i+1, NULL))
			return NFFS_ERR_WRITE;
	}

	// Clear working
	ctx->tailBlock = 0;

	return NFFS_OK;
}

