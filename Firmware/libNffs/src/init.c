///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

// Create a new NFFS partition
int nffsInit(NFFSCONTEXT* ctx)
{
	void* pBlock;

	// Read the root block
	pBlock = ctx->readBlock(ctx, 0);
	if (!pBlock)
		return NFFS_ERR_READ;

	// Copy the root sector
	memcpy(&ctx->rootSector, pBlock, sizeof(ctx->rootSector));

	// Check signature
	if (ctx->rootSector.signature != NFFS_SIGNATURE)
		return NFFS_ERR_INVALIDFS;

	// Check version
	if (ctx->rootSector.version!= NFFS_VERSION)
		return NFFS_ERR_VERSION;

	// File system clean?
	if (ctx->rootSector.dirty)
		return NFFS_ERR_NEEDSREPAIR;

	// Clear working
	ctx->tailBlock = 0;

	// Done!
	return NFFS_OK;
}

