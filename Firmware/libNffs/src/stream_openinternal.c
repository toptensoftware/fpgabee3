///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

int nffsOpenStreamInternal(NFFSCONTEXT* ctx, const char* filename, uint32_t block, uint32_t blockCount, uint32_t length, bool readOnly, bool tailStream, NFFSSTREAM** pVal)
{
	// Allocate 
	NFFSSTREAM* p = (NFFSSTREAM*)malloc(sizeof(NFFSSTREAM));
	if (!p)
		return NFFS_ERR_MEMORY;

	// Store current position
	p->owner = ctx;
	p->position = 0;
	p->length = length;
	p->filename = malloc(strlen(filename) + 1);
	strcpy(p->filename, filename);
	p->tailStream = tailStream;
	p->block = block;
	p->blockCount = blockCount;
	p->readOnly = readOnly;
	p->currentBlock = 0xFFFFFFFF;
	p->currentBlockDirty = false;

	*pVal = p;
	return NFFS_OK;
}

