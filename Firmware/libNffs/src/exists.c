///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"


// Check if a file exists
int nffsExists(NFFSCONTEXT* ctx, const char* filename)
{
	NFFSDIRENTRY deUnused;
	return nffsFindFile(ctx, filename, &deUnused) == NFFS_OK;
}
