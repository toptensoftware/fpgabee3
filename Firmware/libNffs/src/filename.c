///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

// Check if a string contains wildcard characters
bool nffsContainsWildcards(const char* filename)
{
	while (*filename)
	{
		if (*filename == '?' || *filename == '*')
			return true;
		filename++;
	}

	return false;
}

// Check if a filename is valid
bool nffsIsValidFileName(const char* filename)
{
	if (!filename)
		return false;

	if (strlen(filename) > NFFS_MAX_FILENAME)
		return false;

	if (nffsContainsWildcards(filename))
		return false;

	return true;
}

const char* nffsExtension(const char* filename)
{
	const char* pszExt = strrchr(filename, '.');
	return pszExt;
}

int nffsEnsureExtension(char* filename, const char* ext)
{
	// Find existing extension
	const char* pszExt = nffsExtension(filename);
	if (pszExt)
	{
		// Check it's valid
		if (!nffsIsValidFileName(filename))
			return NFFS_ERR_FILENAME;

		return 0;
	}

	// Make sure have room to add default extension
	if (strlen(filename) + strlen(ext) + 1 > NFFS_MAX_FILENAME)
		return NFFS_ERR_FILENAME;	

	// Append it
	strcat(filename, ".");
	strcat(filename, ext);

	// Check it's valid
	if (!nffsIsValidFileName(filename))
		return NFFS_ERR_FILENAME;

	return 0;

}
