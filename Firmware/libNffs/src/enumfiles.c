///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

// Enumerate all files (until callback returns false)
int nffsEnumFilesInternal(NFFSCONTEXT* ctx, PFNENUMFILES callback, void* user, bool includedUnusedEntries)
{
	uint32_t directoryBlocksUsed;
	uint32_t endBlock;
	uint32_t deIndex;
	uint32_t block;


	// Work out how many directory blocks are actually in use
	directoryBlocksUsed = (ctx->rootSector.directoryHighWater + NFFS_DIRENTRIES_PER_BLOCK - 1) / NFFS_DIRENTRIES_PER_BLOCK;

	// Work out the last block we need to check
	endBlock = ctx->rootSector.directoryBlock + ctx->rootSector.directoryBlockCount;

	// Also maintain directory entry index
	deIndex = 0;

	// Enumerate directory blocks
	for (block = ctx->rootSector.directoryBlock; block < endBlock; block++)
	{
		int i;
		
		// Read directory block
		NFFSDIRENTRY* de = (NFFSDIRENTRY*)ctx->readBlock(ctx, block);
		if (!de)
			return NFFS_ERR_READ;

		// Hold block
		ctx->lockBlock(ctx, block, true);

		// Enumeate directory entries
		for (i=0; i<NFFS_DIRENTRIES_PER_BLOCK; i++)
		{
			// Ignore unused directory entries
			if (de->filename[0]!='\0' || includedUnusedEntries)
			{
				// Callback
				if (!callback(deIndex, de, user))
				{
					// Release block
					ctx->lockBlock(ctx, block, false);
					return NFFS_ERR_CANCELLED;
				}
			}

			// Update position
			deIndex++;
			de++;
		}

		// Release block
		ctx->lockBlock(ctx, block, false);
	}

	return NFFS_OK;
}

int nffsEnumFiles(NFFSCONTEXT* ctx, PFNENUMFILES callback, void* user)
{
	// File system clean?
	if (ctx->rootSector.dirty)
		return NFFS_ERR_NEEDSREPAIR;

	return nffsEnumFilesInternal(ctx, callback, user, false);
}

