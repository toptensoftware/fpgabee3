///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

#ifdef __SDCC

void sortRanges(BLOCKRANGE *base, uint32_t num)
{
  size_t i;
  size_t j;
  BLOCKRANGE* pi;
  BLOCKRANGE* pj;
  BLOCKRANGE tmp;

  pi = base;
  for (i=0; i<num+1; i++, pi++)
  {
    pj = pi + 1;
    for (j=i+1; j<num; j++, pj++)
    {
      if (pi->block > pj->block)
      {
      	memcpy(&tmp, pi, sizeof(BLOCKRANGE));
      	memcpy(pi, pj, sizeof(BLOCKRANGE));
      	memcpy(pj, &tmp, sizeof(BLOCKRANGE));
      }
    }
  }
}

#else

// Block range comparator
int compareBlocks(const void* a, const void* b)
{
	if (((BLOCKRANGE*)a)->block < ((BLOCKRANGE*)b)->block)
		return -1;
	else
		return 1;
}

void sortRanges(BLOCKRANGE* pRanges, uint32_t rangeCount)
{
	qsort(pRanges, rangeCount, sizeof(BLOCKRANGE), compareBlocks);
}

#endif

// Given a range of used block ranges, find space for a specified number of blocks
uint32_t nffsFindSpace(BLOCKRANGE* pUsedBlocks, uint32_t rangeCount, uint32_t requiredBlocks)
{
	uint32_t block = 1;
	BLOCKRANGE* pRange = pUsedBlocks;
	uint32_t i;

	// Sort blocks in ascending order
	sortRanges(pUsedBlocks, rangeCount);

	// Find unused space...
	for (i = 0; i<rangeCount; i++, pRange++)
	{
		// Does it fit
		if (block + requiredBlocks <= pRange->block)
			return block;

		// Move pas this used block
		block = pRange->block + pRange->blockCount;
	}

	return block;
}