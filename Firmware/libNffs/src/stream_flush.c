///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

int nffsStreamFlush(NFFSSTREAM* stream)
{
	if (!stream || !stream->owner)
		return NFFS_ERR_PARAMETER;
	if (stream->readOnly)
		return NFFS_OK;

	if (stream->currentBlockDirty)
	{
		void* pMem = stream->owner->createBlock(stream->owner, stream->block + stream->currentBlock);
		if (!pMem)
			return NFFS_ERR_MEMORY;

		memcpy(pMem, stream->buffer, NFFS_BLOCK_SIZE);

		if (!stream->owner->writeBlock(stream->owner, stream->block + stream->currentBlock, NULL))
			return NFFS_ERR_WRITE;

		stream->currentBlockDirty = false;
	}

	return 0;
}

