///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

int nffsStreamSetLength(NFFSSTREAM* stream, uint32_t val)
{
	if (!stream || !stream->owner)
		return NFFS_ERR_PARAMETER;
	if (stream->readOnly)
		return NFFS_ERR_READONLY;

	// Check not too large
	if (!stream->tailStream)
	{
		if (val > stream->blockCount * NFFS_BLOCK_SIZE)
			return NFFS_ERR_SPACE;
	}

	// Store it
	stream->length = val;

	return NFFS_OK;
}

