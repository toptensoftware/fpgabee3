///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

// Rename a file
int nffsSetFileLength(NFFSCONTEXT* ctx, const char* filename, uint32_t length, bool trimUnusedBlocks)
{
	FINDFILECTX fctx;
	int err;

	// Find the file
	fctx.ctx = ctx;
	fctx.filename = filename;
	err = nffsFindFileInternal(&fctx);
	if (err)
		return err;

	// Check length is valid
	if (length > fctx.de->blockCount * NFFS_BLOCK_SIZE)
		return NFFS_ERR_SPACE;

	// Redundant?
	if (fctx.de->length == length && !trimUnusedBlocks)
		return NFFS_OK;

	// Store new length
	fctx.de->length = length;

	// Trim unused blocks?
	if (trimUnusedBlocks)
	{
		fctx.de->blockCount = (length + NFFS_BLOCK_SIZE - 1) / NFFS_BLOCK_SIZE;
		if (fctx.de->blockCount == 0)
			fctx.de->blockCount = 1;
	}

	// Re-write the directory block
	if (!ctx->writeBlock(ctx, ctx->rootSector.directoryBlock + fctx.deIndex / NFFS_DIRENTRIES_PER_BLOCK, NULL))
		return NFFS_ERR_WRITE;

	// Done!
	return NFFS_OK;
}

