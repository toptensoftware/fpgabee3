		ORG			08000h

RAM_BASE:			EQU		7000h
LIB_SCRATCH:		EQU		RAM_BASE + 20h
CYL:				EQU		RAM_BASE
HEAD:				EQU		RAM_BASE + 2
SECTOR:				EQU 	RAM_BASE + 3
SUBPAGE:			EQU		RAM_BASE + 4

START:
	LD		SP,0x8000

	LD		HL,CRTC_Registers_80_24
	CALL	setup_crtc

	CALL	clear_screen
	LD		A,COLOR_YELLOW
	CALL	clear_color_buffer
	CALL	clear_attribute_buffer

	; Copy letters to RAM (testing)
	LD		DE,CHAR_RAM_BASE
	LD		HL,MSG
	LD		BC,MSG_END-MSG
	LDIR 

	; ;- setup each bank with signature ;-
	LD		IX,CHAR_RAM_BASE+0x40

	; Select HDD controller
	LD		A,1
	OUT		(0x58),A

	; Read-write to port 0x40 should return different data
	LD		A,0xA0
	OUT		(0x40),A
	XOR		A
	IN		A,(0x40)
	CP		0xA0
	CALL	CHECK_NZ

	; Read-write to port 0x41 should return different data
	LD		A,0xA9
	OUT		(0x41),A
	XOR		A
	IN		A,(0x41)
	CP		0xA9
	CALL	CHECK_NZ

	; Read-write to port 0x42 should return same data (sector count)
	LD		A,0xAA
	OUT		(0x42),A
	XOR		A
	IN		A,(0x42)
	CP		0xAA
	CALL	CHECK_Z

	; Read-write to port 0x43 should return same data (sector number)
	LD		A,0xAB
	OUT		(0x43),A
	XOR		A
	IN		A,(0x43)
	CP		0xAB
	CALL	CHECK_Z

	; Read-write to port 0x44 should return same data (track low)
	LD		A,0xAC
	OUT		(0x44),A
	XOR		A
	IN		A,(0x44)
	CP		0xAC
	CALL	CHECK_Z

	; Read-write to port 0x45 should return same data (track hi)
	LD		A,0xAD
	OUT		(0x45),A
	XOR		A
	IN		A,(0x45)
	CP		0xAD
	CALL	CHECK_Z

	; Read-write to port 0x46 should return same data (SDH)
	LD		A,0xAE
	OUT		(0x46),A
	XOR		A
	IN		A,(0x46)
	CP		0xAE
	CALL	CHECK_Z

	; Read-write to port 0x47 should return different data (CMD)
	LD		A,0x90
	OUT		(0x47),A
	XOR		A
	IN		A,(0x47)
	CP		0x90
	CALL	CHECK_NZ

	INC		IX

	LD		A,1
	LD		(SECTOR),A
	LD		A,0
	LD		(CYL),A
	LD		(CYL+1),A
	LD		(HEAD),A
	LD		(SUBPAGE),A

read_test:

	LD		A,0
	LD		(SUBPAGE),A

	LD		DE,0xf020

	ex		DE,HL
	ld		(HL),'C'
	inc		HL
	ld		(HL),':'
	inc		HL
	ex		DE,HL

	LD		HL,(CYL)
	CALL	prt_int_word

	ex		DE,HL
	ld		(HL),' '
	inc		HL
	ld		(HL),'H'
	inc		HL
	ld		(HL),':'
	inc		HL
	ex		DE,HL

	LD		A,(HEAD)
	LD		L,A
	LD		H,0
	CALL	prt_int_word

	ex		DE,HL
	ld		(HL),' '
	inc		HL
	ld		(HL),'S'
	inc		HL
	ld		(HL),':'
	inc		HL
	ex		DE,HL

	LD		A,(SECTOR)
	LD		L,A
	LD		H,0
	CALL	prt_int_word

	ex		DE,HL
	ld		(HL),' '
	inc		HL
	ld		(HL),' '
	inc		HL
	ld		(HL),' '
	inc		HL
	ld		(HL),' '
	inc		HL
	ld		(HL),' '
	inc		HL
	ex		DE,HL

	; Setup task file
 	LD		A,(SECTOR)		; Sector 1
	OUT		(0x43),A
	LD		A,(CYL)			; Track lo
	OUT		(0x44),A
	LD		A,(CYL+1)		; Track high
	OUT		(0x45),A
	LD		A,(HEAD)
	;OR		10101000b		; HDD SIZE=512,HDD=1,HEAD=0
	OR		0x38			; FDD

	OUT		(0x46),A
	LD		A,1				; Sector count
	OUT		(0x42),A
	LD		A,0x20			; Read
	OUT		(0x47),A

wait_read:
	IN		A,(0x47)
	AND		0x80
	JR		NZ,wait_read

	LD		HL,0x3000
	LD		DE,0x3001
	LD		BC,0x3f
	LD		(HL),0xFA
	LDIR


	LD		HL,0x3000
	LD		BC,0x0040
	INIR
	INIR

render_screen:
	LD		HL,0x3000
	LD		A,(SUBPAGE)
	ADD		A,H
	LD		H,A
	LD		DE,0xF000 + 160
	LD		B,16

print_row:

	push	BC
	push	DE

	; Print address
	push	HL
	ld		A,H
	and		0x01
	ld		H,A
	call	prt_hex_word
	pop		HL

	; Separator
	ex		DE,HL
	ld		(HL),':'
	inc		HL
	ld		(HL),' '
	inc		HL
	ex		DE,HL

	; Hex Bytes
	push	HL
	LD		B,16
	CALL	hexdump
	pop		HL

	; Separator
	ex		DE,HL
	ld		(HL),' '
	inc		HL
	ld		(HL),'|'
	inc		HL
	ld		(HL),' '
	inc		HL
	ex		DE,HL

	; Characters
	ld		B,16
	call	chardump

	pop		DE
	pop		BC

	; Move to next row
	push	HL
	ld		HL,80
	add		HL,DE
	ex		DE,HL
	pop		HL
	djnz	print_row

key_loop:
	call	read_key

	; Change direction if shift
	ld		DE,0xFFFF
	ld		B,A
	ld		A,KEY_SHIFT
	call	is_key_down
	jr		z,shift_key
	ld		DE,1
shift_key:
	ld		A,B

non_shift_key:
	cp		KEY_A + 'C' - 'A'
	jr		z,inc_cyl
	cp		KEY_A + 'H' - 'A'
	jr		z,inc_head
	cp		KEY_A + 'S' - 'A'
	jr		z,inc_sector
	cp		KEY_A + 'X' - 'A'
	jr		z,xor_sector
	cp		KEY_UP
	jr		z,page_up
	cp		KEY_DOWN
	jr		z,page_down
	jr		key_loop

inc_cyl:
	LD		HL,(CYL)
	ADD		HL,DE
	LD		(CYL),HL
	jp		read_test

inc_head:
	LD		A,(HEAD)
	ADD		E
	LD		(HEAD),A
	jp		read_test

inc_sector:
	LD		A,(SECTOR)
	ADD		E
	LD		(SECTOR),A
	jp		read_test

page_up:
	LD		A,0
	LD		(SUBPAGE),A
	jp		render_screen

page_down:
	LD		A,1
	LD		(SUBPAGE),A
	jp		render_screen

xor_sector:
	; XOR all bytes in the sector buffer
	ld		HL,0x3000
	ld		BC,512
xor_loop:
	ld		A,(HL)
	xor		0xFF
	ld		(HL),A
	inc		HL
	dec		BC
	ld		A,B
	or		C
	jr		nz,xor_loop

	
	; Do the disk write
	LD		A,(SECTOR)		; Sector
	OUT		(0x43),A
	LD		A,(CYL)			; Track lo
	OUT		(0x44),A
	LD		A,(CYL+1)		; Track high
	OUT		(0x45),A
	LD		A,(HEAD)
	OR		10101000b		; SIZE=512,HDD=1,HEAD=0
	OUT		(0x46),A
	LD		A,1				; Sector count
	OUT		(0x42),A
	LD		A,0x30			; Invoke Write Common
	OUT		(0x47),A

	; Write the data
	LD		HL,0x3000
	LD		BC,512
write_loop:
	LD		A,(HL)
	OUT		(40h),A
	inc		HL
	dec		BC
	ld		A,B
	or		C
	jr		NZ,write_loop

wait_write:
	IN		A,(0x47)
	AND		0x80
	JR		NZ,wait_write

	jp 		read_test



hexdump:
    LD      A,(HL)
    CALL    prt_hex_byte
    INC     HL
    INC     DE
    DJNZ    hexdump
    RET

chardump:
	LD		A,(HL)
	cp		32
	jr		c,cd_skip_char
cd_continue:
	ld		(DE),A
	inc		DE
	inc		HL
	DJNZ	chardump
	ret

cd_skip_char:
	ld		A,'.'
	jr		cd_continue



GEO_TESTS:

	CALL	wait_key

	; Test calculation of cluster number
	LD		A,2				; Sector 2
	OUT		(0x43),A
	LD		A,2				; Track low
	OUT		(0x44),A
	LD		A,0				; Track high
	OUT		(0x45),A
	LD		A,00001000b		; SDH (drive 1/head 0)  HD0
	OUT		(0x46),A
	LD		A,0x90			; Invoke 
	OUT		(0x47),A	
	CALL 	wait_key		;; Should show 0x89

	; Test calculation of cluster number
	LD		A,2				; Sector 2
	OUT		(0x43),A
	LD		A,2				; Track low
	OUT		(0x44),A
	LD		A,0				; Track high
	OUT		(0x45),A
	LD		A,00001010b		; SDH (drive 1/head 0)  HD0
	OUT		(0x46),A
	LD		A,0x90			; Invoke 
	OUT		(0x47),A	
	CALL 	wait_key		;; Should show 0xAB

	; Test calculation of cluster number
	LD		A,2				; Sector 2
	OUT		(0x43),A
	LD		A,0x33			; Track low
	OUT		(0x44),A
	LD		A,0x01			; Track high
	OUT		(0x45),A
	LD		A,00001011b		; SDH (drive 1/head 0)  HD0
	OUT		(0x46),A
	LD		A,0x90			; Invoke 
	OUT		(0x47),A	
	CALL 	wait_key		;; Should show error (ubee512 doesn't though as relies on OS eof check)

	; Test calculation of cluster number
	LD		A,2				; Sector 2
	OUT		(0x43),A
	LD		A,2				; Track low
	OUT		(0x44),A
	LD		A,1				; Track high
	OUT		(0x45),A
	LD		A,00001011b		; SDH (drive 1/head 0)  HD0
	OUT		(0x46),A
	LD		A,0x90			; Invoke 
	OUT		(0x47),A	
	CALL 	wait_key		;; Should show 44bc

	; Test calculation of cluster number
	LD		A,23			; Sector 23
	OUT		(0x43),A
	LD		A,2				; Track low
	OUT		(0x44),A
	LD		A,0				; Track high
	OUT		(0x45),A
	LD		A,00011000b		; SDH (fdd 1/head 0)   DS80
	OUT		(0x46),A
	LD		A,0x90			; Invoke 
	OUT		(0x47),A	
	CALL 	wait_key		; Should show 0x2a


	; Test calculation of cluster number
	LD		A,2				; Sector 2
	OUT		(0x43),A
	LD		A,2				; Track low
	OUT		(0x44),A
	LD		A,0				; Track high
	OUT		(0x45),A
	LD		A,00011000b		; SDH (fdd 1/head 0)   DS80
	OUT		(0x46),A
	LD		A,0x90			; Invoke 
	OUT		(0x47),A	
	CALL 	wait_key		; Should show error


	; Test calculation of cluster number
	LD		A,2				; Sector 2
	OUT		(0x43),A
	LD		A,1				; Track low
	OUT		(0x44),A
	LD		A,0				; Track high
	OUT		(0x45),A
	LD		A,00011000b		; SDH (fdd 1/head 0)   DS80
	OUT		(0x46),A
	LD		A,0x90			; Invoke 
	OUT		(0x47),A	
	CALL 	wait_key		; Should show 0x15

	RET

CHECK_Z:
	LD		A,6
	JR		Z,L_write
	LD		A,5
L_write:
	LD		(IX+0),A
	INC		IX
	RET

CHECK_NZ:
	LD		A,6
	JR		NZ,L_write
	LD		A,5
	JR		L_write




MSG:
	DB	"Disk Controller Testing"
MSG_END:



include "CommonDefs.asm"

