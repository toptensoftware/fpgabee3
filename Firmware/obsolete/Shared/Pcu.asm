; pcu.asm
;
; Constants relating to PCU


;----------------------------------------------
; PCU Video

SCREEN_WIDTH:						EQU	32
SCREEN_HEIGHT:						EQU	16
VCHAR_RAM:							EQU	0xF000
COLOR_RAM:							EQU 0xF200
VBUFFER_SIZE:						EQU SCREEN_WIDTH * SCREEN_HEIGHT

; PCU Character rom is reduced to 128 characters...
; these are the box draw character codes
BOX_TL:		EQU	6
BOX_TR:		EQU 3
BOX_BL:		EQU	4
BOX_BR:		EQU 5
BOX_H:		EQU 1
BOX_V:		EQU 2


;----------------------------------------------
; PCU Exit

; Write anything to this port to exit PCU mode
; and yield back to Microbee processing.
; Must be immediately followed by RET or RETN

PORT_PCU_EXIT:						EQU 0x80

;----------------------------------------------
; Memory Banking and Boot ROM Latch
;
; Port D0 memory bank/rom latch
;
; Bit 7 - boot rom latch (1 on boot)
; Bit 6 - unused
; Bit 5 - unused
; Bit 4 - MicroBee RAM/ROM latch
; Bits 3..0  = high 4 bits of the 18-bit external ram address
;				(Gives PCU access to all external RAM in 16k Pages)

PORT_MEMORY_BANK:					EQU 0xD0	; write
MEMORY_BANK_BOOT_LATCH:				EQU 0x80
MEMORY_BANK_RAM_LATCH:				EQU 0x10
MEMORY_BANK_ADDR:					EQU 0xC000

;----------------------------------------------
; Debug/LED ports

PORT_LED_REG:						EQU 0xA0	; write
PORT_HEX_LO_REG:					EQU 0xA1	; write
PORT_HEX_HI_REG:					EQU 0xA2	; write

;----------------------------------------------
; Disk Controller

; Ports
PORT_DISK_DATA:						EQU 0xC0	; read/write
PORT_DISK_STATUS:					EQU	0xC7	; read
PORT_DISK_COMMAND:					EQU 0xC7	; write
PORT_DISK_ADDRESS:					EQU 0xC1

PORT_MBEE_DISK_BLOCK_REG:			EQU	0x41
PORT_MBEE_DISK_SET_DRIVE_TYPE:		EQU 0x47

; Status bits
DISK_STATUS_BIT_BUSY:				EQU 0x80 	; Disk controller is busy
DISK_STATUS_BIT_INITIALIZED:		EQU 0x40    ; Disk controller has initialized

; Basic Commands
DISK_COMMAND_REWIND_BUFFER:			EQU 00b
DISK_COMMAND_READ:					EQU 01b
DISK_COMMAND_WRITE:					EQU 10b
			
; To set disk configuration
; Send command with bit 7 set
; Bits 6 downto 3 = disk type
; Bits 2 downto 0 = disk number
DISK_COMMAND_SETDISKCONFIG:			EQU 0x80 
DISK_COMMAND_DISKNUMBERMASK:		EQU 0x03
DISK_COMMAND_DISKTYPE:       		EQU 0x7C

; Disk type constants
DISK_DS40: EQU 0
DISK_SS80: EQU 1
DISK_DS80: EQU 2
DISK_DS82: EQU 3
DISK_DS84: EQU 4
DISK_DS8B: EQU 5
DISK_HD0:  EQU 6
DISK_HD1:  EQU 7
DISK_NONE: EQU 8

;----------------------------------------------
; MBR

; Offset in the boot sector to the partition table
MBR_OFFSET_PARTITION_TABLE:	EQU  0x1BE

; Offsets into a partition table entry
MBR_OFFSET_PARTITION_ENTRY_STATUS:		EQU 0		; 0x80 = bootable
MBR_OFFSET_PARTITION_ENTRY_START:		EQU	1 		; 3 byte CHS address
MBR_OFFSET_PARTITION_ENTRY_PARTTYPE: 	EQU 4		; 
MBR_OFFSET_PARTITION_ENTRY_END: 		EQU 5 		; 3 byte CHS address
MBR_OFFSET_PARTITION_FIRST_SECTOR: 		EQU 8 		; 4 bytes
MBR_OFFSET_PARTITION_SECTOR_COUNT: 		EQU 12 		; 4 bytes


;----------------------------------------------
; FBFS

; FBFS Config Sector
FBFS_SIG:				EQU		0			; "fbfs"
FBFS_VER:				EQU		4			; 1
FBFS_DIRBLK:			EQU		6			; First block of directory table
FBFS_DIRCOUNT:			EQU		10			; Number of blocks in directory table
FBFS_DI_SYSTEM:			EQU		12			; Directory Index of system image
FBFS_DI_ROM_0:			EQU		14			; Selected Rom Packs
FBFS_DI_ROM_1:			EQU		16			; 
FBFS_DI_ROM_2:			EQU		18			; 
FBFS_DI_DISK_0:			EQU		20			; Selected disks
FBFS_DI_DISK_1:			EQU		22
FBFS_DI_DISK_2:			EQU		24
FBFS_DI_DISK_3:			EQU		26
FBFS_DI_DISK_4:			EQU		28
FBFS_DI_DISK_5:			EQU		30
FBFS_DI_DISK_6:			EQU		32
FBFS_CONFIG_SIZE:		EQU		34			; Size of config data
	
; FBFS Directory structure
FBFS_DIR_BLOCK:			EQU		0			; Block number where file starts
FBFS_DIR_BLOCK_COUNT:	EQU		4			; Number of blocks in file
FBFS_DIR_RESERVED:		EQU		6			; Unused
FBFS_DIR_FILENAME:		EQU		10			; Filename
FBFS_DIR_SIZE:			EQU		32

FBFS_MAX_FILENAME:		EQU		22			; Maximum length of filename (including the NULL)


;----------------------------------------------
; Keyboard

; Keycodes
VK_BACKTICK: EQU 0x01
VK_LSHIFT: EQU 0x02
VK_RSHIFT: EQU 0x03
VK_LCTRL: EQU 0x04
VK_RCTRL: EQU 0x05
VK_LMENU: EQU 0x06
VK_RMENU: EQU 0x07
VK_BACKSPACE: EQU 0x08
VK_TAB: EQU 0x09
VK_COMMA: EQU 0x0A
VK_PERIOD: EQU 0x0B
VK_HYPHEN: EQU 0x0C
VK_ENTER: EQU 0x0D
VK_SEMICOLON: EQU 0x0E
VK_EQUALS: EQU 0x0F
VK_ESCAPE: EQU 0x10
VK_F1: EQU 0x11
VK_F2: EQU 0x12
VK_F3: EQU 0x13
VK_F4: EQU 0x14
VK_F5: EQU 0x15
VK_F6: EQU 0x16
VK_F7: EQU 0x17
VK_F8: EQU 0x18
VK_F9: EQU 0x19
VK_F10: EQU 0x1a
VK_F11: EQU 0x1b
VK_F12: EQU 0x1c
VK_LSQUARE: EQU 0x1d
VK_RSQUARE: EQU 0x1e
VK_QUOTE: EQU 0x1f
VK_SPACE: EQU 0x20
VK_LEFT: EQU 0x21
VK_RIGHT: EQU 0x22
VK_UP: EQU 0x23
VK_DOWN: EQU 0x24
VK_HOME: EQU 0x25
VK_END: EQU 0x26
VK_NEXT: EQU 0x27
VK_PRIOR: EQU 0x28
VK_INSERT: EQU 0x29
VK_DELETE: EQU 0x2a
VK_SLASH: EQU 0x2b
; unused 2c
; unused 2d
; unused 2e
; unused 2f
; = 0x30 = = 0x39 = 0 - 9
VK_BACKSLASH: EQU 0x3a
VK_CAPITAL: EQU 0x3b
VK_NUMENTER: EQU 0x3c
; unused 3d
; unused 3e
; unused 3f
; unused 40
; = 0x41 - = 0x5A = A - Z
VK_SUBTRACT: EQU 0x5b
VK_MULTIPLY: EQU 0x5c
VK_DIVIDE: EQU 0x5d
VK_ADD: EQU 0x5e
VK_DECIMAL: EQU 0x5F
VK_NUM0: EQU 0x60
VK_NUM1: EQU 0x61
VK_NUM2: EQU 0x62
VK_NUM3: EQU 0x63
VK_NUM4: EQU 0x64
VK_NUM5: EQU 0x65
VK_NUM6: EQU 0x66
VK_NUM7: EQU 0x67
VK_NUM8: EQU 0x68
VK_NUM9: EQU 0x69


; ; Write block DEBC from SECTOR_BUFFER
; DISK_WRITE:
; 
; 	; Setup block number
; 	ld		A,C
; 	ld		C,PORT_DISK_ADDRESS
; 	out		(C),A
; 	out		(C),B
; 	out		(C),E
; 	out		(C),D
; 
; 	; Initiate the write command
; 	ld			A,DISK_COMMAND_WRITE
; 	out			(PORT_DISK_COMMAND),A
; 
; 	; Read it
; 	ld		BC,0x00C0
; 	otir
; 	otir
; 
; 	; Wait for write to finish
; dw_wait:	
; 	in		A,(PORT_DISK_STATUS)
; 	and		DISK_STATUS_BIT_BUSY
; 	JR		NZ,dw_wait
; 
; 	; Done!
; 	ret
; DISK_WRITE_END:
; 





;check_dc_ram:
;	LD		BC,512
;	LD		E,0
;	LD		D,0
;cdr_loop1:
;	; Get byte and compare to test byte
;	ld		A,E
;	out		(PORT_DISK_DATA),A
;
;	; Calculate next test byte
;	ld		A,E
;	xor		D
;	ld		E,A
;
;	; Update counters and loop
;	inc		D
;	dec		bc
;	ld		a,b
;	or		c
;	jr		nz,cdr_loop1
;
;
;	LD		BC,512
;	LD		E,0
;	LD		D,0
;cdr_loop2:
;	; Get byte and compare to test byte
;	in		A,(PORT_DISK_DATA)
;
;	cp		E
;	jr		nz,cdr_bad
;
;
;	; Calculate next test byte
;	ld		A,E
;	xor		D
;	ld		E,A
;
;	; Update counters and loop
;	inc		D
;	dec		bc
;	ld		a,b
;	or		c
;	jr		nz,cdr_loop2
;
;cdr_good:
;	LD 		A,0xAA
;	OUT		(PORT_LED_REG),A
;	jr $
;
;cdr_bad:
;	LD 		A,0x01
;	OUT		(PORT_LED_REG),A
;	jr $
