; DE = directory index (-ve to eject)
; A = drive number (0-6)
InsertDisk:

	; Save registers
	push	HL
	push	BC

	; Save drive number 
	push	AF

	; Check for "eject" (DE==-1)
	ld		A,D
	and		80h
	jr		z,id_has_disk

	; Clear the block number
	ld		A,0
	out		(PORT_MBEE_DISK_BLOCK_REG),A
	out		(PORT_MBEE_DISK_BLOCK_REG),A
	out		(PORT_MBEE_DISK_BLOCK_REG),A
	out		(PORT_MBEE_DISK_BLOCK_REG),A

	; No disk
	ld		A,DISK_NONE
	jr		id_have_disk_type

id_has_disk:
	ex		DE,HL
	call	FbfsGetDirEntry

	; Adjust the disk block number to the partition start sector
	push	HL
	pop		IY
	ld		c,(IY+0)
	ld		b,(IY+1)
	ld		e,(IY+2)
	ld		d,(IY+3)
	call	AdjustSector

	; Send to disk controller
	ld		A,C
	out		(PORT_MBEE_DISK_BLOCK_REG),A
	ld		A,B
	out		(PORT_MBEE_DISK_BLOCK_REG),A
	ld		A,E
	out		(PORT_MBEE_DISK_BLOCK_REG),A
	ld		A,D
	out		(PORT_MBEE_DISK_BLOCK_REG),A

	; Work out the disk type from the file's extension
	ld		DE,FBFS_DIR_FILENAME
	add		HL,DE
	call	IMAGE_TYPE_FROM_EXTENSION

id_have_disk_type:
	sla		A
	sla		A
	sla		A
	ld		B,A

	; Get drive number back
	pop		AF

	; Adjust floppy drive number to match what disk controller expects
	cp		3
	jr		C,id_no_inc
	inc		A
id_no_inc:

	; Compose the final command and send it
	or		B			; Disk type
	out		(PORT_MBEE_DISK_SET_DRIVE_TYPE),A

	; Restore registers
	pop		BC
	pop		HL
	ret



InsertAllDisks:
	ld		HL,FBFS_CONFIG + FBFS_DI_DISK_0
	ld		B,7

id_loop:
	; Get the directory entry
	ld		E,(HL)
	inc		HL
	ld		D,(HL)
	inc		HL

	; Calculate drive number
	ld		A,7
	sub		B

	; Insert it
	call	InsertDisk

	djnz	id_loop

	ret


STR_DISK_EXTENSIONS:
	db "ds40",0
	db "ss80",0
	db "ds80",0
	db "ds82",0
	db "ds84",0
	db "ds8B",0
	db "hd0",0,0		; Pad to 5 chars
	db "hd1",0,0


; Work out the image type of a file by looking at it's extension
; HL should point to the file name
IMAGE_TYPE_FROM_EXTENSION:
	
	call	FIND_EXTENSION
	ld		A,H
	or		L
	jr		nz,itfe_has_extension
	ld		A,DISK_NONE
	ret
itfe_has_extension:

	inc		HL
	ld		B,8
	ld		DE,STR_DISK_EXTENSIONS
	ex		DE,HL
itfe_loop:
	push	HL
	push	DE
	call	STRICMP
	pop		DE
	pop		HL
	jr		z,itfe_found
	push	BC
	ld		BC,5
	add		HL,BC
	pop		BC	
	djnz	itfe_loop
itfe_found:
	ld		A,DISK_NONE
	sub		B
	ret
